(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


(lib.AnMovieClip = function(){
	this.actionFrames = [];
	this.ignorePause = false;
	this.gotoAndPlay = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndPlay.call(this,positionOrLabel);
	}
	this.play = function(){
		cjs.MovieClip.prototype.play.call(this);
	}
	this.gotoAndStop = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndStop.call(this,positionOrLabel);
	}
	this.stop = function(){
		cjs.MovieClip.prototype.stop.call(this);
	}
}).prototype = p = new cjs.MovieClip();
// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop, this.reversed));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.UnMute_Button = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.3,1,1).p("An0yCIDLRYIjLStAESyCIDjRTIjjSyAhkyCIDURoIjUSd");
	this.shape.setTransform(185.075,1.475);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(1,1,1).p("ArvjWIXfqKIAAbBI3cmag");
	this.shape_1.setTransform(0.15,-2.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#999999").s().p("ArsHGIgCqbIXeqLIAAbBg");
	this.shape_2.setTransform(0.15,-2.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-76,-119.6,316.8,242.2);


(lib.UFO = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("AZUhUQAABIg1AzQg0AzhLAAQhKAAg1gzQg1gzAAhIQAAhJA1gzQA1gzBKAAQBLAAA0AzQA1AzAABJgAPogOQAABlhIBHQhHBIhmAAQhlAAhIhIQhIhHAAhlQAAhlBIhJQBIhHBlAAQBmAABHBHQBIBJAABlgAEEAoQAABXg9A9Qg+A+hXAAQhWAAg+g+Qg9g9AAhXQAAhWA9g+QA+g9BWAAQBXAAA+A9QA9A+AABWgAnLAKQAABnhKBKQhJBJhnAAQhoAAhJhJQhJhKAAhnQAAhnBJhJQBJhJBoAAQBnAABJBJQBKBJAABngAz1giQAABIg0AyQgzA0hIAAQhJAAgzg0QgzgyAAhIQAAhJAzgzQAzgzBJAAQBIAAAzAzQA0AzAABJg");
	this.shape.setTransform(2,56);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF00FF").s().p("AitCtQhHhIAAhlQAAhlBHhHQBJhIBkAAQBmAABHBIQBIBHAABlQAABlhIBIQhHBIhmAAQhkAAhJhIg");
	this.shape_1.setTransform(77.5,54.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF0000").s().p("AiTCVQg+g+AAhXQAAhWA+g9QA9g+BWAAQBXAAA+A+QA9A9AABWQAABXg9A+Qg+A9hXAAQhWAAg9g9g");
	this.shape_2.setTransform(7,60);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#0000CC").s().p("AiwCwQhJhIAAhoQAAhmBJhKQBKhJBmAAQBoAABIBJQBKBKAABmQAABohKBIQhIBKhoAAQhmAAhKhKg");
	this.shape_3.setTransform(-69,57);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#00FF00").s().p("Ah7B8QgzgzAAhJQAAhIAzgzQAzgzBIAAQBIAAA0AzQAzAzAABIQAABJgzAzQg0AzhIAAQhIAAgzgzg");
	this.shape_4.setTransform(-142.5,52.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#CC6633").s().p("Ah+B7Qg1gzAAhIQAAhIA1gzQA0gzBKAAQBKAAA1AzQA1AzAABIQAABIg1AzQg1A0hKAAQhKAAg0g0g");
	this.shape_5.setTransform(146,47.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#66FF00").s().p("AitCtQhHhIAAhlQAAhlBHhHQBJhIBkAAQBmAABHBIQBIBHAABlQAABlhIBIQhHBIhmAAQhkAAhJhIg");
	this.shape_6.setTransform(77.5,54.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#0000FF").s().p("AiTCVQg+g+AAhXQAAhWA+g9QA9g+BWAAQBXAAA+A+QA9A9AABWQAABXg9A+Qg+A9hXAAQhWAAg9g9g");
	this.shape_7.setTransform(7,60);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFF00").s().p("AiwCwQhJhIAAhoQAAhmBJhKQBKhJBmAAQBoAABIBJQBKBKAABmQAABohKBIQhIBKhoAAQhmAAhKhKg");
	this.shape_8.setTransform(-69,57);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF0000").s().p("Ah+B7Qg1gzAAhIQAAhIA1gzQA0gzBKAAQBKAAA1AzQA1AzAABIQAABIg1AzQg1A0hKAAQhKAAg0g0g");
	this.shape_9.setTransform(146,47.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF00FF").s().p("Ah7B8QgzgzAAhJQAAhIAzgzQAzgzBIAAQBIAAA0AzQAzAzAABIQAABJgzAzQg0AzhIAAQhIAAgzgzg");
	this.shape_10.setTransform(-142.5,52.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFCCCC").s().p("Ah+B7Qg1gzAAhIQAAhIA1gzQA0gzBKAAQBKAAA1AzQA1AzAABIQAABIg1AzQg1A0hKAAQhKAAg0g0g");
	this.shape_11.setTransform(146,47.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#9900FF").s().p("AiTCVQg+g+AAhXQAAhWA+g9QA9g+BWAAQBXAAA+A+QA9A9AABWQAABXg9A+Qg+A9hXAAQhWAAg9g9g");
	this.shape_12.setTransform(7,60);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#33CC33").s().p("AiwCwQhJhIAAhoQAAhmBJhKQBKhJBmAAQBoAABIBJQBKBKAABmQAABohKBIQhIBKhoAAQhmAAhKhKg");
	this.shape_13.setTransform(-69,57);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#CCFF33").s().p("Ah7B8QgzgzAAhJQAAhIAzgzQAzgzBIAAQBIAAA0AzQAzAzAABIQAABJgzAzQg0AzhIAAQhIAAgzgzg");
	this.shape_14.setTransform(-142.5,52.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#990000").s().p("AitCtQhHhIAAhlQAAhlBHhHQBJhIBkAAQBmAABHBIQBIBHAABlQAABlhIBIQhHBIhmAAQhkAAhJhIg");
	this.shape_15.setTransform(77.5,54.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(1,1,1).p("APogOQAABkhIBIQhHBIhmAAQhlAAhIhIQhIhIAAhkQAAhmBIhIQBIhHBlAAQBmAABHBHQBIBIAABmgAEEAoQAABXg+A9Qg9A+hXAAQhWAAg+g+Qg9g9AAhXQAAhWA9g+QA+g9BWAAQBXAAA9A9QA+A+AABWgAZUhUQAABIg1AyQg0A0hLAAQhKAAg1g0Qg1gyAAhIQAAhJA1gzQA1gzBKAAQBLAAA0AzQA1AzAABJgAnLAKQAABnhKBJQhJBKhnAAQhoAAhJhKQhJhJAAhnQAAhnBJhJQBJhJBoAAQBnAABJBJQBKBJAABngAz1giQAABIg0AyQgzA0hIAAQhJAAgzg0QgzgyAAhIQAAhJAzgzQAzgzBJAAQBIAAAzAzQA0AzAABJg");
	this.shape_16.setTransform(2,56);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#336699").s().p("Ah7B7QgzgzAAhIQAAhIAzgzQAzgzBIAAQBIAAA0AzQAzAzAABIQAABIgzAzQg0A0hIAAQhIAAgzg0g");
	this.shape_17.setTransform(-142.5,52.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#CC9966").s().p("AiwCxQhJhKAAhnQAAhmBJhKQBKhJBmAAQBoAABIBJQBKBKAABmQAABnhKBKQhIBJhoAAQhmAAhKhJg");
	this.shape_18.setTransform(-69,57);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#330000").s().p("Ah/B7Qg0gyAAhJQAAhHA0g0QA2gzBJAAQBLAAA0AzQA1A0AABHQAABJg1AyQg0A0hLAAQhJAAg2g0g");
	this.shape_19.setTransform(146,47.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#3399CC").s().p("AiTCUQg+g9AAhXQAAhWA+g+QA9g9BWAAQBXAAA+A9QA9A+AABWQAABXg9A9Qg+A+hXAAQhWAAg9g+g");
	this.shape_20.setTransform(7,60);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#66FFFF").s().p("AitCtQhHhHAAhmQAAhkBHhJQBJhHBkAAQBmAABHBHQBIBJAABkQAABmhIBHQhHBIhmAAQhkAAhJhIg");
	this.shape_21.setTransform(77.5,54.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("Ah7B7QgzgzAAhIQAAhIAzgzQAzgzBIAAQBIAAA0AzQAzAzAABIQAABIgzAzQg0A0hIAAQhIAAgzg0g");
	this.shape_22.setTransform(-142.5,52.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#CCCC99").s().p("AiwCxQhJhKAAhnQAAhmBJhKQBKhJBmAAQBoAABIBJQBKBKAABmQAABnhKBKQhIBJhoAAQhmAAhKhJg");
	this.shape_23.setTransform(-69,57);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFF00").s().p("Ah/B7Qg0gyAAhJQAAhHA0g0QA2gzBJAAQBLAAA0AzQA1A0AABHQAABJg1AyQg0A0hLAAQhJAAg2g0g");
	this.shape_24.setTransform(146,47.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#3399FF").s().p("AiTCUQg+g9AAhXQAAhWA+g+QA9g9BWAAQBXAAA+A9QA9A+AABWQAABXg9A9Qg+A+hXAAQhWAAg9g+g");
	this.shape_25.setTransform(7,60);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#006633").s().p("AitCtQhHhHAAhmQAAhkBHhJQBJhHBkAAQBmAABHBHQBIBJAABkQAABmhIBHQhHBIhmAAQhkAAhJhIg");
	this.shape_26.setTransform(77.5,54.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.lf(["#FF0000","#FFFF00","#00FF00","#00FFFF","#0000FF","#FF00FF","#FF0000"],[0,0.165,0.365,0.498,0.667,0.831,1],-17.5,0,17.5,0).s().p("Ah7B8QgzgzAAhJQAAhIAzgzQAzgzBIAAQBIAAA0AzQAzAzAABIQAABJgzAzQg0AzhIAAQhIAAgzgzg");
	this.shape_27.setTransform(-142.5,52.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.lf(["#FF0000","#FFFF00","#00FF00","#00FFFF","#0000FF","#FF00FF","#FF0000"],[0,0.165,0.365,0.498,0.667,0.831,1],-25,0,25,0).s().p("AiwCwQhJhIAAhoQAAhmBJhKQBKhJBmAAQBoAABIBJQBKBKAABmQAABohKBIQhIBKhoAAQhmAAhKhKg");
	this.shape_28.setTransform(-69,57);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.lf(["#FF0000","#FFFF00","#00FF00","#00FFFF","#0000FF","#FF00FF","#FF0000"],[0,0.165,0.365,0.498,0.667,0.831,1],-21,0,21,0).s().p("AiTCVQg+g+AAhXQAAhWA+g9QA9g+BWAAQBXAAA+A+QA9A9AABWQAABXg9A+Qg+A9hXAAQhWAAg9g9g");
	this.shape_29.setTransform(7,60);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.lf(["#FF0000","#FFFF00","#00FF00","#00FFFF","#0000FF","#FF00FF","#FF0000"],[0,0.165,0.365,0.498,0.667,0.831,1],-24.5,0,24.5,0).s().p("AitCtQhHhIAAhlQAAhlBHhHQBJhIBkAAQBmAABHBIQBIBHAABlQAABlhIBIQhHBIhmAAQhkAAhJhIg");
	this.shape_30.setTransform(77.5,54.5);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.lf(["#FF0000","#FFFF00","#00FF00","#00FFFF","#0000FF","#FF00FF","#FF0000"],[0,0.165,0.365,0.498,0.667,0.831,1],-18,0,18,0).s().p("Ah+B7Qg1gzAAhIQAAhIA1gzQA0gzBKAAQBKAAA1AzQA1AzAABIQAABIg1AzQg1A0hKAAQhKAAg0g0g");
	this.shape_31.setTransform(146,47.5);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.rf(["#FF0000","#000000"],[0,1],0,0,0,0,0,18.3).s().p("Ah7B8QgzgzAAhJQAAhIAzgzQAzgzBIAAQBIAAA0AzQAzAzAABIQAABJgzAzQg0AzhIAAQhIAAgzgzg");
	this.shape_32.setTransform(-142.5,52.5);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.rf(["#FF0000","#000000"],[0,1],0,0,0,0,0,25.2).s().p("AiwCwQhJhIAAhoQAAhmBJhKQBKhJBmAAQBoAABIBJQBKBKAABmQAABohKBIQhIBKhoAAQhmAAhKhKg");
	this.shape_33.setTransform(-69,57);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.rf(["#FF0000","#000000"],[0,1],0,0,0,0,0,21.1).s().p("AiTCVQg+g+AAhXQAAhWA+g9QA9g+BWAAQBXAAA+A+QA9A9AABWQAABXg9A+Qg+A9hXAAQhWAAg9g9g");
	this.shape_34.setTransform(7,60);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.rf(["#FF0000","#000000"],[0,1],0,0,0,0,0,25.5).s().p("AitCtQhHhIAAhlQAAhlBHhHQBJhIBkAAQBmAABHBIQBIBHAABlQAABlhIBIQhHBIhmAAQhkAAhJhIg");
	this.shape_35.setTransform(77.5,54.5);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.rf(["#FF0000","#000000"],[0,1],0,0,0,0,0,18.7).s().p("Ah+B7Qg1gzAAhIQAAhIA1gzQA0gzBKAAQBKAAA1AzQA1AzAABIQAABIg1AzQg1A0hKAAQhKAAg0g0g");
	this.shape_36.setTransform(146,47.5);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.rf(["#0000FF","#000000"],[0,1],0,0,0,0,0,18.3).s().p("Ah7B8QgzgzAAhJQAAhIAzgzQAzgzBIAAQBIAAA0AzQAzAzAABIQAABJgzAzQg0AzhIAAQhIAAgzgzg");
	this.shape_37.setTransform(-142.5,52.5);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.rf(["#0000FF","#000000"],[0,1],0,0,0,0,0,25.2).s().p("AiwCwQhJhIAAhoQAAhmBJhKQBKhJBmAAQBoAABIBJQBKBKAABmQAABohKBIQhIBKhoAAQhmAAhKhKg");
	this.shape_38.setTransform(-69,57);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.rf(["#0000FF","#000000"],[0,1],0,0,0,0,0,21.1).s().p("AiTCVQg+g+AAhXQAAhWA+g9QA9g+BWAAQBXAAA+A+QA9A9AABWQAABXg9A+Qg+A9hXAAQhWAAg9g9g");
	this.shape_39.setTransform(7,60);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.rf(["#0000FF","#000000"],[0,1],0,0,0,0,0,25.5).s().p("AitCtQhHhIAAhlQAAhlBHhHQBJhIBkAAQBmAABHBIQBIBHAABlQAABlhIBIQhHBIhmAAQhkAAhJhIg");
	this.shape_40.setTransform(77.5,54.5);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.rf(["#0000FF","#000000"],[0,1],0,0,0,0,0,18.7).s().p("Ah+B7Qg1gzAAhIQAAhIA1gzQA0gzBKAAQBKAAA1AzQA1AzAABIQAABIg1AzQg1A0hKAAQhKAAg0g0g");
	this.shape_41.setTransform(146,47.5);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AuKC6QhJhIAAhoQAAhmBJhKQBKhJBnAAQBoAABIBJQBKBKAABmQAABohKBIQhIBKhoAAQhnAAhKhKgAIxCeQhHhIAAhkQAAhmBHhHQBJhIBlAAQBmAABHBIQBIBHAABmQAABkhIBIQhHBIhmAAQhlAAhJhIg");
	this.shape_42.setTransform(4,56);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#000000").s().p("AhhDCQg+g+AAhXQAAhWA+g9QA9g+BWAAQBXAAA+A+QA9A9AABWQAABXg9A+Qg+A9hXAAQhWAAg9g9gA4gBeQgzgzAAhIQAAhJAzgzQAzgzBJAAQBIAAA0AzQAzAzAABJQAABIgzAzQg0AzhIAAQhJAAgzgzgAUhArQg1gyAAhIQAAhJA1gzQA0gzBLAAQBKAAA1AzQA1AzAABJQAABIg1AyQg1A0hKAAQhLAAg0g0g");
	this.shape_43.setTransform(2,55.5);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFF99").s().p("Ah+B7Qg1gzAAhIQAAhIA1gzQA0gzBKAAQBKAAA1AzQA1AzAABIQAABIg1AzQg1A0hKAAQhKAAg0g0g");
	this.shape_44.setTransform(146,47.5);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#663366").s().p("AiwCwQhJhIAAhoQAAhmBJhKQBKhJBmAAQBoAABIBJQBKBKAABmQAABohKBIQhIBKhoAAQhmAAhKhKg");
	this.shape_45.setTransform(-69,57);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#99FFCC").s().p("AiTCVQg+g+AAhXQAAhWA+g9QA9g+BWAAQBXAAA+A+QA9A9AABWQAABXg9A+Qg+A9hXAAQhWAAg9g9g");
	this.shape_46.setTransform(7,60);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape}]},11).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape}]},12).to({state:[{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16}]},12).to({state:[{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_16}]},12).to({state:[{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape}]},13).to({state:[{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape}]},11).to({state:[{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape}]},14).to({state:[{t:this.shape_43},{t:this.shape_42},{t:this.shape}]},10).to({state:[{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_35},{t:this.shape_32},{t:this.shape}]},13).to({state:[{t:this.shape_1},{t:this.shape_2},{t:this.shape_5},{t:this.shape_3},{t:this.shape_4},{t:this.shape}]},11).wait(1));

	// Layer_3
	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#000000").ss(1,1,1).p("AfBAfQAABppFBLQmZA9jrAeQmmA2lWAAQlWAAmhg2QjpgemXg9QpFhLAAhpQAAhZBBhGQBohxD7ggQDsgeHzAGQEXAEIiAKQDAAAD1gRQBFgEFYgcQHhgmDVAbQD6AgB2CUQBNBgAABig");
	this.shape_47.setTransform(1.2,54.4935);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#A6AEA5").s().p("Ar7EuQjpgemWg9QpGhLAAhpQAAhZBAhGQBphxD7ggQDtgeHyAGQEWAEIjAKQDAAAD1gRQBFgEFYgcQHhgmDVAbQD6AgB2CUQBNBgAABiQAABppFBLQmaA9jrAeQmlA2lWAAQlXAAmgg2g");
	this.shape_48.setTransform(1.2,54.4935);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_48},{t:this.shape_47}]}).wait(120));

	// Layer_1
	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#000000").ss(1,1,1).p("EAlRgBIQAADVq6CWQq7CXvcAAQvbAAq7iXQq6iWAAjVQAAg4Aqg8QAthABUg0QDJh+FGgGQFWgHHcADQNMADAnAAQAwAANEgDQHXgCE9AGQFGAGDKB+QBTA0AtBAQAqA8AAA4g");
	this.shape_49.setTransform(-1.5,56.7903);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#666666").s().p("A6WEjQq6iWAAjVQAAg4Aqg8QAthABUg0QDJh+FGgGQFWgHHcADINzADIN0gDQHXgCE8AGQFHAGDKB+QBSA0AuBAQAqA8AAA4QAADVq7CWQq6CXvcAAQvbAAq7iXg");
	this.shape_50.setTransform(-1.5,56.7903);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_50},{t:this.shape_49}]}).wait(120));

	// Layer_2
	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#000000").ss(1,1,1).p("AeFCkQAABPhAA2QggAah4A/QhzA9g/A5QhgBYgtCCQgyCPiXAZQh3AUjeg0QkPhKiOgkQj/hAilAAQikAAj8A1QkaBBh/AaQjbAsh0gQQiTgTgvhzQgyh9h+gyQg8gZijgWQh/gRgjgpQg0g6A6iWQAJgWA7i5QAxiWAuhlQCQk5DuhqQI2j8MeAAQMgAAI1D8QEDB0CbEGQCDDfAADOg");
	this.shape_51.setTransform(-0.5361,-12.4241);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#CCCCCC").s().p("ANQNaQkPhKiOgkQj/hAilAAQikAAj8A1QkaBBh/AaQjbAsh0gQQiTgTgvhzQgyh9h+gyQg8gZijgWQh/gRgjgpQg0g6A6iWQAJgWA7i5QAxiWAuhlQCQk5DuhqQI2j8MeAAQMgAAI1D8QEDB0CbEGQCDDfAADOQAABPhAA2QggAah4A/QhzA9g/A5QhgBYgtCCQgyCPiXAZQghAGgoAAQhrAAihgmg");
	this.shape_52.setTransform(-0.5361,-12.4241);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_52},{t:this.shape_51}]}).wait(120));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-241,-103,479,205);


(lib.Tween32 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("AtCytIZSAAMAOXAlbMg1NgANg");
	this.shape.setTransform(0.025,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF00").s().p("A6mShMANkglOIZSAAMAOXAlbg");
	this.shape_1.setTransform(0.025,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-171.3,-120.8,342.70000000000005,241.6);


(lib.Tween31 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("EgQlgoKIZSAAMAUpBQVMg6rgEQg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF00").s().p("EgdVAj7MAMwhMFIZSAAMAUpBQVg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-188.8,-258.1,377.6,516.2);


(lib.Tween30 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("EgRfgh/IW2C1MATcBBKMgxlAAAg");
	this.shape.setTransform(0.025,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF00").s().p("EgYyAiAMAHThD/IW2C1MATcBBKg");
	this.shape_1.setTransform(0.025,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-159.6,-218.6,319.29999999999995,437.2);


(lib.Tween29 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("AuX18ITOFeMAWqAmbMg3BgBAg");
	this.shape.setTransform(0.025,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF00").s().p("A7gU8MANJgq4ITOFdMAWqAmcg");
	this.shape_1.setTransform(0.025,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-177.1,-141.5,354.29999999999995,283);


(lib.Tween24 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("AlSDPQv1qLvyqaQglgcCMhIQCJhLDmhNQDmhNC7giQC6glAlAcQALAIcwO9QcgO+g4AfQgfAQgJBOQgGAyADB4QACCEgEA3QgGBjgbAuQhDBzi5CUQi3CThgARIgCAAQhiAA/N0Ig");
	this.shape.setTransform(0.0456,0.0055);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-236.9,-149.5,473.9,299);


(lib.Tween23 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("AlSDPQv1qLvyqaQglgcCMhIQCJhLDmhNQDmhNC7giQC6glAlAcQALAIcwO9QcgO+g4AfQgfAQgJBOQgGAyADB4QACCEgEA3QgGBjgbAuQhDBzi5CUQi3CThgARIgCAAQhiAA/N0Ig");
	this.shape.setTransform(0.0456,0.0055);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-236.9,-149.5,473.9,299);


(lib.Tween22 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("AjeDPQpRlwqdmoQo0llgPgMQglgcCLhJQCKhKDmhNQDlhNC8gjQC6glAlAcQALAJYGMBQX2MCg4AfQggAQgQBMQgMAygKBzQgLCHgIAvQgOBigbAtQgcAxgfBdQgRA1ghBsQg/C+hKANIgCAAQhjAA8Xxtg");
	this.shape.setTransform(0.0113,0.0316);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-207.2,-133.9,414.4,267.9);


(lib.Tween21 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("AXPLcIl/gCQhhAAlHgMQlQgKhIgIQg8gEv6qwQkmjGmSkSIlDjdQgkgUBngNQBlgOCzAAQCyAACWAOQCWANAkAUQLWFkLDFfQWAK9grAFQgdAFioAAIiWgBg");
	this.shape.setTransform(53.6848,31.3264,1,1.4695);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-129.8,-76.3,367,215.3);


(lib.Tween20 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("AIEChInoAAIoDgDQmXgChMgCQhDgBD7iXQBIgrBrg9IBVgwQgBgFChgDQCigDDkAAQDkAACiADQCjADAAAFQB9BOBvBNQDcCahDABQgyACjnAAIitgBg");
	this.shape.setTransform(-0.009,0.0063);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-98.5,-16.1,197.1,32.3);


(lib.Tween19 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("EgGDAy0QmggOjWgvQj+g5h5iCQh/iHgVkAQgUj3BImvQA8llBKoIIB8t3QCUwXBqm3QEUx1EWmSQEXmTGJAAQGKAAEXGTUAEXAGSADwAqgUADwAqhgE+ACYQihBOm0AeQkBARlXAAQj9AAktgKg");
	this.shape.setTransform(-0.0037,-0.0138);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-154.5,-326.2,309.1,652.4);


(lib.Tween18 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("AqgBvQkXguABhBQgBhAEXguQEXguGJAAQGKAAEWAuQEYAugBBAQABBBkYAuQkWAumKAAQmJAAkXgug");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-15.7,190.3,31.4);


(lib.Tween17 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("AgZgnQAABCgkBEQgVApg6BLQg8BNgWArQgnBGgEBHQgHB6jJA1QiQAmjkAAQhLAAg5hDQghgng2hoQgyhigfggQg0g0hEAWQhcAfhXhaQhahfAAiRQAAh7B9hnQAegaBZg+QBRg4AsgnQAygsAwhMQAbgpAshIQAkg0AhgFQAogGA0A4QA0A4BQAHQAzAEBkgRQBrgTAtACQBSACA1AvQDwDUAAEsgAX/E1IjIEsIm3gUIpXAFIj4nVIAbsQIHgFeIJDhkIFnjhIhFI/g");
	this.shape.setTransform(0,-0.004);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#006600").s().p("ABnJnIpWAEIj3nVIAbsQIHfFfIJDhkIFnjiIhFI/IBuFxIjIEsg");
	this.shape_1.setTransform(79.225,-2.55);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#666666").s().p("AjFJqQghgng1hoQgyhiggggQgzg0hEAWQhdAfhXhaQhZhfgBiRQAAh7B9hnQAfgaBZg+QBRg4AsgnQAxgsAxhMIBGhxQAlg0AhgFQAngGA0A4QA0A4BPAHQAzAEBkgRQBrgTAtACQBSACA1AvQDwDUAAEsQABBCglBEQgVApg6BLQg7BNgXArQgmBGgFBHQgGB6jKA1QiQAmjiAAQhLAAg6hDg");
	this.shape_2.setTransform(-78.05,-0.004);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-154.5,-69.4,309,138.9);


(lib.Tween16 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("AX/E1IjIEsIm4gUIpXAFIj3nVIAbsQIHfFeIJEhkIFnjhIhFI/gAgZgnQAABCgkBEQgWApg6BLQg7BNgXArQgmBGgEBHQgHB6jKA1QiQAmjjAAQhLAAg5hDQgigng1hoQgyhiggggQgzg0hEAWQhdAfhWhaQhahfAAiRQAAh7B9hnQAegaBZg+QBRg4AsgnQAxgsAxhMQAbgpAshIQAkg0AhgFQAngGA1A4QA0A4BPAHQAzAEBlgRQBrgTAtACQBSACA1AvQDwDUAAEsg");
	this.shape.setTransform(0,-0.004);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#666666").s().p("AjFJqQghgng1hoQgyhiggggQgzg0hEAWQhdAfhXhaQhZhfgBiRQAAh7B9hnQAfgaBZg+QBRg4AsgnQAxgsAxhMIBGhxQAlg0AhgFQAngGA0A4QA0A4BPAHQAzAEBkgRQBrgTAtACQBSACA1AvQDwDUAAEsQABBCglBEQgVApg6BLQg7BNgXArQgmBGgFBHQgGB6jKA1QiQAmjiAAQhLAAg6hDg");
	this.shape_1.setTransform(-78.05,-0.004);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#006600").s().p("ABnJnIpWAEIj3nVIAbsQIHfFeIJDhjIFnjiIhFI/IBuFxIjIErg");
	this.shape_2.setTransform(79.225,-2.55);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-154.5,-69.4,309,138.9);


(lib.Tween15 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("AgZgnQAABCgkBEQgVApg6BLQg7BNgXArQgnBGgEBHQgHB6jJA1QiQAmjjAAQhLAAg6hDQghgng2hoQgyhigfggQg0g0hDAWQhdAfhXhaQhahfAAiRQAAh7B9hnQAegaBag+QBQg4AtgnQAxgsAwhMQAbgpAshIQAlg0AggFQAogGA0A4QA0A4BQAHQAzAEBkgRQBrgTAtACQBSACA1AvQDwDUAAEsgAX/E1IjIEsIm3gUIpYAFIj2nVIAasQIHgFeIJDhkIFnjhIhFI/g");
	this.shape.setTransform(0,-0.004);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#006600").s().p("ABnJnIpWAEIj3nVIAbsQIHfFeIJDhjIFnjiIhFI/IBuFxIjIErg");
	this.shape_1.setTransform(79.225,-2.55);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#666666").s().p("AjFJqQghgng1hoQgyhiggggQgzg0hEAWQhdAfhXhaQhZhfgBiRQAAh7B9hnQAfgaBZg+QBRg4AsgnQAxgsAxhMIBGhxQAlg0AhgFQAngGA0A4QA0A4BPAHQAzAEBlgRQBqgTAtACQBTACA0AvQDwDUAAEsQABBCglBEQgVApg6BLQg7BNgXArQgmBGgFBHQgGB6jKA1QiQAmjiAAQhLAAg6hDg");
	this.shape_2.setTransform(-78.05,-0.004);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-154.5,-69.4,309,138.9);


(lib.Tween14 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("AgZgnQAABCgkBEQgVApg6BLQg7BNgXArQgnBGgEBHQgHB6jKA1QiPAmjjAAQhLAAg6hDQghgng1hoQgyhiggggQg0g0hDAWQhdAfhXhaQhahfAAiRQAAh7B9hnQAfgaBZg+QBRg4AsgnQAxgsAwhMQAbgpAshIQAlg0AggFQAogGA0A4QA0A4BPAHQA0AEBkgRQBrgTAtACQBSACA1AvQDwDUAAEsgAX/E1IjIEsIm4gUIpWAFIj3nVIAasQIHfFeIJEhkIFnjhIhFI/g");
	this.shape.setTransform(0,-0.004);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#006600").s().p("ABnJnIpWAEIj3nVIAbsQIHfFeIJDhjIFnjiIhFI/IBuFxIjIErg");
	this.shape_1.setTransform(79.225,-2.55);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#666666").s().p("AjFJqQghgng1hoQgyhiggggQgzg0hEAWQhdAfhXhaQhahfAAiRQABh7B8hnQAfgaBZg+QBRg4AsgnQAxgsAxhMIBHhxQAkg0AhgFQAngGA1A4QA0A4BOAHQAzAEBkgRQBsgTAsACQBSACA1AvQDxDUAAEsQgBBCgjBEQgWApg6BLQg7BNgXArQgnBGgEBHQgGB6jKA1QiQAmjiAAQhLAAg6hDg");
	this.shape_2.setTransform(-78.05,-0.004);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-154.5,-69.4,309,138.9);


(lib.Tween13 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("EAxvgAbQAABDgkBDQgWApg5BMQg8BMgXArQgmBGgEBIQgHB5jJA1QiRAmjjAAQhLAAg5hDQghgng1hoQgyhiggggQgzg0hFAXQhdAehWhaQhaheAAiSQAAh7B9hnQAfgaBYg9QBSg5ArgnQAygrAxhMQAagqAshIQAkgzAhgFQAngHA1A5QA0A3BQAHQAyAEBlgRQBrgSAtABQBSACA1AvQDwDUAAEsgA6hEQIjIEsIm3gUIpXAFIj3nVIAasQIHgFeIJDhkIFojiIhGI/g");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#006600").s().p("ABnJmIpWAGIj3nWIAbsQIHfFeIJDhkIFnjhIhFI/IBuFwIjIEsg");
	this.shape_1.setTransform(-244.025,-6.25);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#666666").s().p("AjEJqQgigng1hoQgyhiggggQgzg0hEAWQhdAfhWhaQhbhfABiRQgBh7B+hnQAegaBZg+QBRg4AsgnQAxgsAxhMIBGhxQAlg0AggFQAogGA0A4QA1A4BOAHQAzAEBlgRQBqgTAuACQBSACA1AvQDwDUgBEsQAABCgkBEQgVApg6BLQg7BNgXArQgnBGgDBHQgIB6jJA1QiQAmjiAAQhLAAg5hDg");
	this.shape_2.setTransform(242.85,1.246);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-319.3,-70.7,638.6,141.4);


(lib.Tween12 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("EAxvgAbQAABDgkBDQgVApg6BMQg7BMgXArQgnBHgEBHQgHB5jJA1QiQAmjjAAQhLAAg6hDQghgng1hoQgyhiggggQgzg0hEAXQhdAfhXhbQhaheAAiSQAAh7B9hnQAfgaBZg9QBRg5AsgnQAxgrAxhMQAagqAshIQAlgzAggFQAogHA0A5QA0A3BQAHQAzAEBkgRQBrgSAtABQBSACA1AvQDwDUAAEsgA6gEQIjIEsIm4gUIpXAFIj3nVIAbsQIHfFeIJEhkIFnjiIhFI/g");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#006600").s().p("ABnJmIpWAFIj3nVIAbsQIHfFeIJDhkIFnjhIhFI/IBuFxIjIErg");
	this.shape_1.setTransform(-244.025,-6.25);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#666666").s().p("AjFJqQghgng1hoQgyhiggggQgzg0hEAWQhdAfhWhaQhahfAAiRQAAh7B8hnQAfgaBZg+QBRg4AsgnQAxgsAxhMIBGhxQAlg0AggFQAogGA0A4QA0A4BPAHQAzAEBlgRQBqgTAuACQBSACA1AvQDvDUAAEsQABBCglBEQgVApg6BLQg7BNgXArQgnBGgDBHQgIB6jJA1QiQAmjiAAQhLAAg6hDg");
	this.shape_2.setTransform(242.85,1.246);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-319.3,-70.7,638.6,141.4);


(lib.Tween11 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("EAxvgAaQAABCgkBDQgVApg6BMQg7BNgXAqQgnBHgEBHQgHB5jJA1QiQAmjjAAQhLAAg6hDQghgng1hoQgyhiggggQg0g0hDAXQhdAfhXhbQhaheAAiSQAAh7B9hnQAfgZBZg+QBRg5AsgnQAxgrAxhMQAagqAshIQAlgzAggFQAogGA0A4QA0A3BQAHQAzAFBkgSQBrgSAtABQBSACA1AvQDwDVAAEsgA6hEQIjIEsIm3gUIpXAFIj3nVIAasQIHgFeIJDhkIFnjhIhFI/g");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#006600").s().p("ABnJmIpWAGIj3nWIAbsQIHfFeIJDhkIFnjhIhFI/IBuFxIjIErg");
	this.shape_1.setTransform(-244.025,-6.25);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#666666").s().p("AjEJqQgigng1hoQgyhiggggQgzg0hEAWQhdAfhWhaQhbhfABiRQAAh7B9hnQAegaBZg+QBRg4AsgnQAxgsAxhMIBHhxQAkg0AggFQAogGA1A4QAzA4BPAHQAzAEBlgRQBrgTAtACQBRACA2AvQDwDUAAEsQgBBCgjBEQgWApg6BLQg7BNgXArQgnBGgDBHQgIB6jJA1QiQAmjiAAQhLAAg5hDg");
	this.shape_2.setTransform(242.85,1.246);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-319.3,-70.7,638.6,141.4);


(lib.Tween10 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("EAxvgAbQAABCgkBEQgVApg6BLQg7BNgXArQgnBGgEBHQgHB6jJA1QiQAmjjAAQhLAAg6hDQghgng1hoQgyhiggggQg0g0hDAXQhdAehXhaQhahfAAiRQAAh7B9hnQAfgaBZg+QBRg4AsgnQAxgsAxhMQAagpAshIQAlg0AggFQAogGA0A4QA0A4BQAHQAzAEBkgRQBrgTAtACQBSACA1AvQDwDUAAEsgA6hEQIjIEsIm3gUIpXAFIj3nVIAasQIHgFeIJDhkIFnjiIhFI/g");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#006600").s().p("ABnJmIpWAGIj3nWIAbsQIHfFeIJDhkIFnjhIhFI/IBuFwIjIEsg");
	this.shape_1.setTransform(-244.025,-6.25);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#666666").s().p("AjEJqQgigng1hoQgyhiggggQgzg0hEAWQhdAfhWhaQhbhfABiRQAAh7B9hnQAegaBZg+QBRg4AsgnQAxgsAxhMIBHhxQAkg0AggFQAogGA1A4QAzA4BPAHQAzAEBlgRQBrgTAtACQBRACA2AvQDwDUAAEsQgBBCgjBEQgWApg6BLQg7BNgXArQgnBGgDBHQgIB6jJA1QiQAmjiAAQhLAAg5hDg");
	this.shape_2.setTransform(242.85,1.246);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-319.3,-70.7,638.6,141.4);


(lib.Tween9 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("EAxvgAbQAABCgkBEQgWApg5BLQg7BNgYArQgmBGgEBHQgHB6jKA1QiQAmjiAAQhLAAg6hDQghgng2hoQgyhigfggQg0g0hDAXQheAehWhaQhahfAAiRQAAh7B9hnQAegaBag+QBQg4AtgnQAxgsAxhMQAagpAshIQAlg0AggFQAngGA1A4QA0A4BPAHQAzAEBlgRQBrgTAtACQBSACA1AvQDwDUAAEsgA6hEQIjIEsIm3gUIpXAFIj3nVIAasQIHgFeIJDhkIFnjiIhFI/g");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#006600").s().p("ABnJnIpWAEIj3nVIAbsQIHfFfIJDhkIFnjiIhFI/IBuFxIjIErg");
	this.shape_1.setTransform(-244.025,-6.25);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#666666").s().p("AjFJqQghgng1hoQgyhiggggQgzg0hEAWQhdAfhXhaQhZhfgBiRQAAh7B9hnQAfgaBZg+QBRg4AsgnQAxgsAxhMIBGhxQAlg0AhgFQAngGA0A4QA0A4BPAHQAzAEBlgRQBqgTAtACQBTACA0AvQDwDUAAEsQABBCglBEQgVApg6BLQg7BNgXArQgmBGgFBHQgGB6jKA1QiQAmjiAAQhLAAg6hDg");
	this.shape_2.setTransform(242.85,1.246);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-319.3,-70.7,638.6,141.4);


(lib.Tween8 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("EAxvgAbQAABCgkBEQgWApg5BLQg8BNgXArQgmBGgEBHQgHB6jKA1QiQAmjjAAQhLAAg5hDQgigng1hoQgyhiggggQgzg0hEAXQhdAehWhaQhahfAAiRQAAh7B9hnQAegaBZg+QBRg4AsgnQAxgsAxhMQAbgpAshIQAkg0AhgFQAngGA1A4QA0A4BPAHQAzAEBlgRQBrgTAtACQBSACA1AvQDwDUAAEsgA6hEQIjIEsIm3gUIpXAFIj3nVIAasQIHgFeIJDhkIFnjiIhFI/g");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#006600").s().p("ABnJnIpWAEIj3nVIAbsPIHfFeIJDhkIFnjiIhFI/IBuFxIjIEsg");
	this.shape_1.setTransform(-244.025,-6.25);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#666666").s().p("AjFJqQghgng1hoQgyhiggggQgzg0hEAWQhdAfhXhaQhZhfgBiRQAAh7B9hnQAfgaBZg+QBRg4AsgnQAxgsAxhMIBGhxQAlg0AhgFQAngGA0A4QA0A4BPAHQAzAEBlgRQBqgTAtACQBSACA1AvQDwDUAAEsQABBCglBEQgVApg6BLQg7BNgXArQgmBGgFBHQgGB6jKA1QiQAmjiAAQhLAAg6hDg");
	this.shape_2.setTransform(242.85,1.246);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-319.3,-70.7,638.6,141.4);


(lib.Title_btn = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Ah2CDQgigyAAhLQAAhaAyg2QAxg2BBAAQA4AAApAtQAoAuAEBZIjIAAQAEBHAjArQAaAgAmAAQAXAAATgNQATgNAWgiIAOAIQgeA9gkAZQgkAagvAAQhQAAgqg/gAgaiTQgXAiAAA9IAAAOIBpAAQAAhAgGgXQgHgXgPgMQgIgIgOABQgTgBgNAVg");
	this.shape.setTransform(103.075,4.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AhiEJIAAgOQAYgBAMgNQAIgJAAgnIAAl5QAAgmgJgLQgJgLgagBIAAgPICYAAIAAHFQABAnAIALQAJAKAaACIAAAOg");
	this.shape_1.setTransform(74.05,-3.925);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgmDpQgZgUgGgXQgEgNAAg7IAAjEIgwAAIAAgOQAxgjAjgnQAiglAZgwIANAAIAACGIBXAAIAAAnIhXAAIAADgQAAAgADAJQADAJAHAGQAIAFAGAAQAZAAAXgnIAMAJQggBLhHAAQgiAAgXgTg");
	this.shape_2.setTransform(49.525,-2.15);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AhhEPIAAgOQAYgBALgNQAIgJAAgnIAAjUQAAgmgJgMQgJgKgZgCIAAgOICYAAIAAEgQgBAnAKALQAJAKAaACIAAAOgAgqinQgRgSgBgYQABgZARgSQASgSAYAAQAZAAASASQASASAAAZQAAAYgSASQgSARgZAAQgYAAgSgRg");
	this.shape_3.setTransform(25.15,-4.55);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AiKEJIAAgPIASAAQAXAAAOgIQAKgFAGgOQAEgJAAgoIAAmXIgmAAQg2AAgYAWQgiAggJA6IgPAAIAAiPIHbAAIAACPIgOAAQgNgxgPgWQgPgWgbgMQgOgHgmAAIgnAAIAAGXQAAApAFAKQAEAKANAIQANAHAXAAIARAAIAAAPg");
	this.shape_4.setTransform(-11.575,-3.925);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(1,1,1).p("Egg9gLPMBB7AAAIAAWfMhB7AAAg");
	this.shape_5.setTransform(41,-3.975);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#00FF00").s().p("Egg9ALQIAA2fMBB7AAAIAAWfg");
	this.shape_6.setTransform(41,-3.975);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-171,-76.9,424,145.9);


(lib.Title = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("AAJCGIAAgKQAPgCAGgKQAFgHgBgcIAAhyQAAgggCgJQgDgJgGgDQgFgFgIgBQgXAAgUAjIAACKQgBAdAGAIQAFAIAPACIAAAKIiEAAIAAgKQARgCAHgIQAEgHAAgeIAAiSQAAgdgEgIQgHgGgRgDIAAgKIBrAAIAAAiQATgWASgKQATgKAUAAQAaAAAQAOQARAOAGAVQAEAQAAAtIAABkQAAAeAGAIQAFAHARACIAAAKg");
	this.shape.setTransform(112.6,38.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF00").s().p("AhZBgQgggpAAg2QAAg3AhgpQAggqA4AAQAiAAAcARQAeASAPAhQAPAfAAAmQAAA3gcAmQgiAtg7AAQg7AAgfgqgAgWhqQgLAKgDAgQgDAfgBA3QAAAeAEAaQAEAUAKALQAKAJAMABQAMAAAJgHQAKgKAEgRQAFgaABhQQgBgvgFgSQgFgRgKgJQgIgFgLgBQgNAAgKAMg");
	this.shape_1.setTransform(82.35,38.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFF00").s().p("AhFDBIAAgKQARAAAJgKQAFgGAAgcIAAiXQAAgbgGgIQgHgIgSgBIAAgKIBsAAIAADNQAAAcAGAIQAHAHASABIAAAKgAgdh3QgNgMAAgSQAAgSANgMQAMgNARAAQASAAANANQAMAMAAASQAAASgMAMQgNANgSAAQgRAAgMgNg");
	this.shape_2.setTransform(60.425,32.475);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFF00").s().p("AgbCmQgRgOgFgRQgDgKAAgpIAAiMIgiAAIAAgKQAjgYAZgcQAZgbARghIAJAAIAABfIA+AAIAAAbIg+AAIAACgQAAAWACAHQACAGAGAEQAFAFAEAAQATgBAQgbIAIAGQgXA1gyAAQgYAAgRgNg");
	this.shape_3.setTransform(42.975,34.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFF00").s().p("AhNBjQgfgnAAg4QAAg0AcgoQAjgxA7AAQAoAAAYAVQAYATAAAaQAAAPgKAKQgKAKgPgBQgRAAgKgLQgLgLgCgbQgCgRgHgIQgGgGgJgBQgMAAgJAPQgPAVAAAsQAAAlAMAhQALAhAUARQAPAMAVgBQAOAAAMgGQAMgHARgRIAIAGQgSAjgbARQgcARgfAAQgzAAgfgng");
	this.shape_4.setTransform(21.025,38.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFF00").s().p("AhSB4QgRgNgHgSQgFgSAAgtIAAhnQgBgdgFgIQgFgGgRgDIAAgKIBpAAIAACxQAAAbAEAJQACAHAGAFQAFAEAIAAQAJAAAIgGQALgGAPgWIAAiLQABgdgGgIQgGgGgQgDIAAgKIBqAAIAADLQgBAeAGAHQAGAHARACIAAAKIhrAAIAAgjQgSAXgRAKQgUAKgXAAQgVAAgRgOg");
	this.shape_5.setTransform(-7.95,39.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFF00").s().p("AAfCXQgTAYgPAJQgQAJgTAAQgxAAgegtQgXgkAAg1QAAgqAOggQAPghAagRQAZgRAdAAQASAAAOAHQAOAHAQASIAAhOQAAgegCgGQgEgIgGgEQgHgEgRAAIAAgMIByAAIAAErQAAAeACAGQACAJAHAEQAGAFAPABIAAAJIhuAWgAgjgxQgKAIgHAWQgGAVAAAtQAAAxAHAYQAHAXANALQAGAFALAAQAXAAAWgmIAAiIQgVgogcAAQgLAAgGAGg");
	this.shape_6.setTransform(-39.15,33.325);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFF00").s().p("AgYC6QgQgHgQgPIgrAcIgJAAIAAlKQAAgWgCgFQgDgIgGgEQgGgEgOAAIAAgLIBsAAIAACQQAggiAnAAQAaAAAYAQQAYAPANAdQANAcAAAmQAAArgRAiQgRAjgcASQgcATglAAQgUAAgRgHgAgfgYIAABzQAAAkABAJQADARALAKQAKAKAQAAQAOAAAKgIQAKgIAHgYQAHgYAAg+QAAg7gPgWQgLgRgSAAQgXAAgWAbg");
	this.shape_7.setTransform(-71.925,33.325);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFF00").s().p("AASDBIAAgLIAHAAQAWAAAJgGQAGgEAAgJIgBgKIgJgVIgUgvIiEAAIgQAlQgIASAAAMQAAAQANAIQAIAEAdACIAAALIh9AAIAAgLQAUgDANgNQANgPATgrICGkrIAGAAICIE0QATAsANALQAJAJARABIAAALgAhaBAIBxAAIg3iAg");
	this.shape_8.setTransform(-107.825,32.55);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFF00").s().p("AhUBdQgYgkAAg1QAAhAAkgmQAjgnAuAAQAnAAAeAgQAdAhACBAIiOAAQADAzAZAeQASAWAbAAQARAAANgJQAOgJAPgYIAKAGQgVArgaASQgZASgiAAQg5AAgegtgAgShpQgQAZAAArIAAAKIBKAAQAAgtgEgQQgFgRgLgJQgFgFgKAAQgOAAgJAOg");
	this.shape_9.setTransform(35.275,-26.35);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFF00").s().p("AAJC9IAAgKQANgCAHgJQAFgIABgcIAAh0QAAgfgDgIQgCgJgHgEQgFgFgIAAQgKAAgLAIQgKAIgMATIAACKQAAAcADAHQAGAKARACIAAAKIiFAAIAAgKQARgCAHgJQAFgGgBgeIAAkHQAAgegEgHQgHgHgRgCIAAgLIBrAAIAACXQAVgXARgJQARgJAUAAQAYAAASAOQARAOAGASQAGATgBAsIAABlQABAeAFAIQAGAHAQACIAAAKg");
	this.shape_10.setTransform(6.5,-32.225);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFF00").s().p("AhiC9IAAgLIAMAAQARAAAKgFQAHgEAEgKQAEgGAAgdIAAkiIgcAAQgmAAgRAQQgZAXgGApIgLAAIAAhmIFTAAIAABmIgKAAQgJgjgLgPQgLgQgTgJQgLgFgaAAIgcAAIAAEiQAAAdADAHQADAIAKAFQAJAFAQAAIANAAIAAALg");
	this.shape_11.setTransform(-28.5,-32.225);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Title, new cjs.Rectangle(-359.8,-66.1,719.7,132.3), null);


(lib.Rock = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("AfGIwQhqAGhiBtQhiBtiNBPQlPiKmiBpQmiBpkRAAQkQAAingVQiKgRhNAAQg/AAi8ANQjEAMhrACQl3AEh4huQiAh1jglBQjflBhQjpQhPjqHZhpQHZhoBEhKQBFhLAogWQAogWBcA/QBdBABghOQC3jnBeBNQBfBNAeggQAeggArAZQArAZDwguQDwgtAIAoQAIAoCZghQCZghB+BYQB/BYDIgkQDHgjKMEGQKMEGiRGrQiSGrhpAGg");
	this.shape.setTransform(69.0902,10.1799);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#666666").s().p("AlQOSQiKgRhNAAQg/AAi8ANQjEAMhrACQl3AEh4huQiAh1jglBQjflBhQjpQhPjqHZhpQHZhoBEhKQBFhLAogWQAogWBcA/QBdBABghOQC3jnBeBNQBfBNAeggQAeggArAZQArAZDwguQDwgtAIAoQAIAoCZghQCZghB+BYQB/BYDIgkQDHgjKMEGQKMEGiRGrQiSGrhpAGQhqAGhiBtQhiBtiNBPQlPiKmiBpQmiBpkRAAQkQAAingVg");
	this.shape_1.setTransform(69.0902,10.1799);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-158.6,-84.3,455.5,189);


(lib.Road = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#663300").s().p("EhlfAOyIAA9jMDK/AAAIAAdjg");
	this.shape.setTransform(649.575,94.625);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1299.2,189.3);


(lib.Prev = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(30,1,1).p("AxpgTIAKAAILkLjAxpgTIK8q8AyRgTIAoAAAxfgTMAjxAAAAx9AAIAUgT");
	this.shape.setTransform(34.5,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-97.5,-87,264,174);


(lib.Play_Button = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AhuCzQgPgOAAgTQAAgPAKgMQAJgKAPAAQAOAAAIAJQAJAJAAATQABAKACADQACADAEABQAHAAAIgJQALgLAPgqIAIgWIhVjAQgSgsgKgKQgJgKgMgDIAAgKICJAAIAAAKQgMAAgGAFQgGAEAAAIQAAAKAOAfIAsBmIAehPQAQgqAAgRQAAgKgGgGQgHgGgRAAIAAgKIBWAAIAAAKQgMABgIAJQgIAIgTAxIhKDAQgbBIgOARQgUAYgcgBQgXABgOgNg");
	this.shape.setTransform(40,10.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAcB+QgNgLgDgVQguAqgmAAQgXAAgOgPQgPgOAAgWQAAgeAagYQAagYBUgmIAAgaQAAgegCgHQgEgIgIgGQgJgGgLAAQgSAAgMAIQgHAFAAAHQAAAGAIAJQALAMgBALQABANgLAKQgKAKgRAAQgRAAgLgLQgNgLAAgOQAAgUAQgSQAQgSAcgKQAdgKAeAAQAkAAAWAQQAVAQAHASQAEAMAAAqIAABkQAAASACAEQABAFACACQAEACADAAQAHAAAIgKIAIAHQgOAUgPAKQgPAJgSAAQgXAAgMgKgAglAdQgLAPAAAPQABANAIAKQAIAHAMAAQAOAAARgPIAAhXQggATgRAXg");
	this.shape_1.setTransform(12.35,3.825);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AhFC9IAAgKQARgBAIgJQAGgHAAgbIAAkNQAAgbgHgHQgGgIgSgBIAAgLIBsAAIAAFDQAAAbAGAIQAHAIASABIAAAKg");
	this.shape_2.setTransform(-10.325,-1.825);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AidC9IAAgLQAaAAAJgEQAKgFADgIQAEgIAAgeIAAj0QAAgfgEgIQgDgIgKgFQgJgEgaAAIAAgLICjAAQBRAAAkAdQAjAdAAAsQAAAkgXAbQgWAZgpAJQgbAHg/AAIAABpQAAAeAEAIQAEAIAJAFQAJAEAZAAIAAALgAgSgDIAOABQAhAAASgUQATgVAAgqQAAgqgTgTQgSgUgjAAIgMAAg");
	this.shape_3.setTransform(-36.075,-1.825);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(1,1,1).p("A0YnpMAoxAAAIAAPTMgoxAAAg");
	this.shape_4.setTransform(0.5,1.025);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#00FF00").s().p("A0YHqIAAvTMAoxAAAIAAPTg");
	this.shape_5.setTransform(0.5,1.025);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-131,-48.9,263,99.9);


(lib.Next = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(30,1,1).p("ARqAUIgKAAMgjxAAAASSAUIgoAAIq8K8AR+AAIgUAUARgAUIrkrj");
	this.shape.setTransform(-87,-4.95);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-219,-91.9,264,174);


(lib.Mute_Button = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(17,1,1).p("ALavcIgGAIQAjAjAiAnQFJF3AAITQAAIUlJF4QgaAdgbAcQgeAfggAcQkkEDmCAAQnQAAlIl3QlJl4AAoUQAAoTFJl3QFIl4HQAAQGhAAEzEuIiQCwIxpVrIjSEB");
	this.shape.setTransform(4.975,-7.025);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(1,1,1).p("ArvjWIWwp2IAvgUIAAZ7IAABGIg6gQIxfkyIlDhYg");
	this.shape_1.setTransform(-7.5,-3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#999999").s().p("AK2NRIxekyIRo1rIAwgUIAAZ6IAABHgArsHHIgCqdIWup2IxoVrgALAtMg");
	this.shape_2.setTransform(-7.5,-3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-115.7,-143.8,241.4,273.6);


(lib.Human = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_12
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("AETjCQAAAjgTAZQgTAZgbAAQgbAAgTgZQgTgZAAgjQAAgjATgZQATgZAbAAQAbAAATAZQATAZAAAjgADIjRQAAABAAABQAFAAADABIABAAQADAAABADQAAAAABAAIAAABQABABABACQABAAACABIAAABQgBAAAAAAIAAABIABAAIACAFIgCACQgCAAgBAAQgBAAgBAAIAAACIAAADQgBAAgCAAQgBAAgBAAIAAACQgBAAAAAAIAAAAQAAAAAAABQAAAAgBABIAAABQgBAAgBACIgEAAQgBAAAAgCQAAgBAAAAQgBgCAAgBQgBAAAAgBQgCAAgBgBQgBgBgBAAQgBAAgBgBQgBAAgBAAQAAgBAAAAQgCgCgBgBQAAgBAAgBQABgBAAgBQgBgBAAAAQgBgBAAgFQAAgCABAAQAAgCAEAAQAEgBADAAIABAAQABAAABAAQAAACABADQAAAAAAAAQAAAAACAAIAAgBIAAgDIAFAAIAGgDIAAAGQACgBADAAQAAACAAACIAAACQABAAABABQABABABADIADABQAEADgBACQAAADgFAAQgEABgGABIAAAAIAAABQgBACgDAAQgBAAAAAAIAAAAQgBAAgBAAIAAAAQgEgBgDAAQgIgCgGgCQgCAAAAgDQgDgDAEgCQAAAAABgBQABAAACgBIAAAAQABgDABgCIADAAIABAAQACgBACgBQABgBABAAQABgBABAAQABgBABAAIADgCQACABABABQABAAABABQAAAAABABIAAACQABABADAAIAAACIABABIACACIACABQAAAAAAABIgCABIAAACQACAAAAADQgDACgBAAQgCABgDABQgCAAgBAAQgBAAgBAAIAAAAIAAADQgBAAgBAAIAAgBQgCgBgBgBQgBgBgBAAQAAgBgBAAQAAAAAAAAQgCgCAAgBIgBAAIAAAAQgBgBgBgBQAAgBgBAAQAAgCAAgCQAAAAAAAAIAAgBQAAAAAAgBQABAAAAgBQABAAABAAIAAAAQAAAAAAgBQAAAAABgBQABgBABAAQAAABAAAAQAAABAAABQABAAgBAAQABAAABAAIAAgBIAAgBQADAAACAAQAAAAABAAIAAAAIAAACQAAABABAAQABABADADQAAAAAAAAQAAABAAAAIAAACIAAAAQgBABgBAAQAAAAgBAAQAAAAAAABQgCAAAAAAQgDgBgCAAQAAAAAAABQAAABABABQAAAAAAAAIAAABQgBAAgBAAQgBAAgBAAQAAAAgBAAQgBAAAAAAQgBgBAAAAQAAgBAAgBQgBAAgBAAQgBAAAAAAQAAAAgBAAQgCAAgBAAQgBAAgBgBQAAgDAAgDQAAAAAAAAQgBAAgBgBQABgCACgCQAFgGACgCADGjPQABgBABgBQABAAACAAQADAAACACQAAAAAAABQAAABABACQACAAACAAAC+jCQAAgCABgBIADgCIACAAIACAAIAAAAIABAAQABAAABAAQABAAABAAIAAgCIAAgBIAAAAQAAAAABAAIABAAQAAAAABAAQABAAACABIAAACQACAAACAAIABAAQABAAABAAIACADQgBABgBACQAAAAAAAAIAAABQgBABAAAAQAAABABABQABABACABQgCAAgBAAQAAADgBAAQAAAAgBAAIAAACAC9jCIAAAAQAAAAABAAIAAAAQADgBACgBQAAgCABgBADCjCIABAAIADgBQAAgBAAgBQAAAAAAAAQAAgBAAgBQAAgBABgBQABAAAAAAQAAAAABgBIAAABQABAAABgBQABAAABAAQACgBACAAQgBABABAAIACAAQABABABABQAAAAAAABQAAAAABAAQAAABAAABIAAAFIAAABQgCABgBACQgBAAgBABIAAABIAAACQgBABAAABQgBAAgBAAQgCgBgCAAQgBgBgBAAQgBAAAAAAIAAAAQgBAAAAAAQgCgBgBAAQgBgBAAgBQAAAAgBgBQAAAAAAgBQgCAAgBgDQAAAAAAgBQAAgBAAAAADCjBQAAAAAAgBIAAAAQABAAAAgBQABgBAAgBQABAAABAAQAAgBAAAAIAAgBQABgBABgBQAAAAABAAQAAAAAAABQABgBABAAQABgBACAAQABAAACAAQAAAAAAABQAAAAABAAQABAAACABQAAAAABAAIAAABIAAAAQAAAAABAAQACACgBADIAAABQAAAAAAABQAAABAAACQAAABAAABQgBAAgBAAQgCAAgBAAQgBAAAAABQgBAAAAAAQgCABgBAAQgBAAAAAAQgBABgBAAQgBAAgBAAIAAAAQgCgBgBAAQgBAAgBgBQAAAAAAgBQgBgBAAgCIAAAAQgBgCAAgBQAAAAAAgBADBi/QAAgBABgBADBi/QgBAAgBgBQgBAAAAgBQgBAAAAgBADBi8QAAgBAAAAQAAgBAAgBADDi+QgBAAgBgBADGjDQAAgBAAgBQACAAABAAQABgBABAAQADAAADgBIAFAAADGjDIADgCIAAAAQABACABACQAAACABADQgBAAAAgBIAAgEIAAgEQADAAADAAIAAAKQgDAAgCAAQgBAAgCAAQgBAAAAAAQgBAAAAAAQgBAAAAAAQAAgBAAgBQgBAAgBAAIgBgBADHi8QAAgBAAgCQgBgCAAgCADHi8QAAAAgBgBQAAgCAAgDQAAgBAAAAADTi7IAAAAQgBAAAAABQgBABAAABQgCAAgBgBQgBAAAAAAQgCgBgCgBQgBAAgBgBADWi9QgBAAAAAAIgBABQAAAAgBABADWjAQAAABAAAAIABAAQgBABAAABADYjBQgBABgBAAADWjFQABACABACADLjHIAGAAAAZAjQAABdg7BCQg8BChVAAQhVAAg8hCQg8hCAAhdQAAhcA8hCQA8hCBVAAQBVAAA8BCQA7BCAABcgADTi2QgBAAgBAAQgBAAAAAAQgCAAgBAAQgBAAAAAAQgDAAgCgBQAAAAAAgBQgBAAAAAAQgBgBgBgBQAAAAgBgBQgBAAgBgBADWi2QgCAAgBAAADQi2QgBAAgBAAQgBgBgCgBQgBAAgBAAQgBgCAAgBQgBAAAAgBAGBDIQAAAhgZAYQgZAXgjAAQgjAAgZgXQgZgYAAghQAAghAZgXQAZgYAjAAQAjAAAZAYQAZAXAAAhgAEZCyQgBgCABgBQABAAABAAQACgCADAAIAKgDIADgBQABAAABAAQABgBACgBIADgCIAAADQAAAAAAABQADgCABgCQAAACAAADIACAAQAAABAAACQABAAACAAQAAABAAAAQAAAAABAAQACAAACABIADADQADAJgBAFQgFACgEAAQgDAEgGACQgJADgHAAQgFAAgDgBQgDgCAAgFQgDgHAAgIQABAAAAgCQAAAAAAgBQABgCABAAQAGgBAHAAQAAAAAAAAQACgCADgCQABAAABgBQABAAABABQABAAABABQABABABACQAAAAAAABQABAAAAAAQABAAABAAQABAAACAAQABAAAAAAQAAAAACgBQAAABAAACQAAAAAAAAQAAABAAABQABAAABABIADAAIAAAIQgBABAAAAQgBADgCACQAAAAAAAAQgBABAAAAQgFACgDABIgHACIgBgBQgDABgBAAQgBgBgBgBQgCAAgBgBQgEgDgBgHIAAAAQAAgDABgBQAAgCADgBQABgCACgDQAAAAABgBIAAAAIAAAAAEoCzIAAgEIAAgBQAEAAAEAAQAAgBACgBQAAgBABAAQABgBABAAIAAAAIAEAAQAAABgBACQAAABAAABQgBABAAABQACAAACAAQAAAAAAAAQAAABgBABQACAAACAAQgBAEgCAEQgEAEgGACQgBAAAAABQgBABAAAAQgCACgBAAQgBgCAAAAQgCgDgBgCQgBgDABgCQABgBABgBQAAAAAAgBQgBAAgBgCQAAAAAAgBIAAAAIAAAAQAAgCACAAQAAgBAEgBIABAAQACAAAEAAQABAAAAABQAAABAAAAIAAAAIABAAIAAAAAEkC0IABAAQABAAACgBIAAAAAEoC0QAAAAAAAAIAAgBQACgBACAAQABgBABgCQABAAABgBQACAAADAAQAAgBABABQAAAAAAABIAAAAQAAAAAAABQAAABABAAIAAABQAAAAAAAAQACgBACAAQAAAAABAAQABAAABAAQABAAABABIAAACIAAAHQAAACgBACQAAACAAABQgDABgCAAQgDABgCABQgEACgCAAQAAAAgBAAQgBgBgBgBQgCABgCAAQgCgCgBgEQgBgCgBgBQgCgBgBgBQgCgBgBgBQgDgEgBgDAE1CqQAAgBAAAAQAAAAAAABQABABAAADQABgBABAAQACAAABAAQAAABAAABQAAABAAABQACgBABABQACgBADABIAAADIABAAQACACgBABQAAADgDADQgBABgCABQAAABAAAAQgDABgCACQgFgBgEgBIAAAAQgBAAAAAAQAAAAAAABQgBAAgBAAQAAAAgBABQAAgCAAgBQAAgBAAgBIAAgBQAAAAgBAAQgBAAAAABQgBgBAAgBQgBgBgBgCQAAgCAAgDAExCqIAEAAAErC0QABgBAAgBIAAAAQAEgBAFAAQAAAAABAAQAAABAAAAQAAAAABAAQAAAAAAAAQACAAABABQgBAEgFADQgBABgBABIgBAAQgCABgCAAQAAgDABgDIgBAAQgBAAgBAAQAAAAgBgBQAAgBABgBQACAAADAAQADgCADgBAErC3QAAgCAAgBAEuC3QAAgBABAAQABgCAEAAQABgBABgBQAAADgBACQAAAAAAAAgAEtCqQACAAACAAAE2CxQABAAAAABQAAAAAAAAIAAACQABABgBABQAAABgCAAQAAACgBABQAAABAAABQgBACgDACIAAABQgBAAgBABQAAABgBAAQAAAAAAAAIAAAAQAAgCgBgCQAAgBAAgBQAAgEgBgCAEsC/QgBgBAAAAQgCAAgBAAQgCgBgDAAQAAgDAAgCQABgCAAgCAEtDAQAAgBgBAAAEyC8QgBACgBACAEwDBQgBgBgCAA");
	this.shape.setTransform(328.5,93.05);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AiQCfQg8hCAAhdQAAhcA8hCQA8hCBUAAQBVAAA8BCQA8BCAABcQAABdg8BCQg8BChVAAQhUAAg8hCg");
	this.shape_1.setTransform(310.5,96.55);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgYEBQgZgYAAghQAAghAZgYQAYgXAjAAQAjAAAZAXQAZAYAAAhQAAAhgZAYQgZAXgjAAQgjAAgYgXgAAPC0QAAAIADAHQAAAFADACIAHABQAJAAAIgCQAGgDADgEIAJgCIAAgDIgCgLIgEgDIAAgBIAAAAIgBAAIgBAAIgBAAIgBAAIAAgBIgDAAIAAAAIAAgEIgCAAIAAgBIgBgCIgDADIAAgBIAAgCIgDABIgDACIgCAAIgDAAIgLAEQAAAAgBAAQAAAAgBAAQAAABgBAAQAAAAgBABIgCAAIAAABIAAACIgBACIAAAAgAAVDKQgDgCAAgFQgDgHAAgIIABgCQABADADAEIACACIAAAAQADAHADADIACABIADACIAAAAIAAAAIABAAIAAAAIAAAAIADgBIAAABIAIgCIAIgDIABgBIAAAAQgDAEgGADQgIACgJAAIgHgBgAAhDIIAAAAIAAAAIgDgCIABAAIABAAIADgBIAAAAIACACIgDABIAAAAIAAAAIgBAAgAAlDHIABAAIAGgCIAFgCIABAAIAAAAIACgBIACAAIgBABIgIADIgIACgAAlDHIAAAAgAAjDFIACgCIABgBIACgBQAEABAFABIgFACIgGACIgBAAIgCgCgAAeDGIgCgGIgCgDIAFAAIADAGIABACIAAAAIgDABIgBAAIgBAAgAAcDFQgDgDgDgHIAAAAIAEACIACADIACAGIgCgBgAAjDFIAAAAgAAjDFIAAAAgAAjDFIAAAAgAAiDDIgDgGIADAAIABACIABAEIAAAAIAAAAIABgBIABAAIgBABIgCACIgBgCgAAkDDIAAAAIgBgEIABABIAAADIABgBIgBABIAAAAgAAxDDIAFgDIAAgBIAAADIgCAAIgCABIAAAAIgBAAgAAxDDIAAAAgAAxDDQgFgBgEgBQAGgCAEgEQACgEABgEIgEgBIABgBIABAAIACAAIAAACIACAAIAAAIIgBAEIAAABIgFADIAAAAgAAkDAIADABIgCABIgBABIAAgDgAA2DCIAAgDIADgCIgDAFIAAAAIAAAAgAA2DCIAAAAgAA5C9QADgDAAgDIAAgBIgCgDIgBAAIAAgCIgCAAIgCgBIAAAAIAAgCIABAAIABAAIABAAIABAAIAAAAIAAABIAEADIACALIAAADIgJACIADgFgAA2DCIAAAAgAAnDBIgBABIgBAAIACgBgAAnDBIABAAIAAAAIgCABIABgBgAAoDBIAAAAgAAoDBIgBAAIAAgBIAAABIgDgBIAAgDIAAAAIAEgBIAAAAIgBAEIAEgEIAAgCQAFgDABgFIAEABQgBAEgCAEQgEAEgGACgAAnDBIAAAAgAAoC8IADgCIAAACIgEAEIABgEgAAjC/IAAgCIABAAIAAAAIAAADIgBgBgAA3C7IAAgIIACAAIAAAJIAAABIgDACIABgEgAAjC/IAAAAgAAiC9IABAAIAAACIgBgCgAAfC9IAAgCIAAgCIACADIABABIgCABIgBgBgAAiC9IAAAAgAAhC8IgCgDIACgCIAAgBIgCgCIAAgBIAAAAIAEgBIAAAAIAAACIgCACIACABIAAAGIgBAAIgBgBgAAaC9IAAgCIABgDIAAgEIABAAIADgBIAAABIAAAAIAAAFIAAACIAAACIgFAAgAA5C9IAAAAgAA5C8IAAgJIgCAAIAAgBIgCgBIAAAAIAAgBIAAAAIACABIACAAIAAACIABAAIACADIAAABQAAADgDADIAAgBgAAjC3IABAAIABAAIgBAEIAAACIgBAAIAAgGgAAkC9IAAAAgAAkC9IAAgCIABgEIAHAAIAAAAIgBADIgDACIAAAAIgEABIAAAAgAAaC9IAAAAgAAaC9IAAAAgAAWC7IACgEQAAgBAAAAQAAAAAAgBQABAAAAAAQABAAABgBIAAAEIgBADIAAACIgEgCgAAoC8IAAAAgAAWC7IAAAAgAAUC5QgDgEgBgDIAAgBIACgCIANgBIgBACIgDAEQgBABgBAAQAAAAgBAAQAAABAAAAQAAAAAAABIgCAEIgCgCgAAsC3IACgBIABgBIgBgBIAAgCIAAAAIADAAQgBAFgFADIABgDgAAfC0IACACIAAABIgCACIAAgFgAAsC3IgHAAIAAgBQACgDAEAAIACgBIgBAFIAAAAgAAtCyIABAAIAAACIABABIgBABIgCABIABgFgAAlC3IgBAAIgBAAIgBgBIABgCIADgBIAHgCIAAABIAAAAIgCABQgEAAgCADIAAABgAAjC3IAAAAgAAhC2IACgCIgBACIABABIgCgBgAAjCyIAAAAIAJgBIABAAIgHACIgDABIAAgCgAAfC0IAAAAgAAfC0IAAAAgAAfC0IAAAAIAAgBIAAAAIAAAAIAAABIAAAAgAAbC0IADgEIABgCIAAAAIAAABIAAADIAAABIgDABgAAbC0IAAAAgAA3CzIAAAAgAA3CzgAA3CzIgCAAIAAgCIACABIAAABgAAfCyIACgBQAAAAAAgBQABAAAAAAQABAAAAAAQABAAABAAIgCACIgEABgAAfCzIAAgBIAAABgAAfCzIAAAAgAAxCyIgDAAIAEgBIgBABIAAAAgAAfCvIAAgBIAHAAIgBACQgBAAgBAAQAAAAgBAAQAAAAgBAAQAAABAAAAIgCABgAAtCyIAAgBIAAAAIABAAIAAABIgBAAIAAAAgAAQCyIAAAAgAAQCwIAAgBIACAAIgCACIAAABIAAgCgAAuCyIAAAAgAAuCxIAAABIAAAAIAAgBgAAuCyIAAAAgAAuCxIgBAAIABAAIAEgBIAAABIAAAAIgEABIAAgBgAAlCwIAAAAIAHgBIABABIAAABIgBAAIgJABIACgCgAAtCxIAAAAIABAAIgBAAgAAzCxIgBAAIAAAAIAAgBIADAAIAAABIAAAAIgCAAgAAtCxIAAgBIAAgBIACAAIgBACgAAvCvIADAAIAAABIgEABIABgCgAAtCxgAAtCxIAAAAIAAgBIgBgBIABAAIAAABIAAABgAAtCxIAAAAgAAyCxIAAAAgAAyCxIAAAAgAA1CwIAAAAgAAyCwIAAgBIABAAIACgBIAAACIgDAAgAAlCwIABgCIAGAAIAAABIgHABgAAsCvIAAgBIABAAIAAABIAAAAIgBAAgAAtCvIAAgBIACgBIAAACIgCAAgAAvCvIAAgCIADAAIAAABIAAABIgCAAIgBAAgAAyCvIAAgBIAAgBIADAAIAAABIgCABIgBAAgAASCvIAAAAgAAWCtIALgEIADAAIACAAIgCABIgFAEIAAAAIgNABQABgBAAAAQABAAAAgBQABAAAAAAQABAAAAAAgAA1CuIAAAAgAAfCugAAfCuIAAAAIAAAAgAAfCuIAFgEIAEgBIgEABIACgBIACAAIACACIgCABIgCACIgHAAgAAoCsIACgBIACADIgGAAIACgCgAAtCuIgBgFIAEAAIgBAEIgCABIAAAAgAAsCuIgCgDIACgCIAAAAIABAFIgBAAgAAvCtIABgEIgEAAIAAAAIADgDIABACIAAABIACAAIAAAEIAAAAIgBAAIgCAAgAAqCrIgCgCIAEAAIgCACIAAAAgAAsCpIAAAAIAAAAgAAoCpIgCAAIADgCIADgBIAAACIAAABIAAAAgAAsCpIAAAAgAAmCpIAAAAgAhkiGQgTgZAAgjQAAgjATgZQATgZAbAAQAbAAATAZQASAZAAAjQAAAjgSAZQgTAZgbAAQgbAAgTgZgAhAi0IAAAAQAAABAAABQAAAAAAAAQAAABABAAQAAAAAAAAIAEAAIACgDIgHAAIgBgDIAEABIgBgBIAEAAIACABIgDAAIgCAAIAEACIAAAAIAAAAIAAAAIABgBIAAgBIABAAIgBAAIAAAAIABAAIAAgCIACABIACgBIAAACIgCAAIgCAAIAAAAIAAACIgCAAIACABIAAgBIABAAQABAAAAAAQABAAAAAAQABAAAAAAQAAgBAAAAIAAgBIAAAAIAKgBQAGgBAAgDIAAgBQAAgBAAAAQAAgBgBAAQAAgBgBAAQAAgBgBAAIgEgBQAAgBAAAAQAAgBAAgBQAAAAAAAAQgBgBAAAAIgCgBIAAgCIAAgCIAAgBIgGAAIAAgGIgFADIAAgBQgDgCgDAAIgCAAIgDACIAAAAIAAAAIADAAIABAFIgCABIgEABIgBABIgDAAIgCAFIgBAAIgBgBIgBgBQgBgBAAgFQAAgBAAAAQAAgBAAAAQAAAAABAAQAAAAAAAAQAAgBAAAAQABAAAAgBQABAAAAAAQABAAABAAIAHgBIgHAIIgDAEIADgEIAHgIIgHABQgBAAgBAAQAAAAgBAAQAAABgBAAQAAAAAAABQAAAAAAAAQgBAAAAAAQAAAAAAABQAAAAAAABQAAAFABABIABABIgBACIABgCIABABIgCABIgBABQgBAAAAABQAAAAgBAAQAAABAAAAQAAABAAAAIABACQAAABAAAAQAAABAAAAQAAAAABAAQAAABABAAQAGACAHACIABAAIAAAAgAg/ixQAAAAAAAAQgBAAAAgBQAAAAAAAAQAAgBAAgBIAAAAIAHAAIgCADgAg5i0IACAAIAAABIgCgBgAg3i0IAAgCIAAAAIACAAIgCAAIACAAIACAAIAAABQAAAAAAABQAAAAgBAAQAAAAgBAAQAAAAgBAAIgBAAgAg3i0IAAAAgAg5i0gAg5i0IAAAAIAAAAgAg9i2IACAAIADAAIAAABIgBABIgEgCgAhBi0QgHgCgGgCQgBAAAAgBQgBAAAAAAQAAAAAAgBQAAAAAAgBIgBgCQAAAAAAgBQAAAAAAgBQABAAAAAAQAAgBABAAIABgBIACgBIABABIAAgBIABAAIAAAAIAFgCIAAABIgBABIAAABIAAAAIgBACIgCgBIgBgBIAAgBIAAAAIAAAAIAAABIgBAAIgBAFIgCgDIAAgCIAAACIACADIAAABIADAAIACABIACABIACACIACAAIABADIgBAAgAgzi2IAAAAgAg1i2IACAAIAAAAIgCAAgAg4i2IAAAAgAg4i2IgCgBIACABIAAAAIAAAAgAhBi3IAAgBIABAAIACABIABABIgEgBgAg4i2IABgCIAAACIgBAAIAAAAgAgzi2gAgzi2gAgzi2IAGgBQAAAAAAgBQABAAAAAAQABAAAAgBQABAAAAgBQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBAAAAgBIgBgBIACgBIAAgBIAEABQABAAAAABQABAAAAABQABAAAAABQAAAAAAABIAAABQAAADgGABIgKABgAgzi2gAgzi4IACAAQAAAAAAgBQABAAAAAAQAAAAAAgBQAAAAAAgBIADAAIgDgCIAAACIgDAAIAAgCIAAACIADAAQAAABAAAAQAAABAAAAQAAAAgBAAQAAABAAAAIgCAAIAAgDIgCAAIgBABIABgBIgBABIgBAAIAAgCIgFABIAAgBIAAABIgDAAIAEACIAAAAIgDABIgBAAIgBgDIABADIAAAAIAAAAIgBAAIgBAAIAAAAIABAAIgBAAIgBgBIADABIgCgCIABgBIAAAAIABAAIgCgBIAAgDIgCgEIAAACIABAEIgCgBIACADIAAABIgCAAIgBgEIABAAIgBAAIABAEIgBAAIgCgCIAAgCIAAgBIACABIAAAAIABAAIgBAAIAAAAIgCgBIABgCIABADIgBgDIAAAAIAAgBIAAABIAAgBIABAAIAAABIAAADIAAgDIAAgBIACgBIAEgCIABAAIgBAAIgEACIABgCIAEgBIAAABIAAAAIAAAEIAAAEIACABIgCgFIAAgEIAAAEIAAgEIAAAAIAHAAIAAAJIgFAAIAFAAIACABIAAAAIABgBIABgBIADAAIADgBIABgBIABABQAAABABAAQAAAAAAABQAAAAAAABQAAAAAAABQAAABgBAAQAAABgBAAQAAAAgBAAQAAABAAAAIgGABgAg4i2IgCgBIgEgBIADAAIAAgBIABAAIADABIgBACIAAAAgAg+i3IgBgBIABAAIAEABIgEAAgAhDi3IgCgCIACABIACAAIgCAAIACAAIAAAAIAAABIgCAAgAhAi4IABAAIABABIgCgBgAg3i4IABgCIABgBIACAAIAAADIgCABIgCgBgAg/i4IAAAAIAAAAIABAAIgBAAIAAAAgAg3i4IAAgCIABAAIgBACIAAAAgAg3i4IAAAAgAg6i5IADgBIAAACIgDgBgAg3i4gAg+i4IgBAAIABAAIADgBIAAABIgDAAIAAAAgAhDi4IgBgCIACABIABABIgCAAgAhCi5IAAgBIABAAIACACIgDgBgAhFi5IgBgBIABAAIABAAIAAAAIAAAAIACAAIAAABIgCgBIgBAAIABAAIABACIgCgBgAg6i5IgBAAIgBgCIAFgBIAAACIgDABIAAAAgAg7i5IAAAAIABAAIgBAAgAg7i5IAAAAgAg/i7IADAAIABACIgEgCgAhFi5IAAAAgAhHi6IgCgBIADABIABABIgCgBgAhBi6IgBgBIAAgCIABABIACABIgBAAIAAAAIgBgBIABABIgBABIAAAAgAhCi6IAAgBIABABgAhCi6gAhCi6gAhCi6IAAAAgAhGi6IgBgCIACACIgBAAgAhJi7IgDgBIABgFIABAAIAAABQAAABAAAAQABABAAAAQAAABABAAQAAAAABAAIAAABIABACIgDgBgAhAi7IAAAAgAhJi7IAAAAgAhMi7IAAgBIADABIgDAAgAgwi7IAAgCIADACIgDAAgAgwi7IAAAAgAgzi7gAgzi7IAAAAgAhEi+IACABIAAACIgCgDgAg1i7IAAgBIABAAIgBABgAg3i8IACAAIAAABIgCgBgAg3i8IAAgJIABABIADAEIAAABIgCADIgCAAIAAAAgAhCi9IgBgEIAAgCIACAEIAAADIgBgBgAhHi8IAAAAgAhHi8IAAAAgAhHi9QgBAAAAAAQgBAAAAgBQAAAAgBgBQAAAAAAgBIAAgBIABABIACABIAAABIAAACIAAgBgAg1i8IAAAAgAgzi/IAAABIgBABIAAABIgBAAIACgDgAg0i9IABgBIAAABIgBABgAg+i9IAAgEIACAFIgCgBgAgzi+IACgBIABACIgDAAgAgwi9IAAAAgAgxjAIABgBIACgDIgCgDIgDAAIAAgBIAFABIAAACIAAABIABAAIABAFIgBABIgDABIgBgDgAgxi/IAAgBIABADIgBgCgAgzi+gAgzi/IABgBIgBAAIAAgFIACAEIgBABIAAABIABAAIgCABgAgsi/IgBgFIACADIABAAIAAABIgCABgAgxi/IgBAAIAAgBIABgBIAAABIAAABIAAAAgAgxi/gAgzi/gAgzjAIABAAIgBABgAgxjAIAAgBIABAAIgBABIAAAAgAgzjAIgDgEIgBgBIAAgCIAEAAIAAACIAAAFIAAAAgAgxjBIAAAAgAgzjFIAAgCIACAAIABAEIAAACIAAAAIgBAAIgCgEgAgrjBIgCgDIgBgBIABAAIAAgBIACABQAAAAABABQAAAAAAAAQAAABAAABQAAAAAAABgAg+jBIAAAAgAhKjBIAAAAgAhKjBIAAAAgAgwjBgAgwjBIAAgCIgBgEIgCAAIAAAAIADAAIACADIgCADgAhMjCIABAAIAAABIgBgBgAhFjDIAAABIgBAAIABgBgAhJjHIADAAIACAAIgBADIgFACIAAAAIABgDIADgCIgDACIgBADIgBAAIACgFgAhFjDIABgCIABAAIABABIgBABIgCABgAhFjEIABgBIgBACIAAgBgAhCjEIgBgBIABAAIgBACIABgBgAgujEIAAgBIABABgAhFjEIAAAAgAhEjHIABAAIABAAIAAABIgBAAIAAgBIAAABIgBABIgBABIABgDgAgujFgAgujHIgFgBIAAgCIADACIADACIAAABIgBAAgAgujFgAg3jFIAAAAgAg+jFIAAgBIAHgBIAAACIgHAAgAhEjFIAAAAgAhDjGIAAABIgBAAIABgBgAhDjFIAAgBIABAAIAAABgAhCjGIABgBIACAAIABABIgEABIAAgBgAgzjFIAAAAgAg+jFIAAAAIAAAAIAAgBIAAAAIAAABIAAAAgAg+jGIgBgBIABAAIAAABIAAAAIAAAAgAg+jHIAHAAIgHABgAgtjGIgDgCIgDgCIAAgBIgBAAQAAgBAAAAQAAgBgBAAQAAgBgBAAQgBAAAAAAIgBAAIAFgDIAAAGIAGAAIAAABIAAACIAAACIAAAAgAhCjHIgBAAIgBAAIgCAAIABgBIAEgBIgCACIACgCIABAAIgCACIABAAIgBABgAgzjHIAAAAgAgzjHIAAAAgAgzjHgAgzjHgAgzjHIAAAAIAAAAIAAAAgAg3jHIAAgBIgBAAIgGABIAAgCIAAACIgBAAIAAgBIgCABIgBAAIACgCIABAAIAAABIABgBIgBABIAAgBIgBAAIABAAIAAAAIABgBIAAABIAEgBIADABIAAABIAEABIAAgBIAAAAIAAABIAAAAIAAAAgAg+jHIAGgBIABAAIAAABgAg+jHgAhBjHIACgBIAAABIgCAAgAhBjHIAAAAgAg3jIIAAgBIgDgBIADAAIAAABIABAAIADABIAAABIgEgBgAg3jIIAAAAgAgzjIIgCgCIgCAAIgBAAIABgBIgBgDIADACIABABIgDAAIADAAIABABIAAACIAAAAgAgzjIIAAAAgAg2jJIgBAAIAAgBIACAAIACACIgDgBgAg/jKIAAAAIAAABIABgBIAAAAIgBABIAAAAIgBAAIgBAAIACgBgAg+jKIADAAIABAAIgEABgAg/jKIAAAAIgBgFIAIABIgGAAIAAADIgBABIABgBIAAABIgBABIAAgBgAg6jKIgBAAIAEgBIgBABIABAAIgDAAIAAAAgAg+jKIACAAIABAAIgDAAgAg3jKIAAAAgAg0jLIABAAIAAABIgBgBgAg8jKIgCAAIAAgBIADgBIADgCIABADIgEABgAg+jKgAg0jLIAAAAgAg1jMIgDgCIABAAQAAAAABAAQABAAAAABQABAAAAABQAAAAAAABIgBgBgAg+jOIAGAAIgDACIgDABgAg4jOIAAAAgAg4jOgAg4jOgAg4jOIAAAAgAhAjPIAAgCIACAAQADAAADACIAAABIgIgBgAhDjPIADgCIAAACIgBAAIgCAAg");
	this.shape_2.setTransform(355,93.05);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_10
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(1,1,1).p("Ap6iSINhpqIABAKIEiFUIAyABIFAg/ICIK4ImJBNAHzmPIBJgOAJEAQIgjiyIgDAcIlvkgIgDgCIABgDIACAFIAHASIqrG0IAEADIgJAAIgBAAIgBgBQkIDGj/lGQA+ilCUhLICqCqIANASAhWheIATgKIACAOIKFBqIAMgBIgDAsAKgEdIgnAEQk/AJjfjJIjHKBIgDAJIgJACIn7kIIDIj6IDpBwIgDgMIBvmsIgBgBIABAAIAVAEAhWhdIAAgBAp9iQIADgCAn8AiIgWgfIAdAdIgFADgAjCFbIAHgCIgHAIgAhsLiIABAHIgEACIgGASIgDgQ");
	this.shape_3.setTransform(6.775,153.475);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFF99").s().p("ApzHtIDIj6IDpBwIABAGIAGgIIgHACIgDgMIBvmsIAAgBIAVAEIKFBqIAMgBIgDAsIADgsIgMABIgjiyIgDAcIlvkgIAHASIqrG0IgdgdIAXAfQkJDGj/lGQA+ilCUhLICqCqIANATIgDABIADgBINhprIABAKIEiFUIAyABIhJAOIBJgOIFAg/ICIK4ImJBNIgCgIIAngEIgnAEQk/AJjfjJIjHKBIgDAJIgJACgACsmgIADACIgCgFg");
	this.shape_4.setTransform(6.775,152.6625);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AA5JEIAJgCIADgIIABAHIgEABIgGATgAgRCzIAIgCIgHAHgAlIiFIAFgDIAEAEgAlKiFIACAAIgBABgAlIiFgAlIiFIgCAAIgWgfIAdAcIgFADgAlDiIgABbkGIATgKIACANgABakGIABAAIgBAAgABbkGgAFepQIABgEIACAGg");
	this.shape_5.setTransform(-11.0125,170.3375);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]}).wait(1));

	// Layer_8
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(1,1,1).p("ArEyIIF0kxQI6EsBLO1IjzA2QjlyCohCcgAFdIEIDcgoICMMgIgyAKIgKAAIwFC0IhQksINShag");
	this.shape_6.setTransform(210.075,98.425);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#CCFF99").s().p("AnMSOINShaIgpowIDcgoICMMgIgyAKIgKAAIwFC0gArEyIIF0kxQI6EsBLO1IjzA2QjlyCohCcg");
	this.shape_7.setTransform(210.075,98.425);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6}]}).wait(1));

	// Layer_9
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(1,1,1).p("AFe2fQAABNhoA2QhoA2iSAAQiTAAhog2Qhog2AAhNQAAhMBog3QBog2CTAAQCSAABoA2QBoA3AABMgAFoVpQAABjhbBGQhbBHiAAAQh/AAhchHQhahGAAhjQAAhkBahGQBchGB/AAQCAAABbBGQBbBGAABkg");
	this.shape_8.setTransform(135,97.525);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFCCCC").s().p("AioYSQhbhGAAhjQAAhkBbhGQBahGCAAAQCAAABbBGQBbBGAABkQAABjhbBGQhbBHiAAAQiAAAhahHgAj/0cQhog2AAhNQAAhMBog3QBog2CTAAQCSAABoA2QBoA3AABMQAABNhoA2QhoA2iSAAQiTAAhog2g");
	this.shape_9.setTransform(135,97.525);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8}]}).wait(1));

	// Layer_5
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(1,1,1).p("AtOi5IYQk8ICMKvI4PE8g");
	this.shape_10.setTransform(182.75,126.45);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#CCFFCC").s().p("AtOi5IYQk8ICMKvI4PE8g");
	this.shape_11.setTransform(182.75,126.45);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10}]}).wait(1));

	// Layer_3
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(1,1,1).p("AJJAAQAAClirB2QirB1jzAAQjyAAirh1Qirh2AAilQAAikCrh2QCrh1DyAAQDzAACrB1QCrB2AACkg");
	this.shape_12.setTransform(335.5,94);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFCCCC").s().p("AmdEaQirh0AAimQAAikCrh2QCrh1DyAAQDzAACqB1QCsB2AACkQAACmisB0QiqB2jzAAQjyAAirh2g");
	this.shape_13.setTransform(335.5,94);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12}]}).wait(1));

	// Layer_4
	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(1,1,1).p("Aj2hPIGqhqIBDEJImqBqg");
	this.shape_14.setTransform(272.025,103.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFCCCC").s().p("Aj2hOIGqhrIBDEIImqBrg");
	this.shape_15.setTransform(272.025,103.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14}]}).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Human, new cjs.Rectangle(-97,-65.9,492,326.9), null);


(lib.House = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(5,1,1).p("AqskcIFyAAIAAIvIlyAAgAEnkIIGGAAIAAIlImGAAg");
	this.shape.setTransform(2.5,-65.45);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(1,1,1).p("ADrzDQAABihFBEQhEBFhiAAQhhAAhFhFQhEhEAAhiQAAhhBEhFQBFhFBhAAQBiAABEBFQBFBFAABhgAjWLzIFnAAIAAK8IlnAAg");
	this.shape_1.setTransform(3.5,-41.475);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFF00").s().p("Ai4EYIAAovIFxAAIAAIvg");
	this.shape_2.setTransform(-47.5,-65.95);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("ABBLuIAAomIGGAAIAAImgAmBlcQhFhFAAhhQAAhhBFhFQBFhFBhAAQBhAABFBFQBEBFAABhQAABhhEBFQhFBFhhAAQhhAAhFhFg");
	this.shape_3.setTransform(25.5,-111.95);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#996600").s().p("AizFeIAAq7IFnAAIAAK7g");
	this.shape_4.setTransform(0,69);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_1
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(1,1,1).p("ASjL3Ik2ACI7VAIIk6ABIS94Eg");
	this.shape_5.setTransform(0.325,-194.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#990000").s().p("AtqRaMAAAgirIbVgIMAAAAizg");
	this.shape_6.setTransform(0.5,-7.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#996600").s().p("AAbsCISIX5Ik2ACI7VAIIk6ACg");
	this.shape_7.setTransform(0.325,-194.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-119.3,-272.9,239.3,377.9);


(lib.Cow = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// timeline functions:
	this.frame_122 = function() {
		this.stop()
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(122).call(this.frame_122).wait(375));

	// Layer_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("AiuGBQAAAqgcAdQgcAdgnAAQgnAAgdgdQgbgdAAgqQAAgqAbgdQAdgdAnAAQAnAAAcAdQAcAdAAAqgAj0AjIqyoHIFoM9AGVGBQAAAygcAjQgcAjgnAAQgnAAgcgjQgcgjAAgyQAAgxAcgkQAcgjAnAAQAnAAAcAjQAcAkAAAxgAD1APIKyoHIloM9");
	this.shape.setTransform(350.5,-122);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AummUIKyIHIlKE2gAD1BfIKyoHIloM9g");
	this.shape_1.setTransform(350.5,-130);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("ADfBUQgcgiAAgyQAAgwAcgkQAcgjAnAAQAnAAAcAjQAcAkAAAwQAAAygcAiQgcAkgnAAQgnAAgcgkgAllBHQgbgeAAgpQAAgoAbgeQAdgdAnAAQAnAAAcAdQAcAeAAAoQAAApgcAeQgcAdgnAAQgnAAgdgdg");
	this.shape_2.setTransform(352.5,-83.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[]},123).wait(374));

	// Layer_4
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(1,1,1).p("Aw6vYIAAexIigAAIAA+xApnr6IAOahIigAAIgZ6/AO5vYIAAexIigAAIAA+xATWubIAFcfIiMAAIAP7t");
	this.shape_3.setTransform(126.75,158.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#615436").s().p("AMZPZIAA+xICgAAIAAexgAzZPZIAA+xICgAAIAAexgAr5OnIgZ6/ICrAeIAOahgAROOEIAQ7sIB4gyIAFceg");
	this.shape_4.setTransform(126.75,158.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3}]}).to({state:[]},123).wait(374));

	// Layer_3
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(1,1,1).p("AFyAAQAABbhtBBQhrBAiaAAQiZAAhshAQhshBAAhbQAAhaBshBQBshACZAAQCaAABrBAQBtBBAABag");
	this.shape_5.setTransform(376,-10);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF638D").s().p("AkFCcQhshBAAhbQAAhaBshBQBshACZAAQCZAABsBAQBtBBAABaQAABbhtBBQhsBAiZAAQiZAAhshAg");
	this.shape_6.setTransform(376,-10);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5}]}).to({state:[]},123).wait(374));

	// Layer_2
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(1,1,1).p("AKFAAQAAExi9DXQi9DXkLAAQkKAAi9jXQi9jXAAkxQAAkwC9jXQC9jXEKAAQELAAC9DXQC9DXAAEwg");
	this.shape_7.setTransform(350.5,-52.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#725436").s().p("AnHIIQi9jYAAkwQAAkwC9jXQC9jXEKAAQELAAC9DXQC9DXAAEwQAAEwi9DYQi9DXkLAAQkKAAi9jXg");
	this.shape_8.setTransform(350.5,-52.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7}]}).to({state:[]},123).wait(374));

	// Layer_1
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(1,1,1).p("EAj3AAAQAAGIqgEUQqgEVu3AAQu2AAqgkVQqgkUAAmIQAAmHKgkVQKgkUO2AAQO3AAKgEUQKgEVAAGHg");
	this.shape_9.setTransform(117.5,-9.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#615436").s().p("A5WKcQqgkVAAmHQAAmHKgkUQKgkVO2AAQO3AAKfEVQKhEUAAGHQAAGHqhEVQqfEVu3AAQu2AAqgkVg");
	this.shape_10.setTransform(117.5,-9.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9}]}).to({state:[]},123).wait(374));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-113,-173.5,558,431.5);


(lib.Beam = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("AtCytIZSAAMAOXAlbMg1NgANg");
	this.shape.setTransform(0.025,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF00").s().p("A6mShMANkglOIZSAAMAOXAlbg");
	this.shape_1.setTransform(0.025,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Beam, new cjs.Rectangle(-171.3,-120.8,342.70000000000005,241.6), null);


(lib.Bag = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("ATxDxQjdDKhsCLQgqA2hJBEQhZBRgwAsQh2BskCgRQg1gEiogVQiJgRhMAAQhAAAi8ANQjDAMhsACQl2AEh4huQiBh1hElKQg1j9AAj4QAAifDuh0QBSgoCFguQCSgvA9gWQBGgaBUg9QAxgkBhhOQBXhDBEggQBbgqBdAAQBcAAA4A5QApAnAoBdQA0B4ASAeQAxBQBIAhQBcAqHTAhQHTAiBTDIQBUDHjeDKg");
	this.shape.setTransform(36.6324,14.0911);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#0033FF").s().p("AE0OYQg1gEiogVQiJgRhMAAQhAAAi8ANQjDAMhsACQl2AEh4huQiBh1hElKQg1j9AAj4QAAifDuh0QBSgoCFguQCSgvA9gWQBGgaBUg9QAxgkBhhOQBXhDBEggQBbgqBdAAQBcAAA4A5QApAnAoBdQA0B4ASAeQAxBQBIAhQBcAqHTAhQHTAiBTDIQBUDHjeDKQjdDKhsCLQgqA2hJBEIiJB9QhmBdjOAAQghAAgjgCg");
	this.shape_1.setTransform(36.6324,14.0911);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Bag, new cjs.Rectangle(-107,-79.1,287.3,186.39999999999998), null);


(lib.Alien = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Face
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("AD1lTQAACwhIB8QhHB8hmAAQhkAAhJh8QhHh8AAiwQAAiwBHh9QBJh8BkAAQBmAABHB8QBIB9AACwgABBlYQAABbgTBBQgTBAgbAAQgaAAgThAQgThBAAhbQAAhbAThBQAThAAaAAQAbAAATBAQATBBAABbgABVJsQAAA8gZArQgZAqgjAAQgiAAgZgqQgZgrAAg8QAAg8AZgqQAZgrAiAAQAjAAAZArQAZAqAAA8g");
	this.shape.setTransform(-256.5,-240.45);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("Ag7JuQgZgqAAg8QAAg8AZgrQAZgqAiAAQAjAAAZAqQAZArAAA8QAAA8gZAqQgZArgjAAQgiAAgZgrgAgtkgQgThBAAhbQAAhbAThBQAThAAaAAQAbAAATBAQATBBAABbQAABbgTBBQgTBAgbAAQgaAAgThAg");
	this.shape_1.setTransform(-256.5,-230.45);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AitEsQhHh8AAiwQAAivBHh8QBJh9BkAAQBmAABHB9QBIB8AACvQAACwhIB8QhHB9hmAAQhkAAhJh9gAgtigQgTBBAABbQAABaATBBQATBAAaAAQAbAAAThAQAThBAAhaQAAhbgThBQgThAgbAAQgaAAgTBAg");
	this.shape_2.setTransform(-256.5,-274.45);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Arms
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#00FF00").s().p("At2ovYASAfARAfATAeYATAfASAeAUAeYATAeAUAdAVAeYAKAPAKAOALAPYAKAOALAPALAOIAhArIAjApYALAOAMANAMAOYAMAOANANANANIASAUIAUASYANAMAOANAOAMYAOAMAOAMAPALIAWARYAHAGAIAFAIAFIAXAQIAZAOYAQALASAIARAIYAJAFAJADAJAEYAJADAJAEAJADIAdAIYAFACAEABAFABIAPADYAoAJApABApgEIAegFYAKgCAKgCAKgDYAFgBAFgCAFgBIAPgFYAKgEAJgDAKgEYASgKATgIARgLYAIgFAJgFAIgGIAYgSIAMgJIALgKIAXgTYAOgNANgOAOgOYAMgPANgOAMgOYAMgPAMgOALgQYALgPALgPALgQIAfgvIgVAYYArgcAsgaAsgUYAWgKAVgJAVgGYAUgGATgDAOAAYAHAAAGABAEABYADABADABADACYACACADACAEAFYADAEAEAHAEAHYAHAPAFAVAFAVYAEAWADAYABAZYAEAxgBAygCA0YgBAagDAbgBAaYgCAagCAbgCAbYgDAagCAbgDAbIgIBQYgCAbgDAbgDAbYgCAbgCAcgDAbIAAAAYAAAFAEAEAEAAYAEAAADgCABgDIASglYAGgMAFgNAGgNYALgZAKgZAKgaYAKgaAJgZAJgbYAJgaAJgaAHgaYAIgbAHgbAHgaYAHgbAGgcAGgbYALg3AIg4ACg7YABgeAAgegEgfYgDgggGghgOgjYgGgRgJgSgMgSYgNgRgQgRgTgOYgTgOgXgLgWgGYgWgGgWgCgUAAYgUABgSACgSAEYgRAEgQAFgQAGYgfALgbAOgbAOYg0AeguAigtAjIgEADYgHAGgGAHgEAHIgYAtYgIAPgIAOgJAOYgJAOgJANgKAOYgJAOgKAMgKANYgLAMgKANgLALIgRAQIgJAIIgIAIIgSAPYgGAFgHAEgGAEYgMAKgOAGgNAIYgHAEgGACgHADIgKAFIgLADYgHACgHADgGACIgWAEYgdAFgeACgfgFIgLgBYgEgBgEgBgEAAIgXgFYgHgCgIgDgHgCYgIgCgIgDgHgDYgPgGgPgGgPgHIgWgLIgWgNYgHgEgIgEgHgFIgVgNYgOgIgOgLgOgKYgOgKgNgLgNgLIgUgQIgUgRYgNgMgNgLgMgMYgNgMgNgMgMgNIgkglIgkgnYgMgNgLgNgMgNYgLgOgMgNgLgNYgXgcgWgbgWgcYgWgbgVgdgWgcYgVgcgVgdgVgdIAAAAYgCgEgGAAgDACYgEADgBAFADAD");
	this.shape_3.setTransform(-151.9413,-6.5252);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#00FF00").s().p("AKrtCYgZgNgZgOgbgMYgNgGgNgGgOgFYgNgGgOgGgNgFYgcgKgbgJgdgIYgcgIgdgGgegEYgOgCgPgDgPAAIgXgCIgXAAYgeAAgeAEgfAGYgeAHgeAJgcAOYgPAGgNAIgOAIYgHADgGAFgHAEYgGAFgHAEgGAFIgRAPIgJAHIgJAJIgQAQYgGAFgFAGgFAGYgFAGgGAGgEAGIgOASYgKAMgIANgJANYgQAagPAbgNAbYgNAcgMAbgLAdYgFAOgFAOgEAOIgIAVIgGAWYgJAdgGAcgHAdYgHAdgGAdgFAeYgFAdgEAdgFAdIAMgdYgWAcgVAcgVAdYgWAdgUAdgVAdYgpA7goA6gmA+YgmA+gkBAgeBFYgPAigNAkgLAkYgLAlgJAmgFAoYgDAUgCAVAAAUYAAAVABAVACAWYADAVAEAWAGAVYAGAWAJAVAKAUYAWApAeAjAjAbYARAOATALASAKYATAKATAIATAGYAmANAmAIAmAFYAlAEAmABAkAAYAlgBAkgEAkgFYASgCASgDARgDYASgEARgEASgEYAEgBADgEgBgFYgBgDgCgDgEAAIAAAAYgSgDgRgCgSgDYgRgCgRgCgSgDYgigFgigHgigHYghgHgggJgfgKYgfgLgegMgagPYgbgPgXgRgSgTYgTgTgNgVgJgXYgIgXgFgagBgbYAAgbACgdAFgeYAGgdAHgfAKgeYAJgfAKgeAMgfYAMgfAMgfAOgeYANgfAOgfAQgeYAPgfAQgeAQgeYARgfARgdASgdYAjg7Amg6Ang5IACgDYAFgIAEgJABgKYACgcACgcAEgbYADgcAEgbAEgcYAFgbAFgbAGgbIAFgUIAFgUYADgOADgNAFgNYAHgaAJgaAKgZYAKgZALgZANgXYAHgMAGgMAHgKIALgRYAEgGAEgFADgFYAQgVASgTATgSYAEgFAGgDAFgFYAFgDAFgFAFgDYALgIAKgHAMgGYAXgOAYgJAZgIYAagHAagGAbgCIAVgCIAUAAYAOgBAOABANABYAcABAcAEAbAFYAcAFAbAGAbAIYAOADAOAEANAFYAOAEANAEANAFYAbAJAbALAbALYAEACAFgCABgFYACgEgCgEgEgC");
	this.shape_4.setTransform(-350.9977,18.7953);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3}]}).wait(1));

	// Body
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(1,1,1).p("AFyAAQAAKLhsHLQhtHMiZAAQiZAAhsnMQhsnLAAqLQAAqKBsnMQBsnLCZAAQCZAABtHLQBsHMAAKKg");
	this.shape_5.setTransform(-261,29);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#00FF00").s().p("AkFRWQhsnMAAqKQAAqKBsnMQBtnLCYAAQCZAABtHLQBsHMAAKKQAAKKhsHMQhtHMiZAAQiYAAhtnMg");
	this.shape_6.setTransform(-261,29);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5}]}).wait(1));

	// Legs
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(1,1,1).p("Ah3tRIBaAAIAAaFIhaAAgAAotHIBQAAIAAaZIhQAAg");
	this.shape_7.setTransform(-261,255);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#00FF00").s().p("AAoNSIAA6ZIBQAAIAAaZgAh3M0IAA6FIBaAAIAAaFg");
	this.shape_8.setTransform(-261,255);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7}]}).wait(1));

	// Head
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(1,1,1).p("AH5AAQAAGeiUElQiUEljRAAQjQAAiUklQiUklAAmeQAAmdCUklQCUklDQAAQDRAACUElQCUElAAGdg");
	this.shape_9.setTransform(-262.45,-241.95);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#00FF00").s().p("AlkLDQiUklAAmeQAAmdCUklQCUklDQAAQDRAACUElQCUElAAGdQAAGeiUElQiUEljRAAQjQAAiUklg");
	this.shape_10.setTransform(-262.45,-241.95);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9}]}).wait(1));

	// Neck
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(1,1,1).p("AhUl2ICpAAIAALtIipAAg");
	this.shape_11.setTransform(-262.5,-149.45);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#00FF00").s().p("AhUF3IAArtICpAAIAALtg");
	this.shape_12.setTransform(-262.5,-149.45);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11}]}).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Alien, new cjs.Rectangle(-419.8,-342.9,356.8,683.9), null);


// stage content:
(lib.TheAbduction_Final = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	this.actionFrames = [0,10,119,125,214,239,245,301,359,365,492,524,571];
	// timeline functions:
	this.frame_0 = function() {
		var music = this;
		//Starts playing music when animation starts
		music.play();
		playSound("bensounddeepblue",-1);
		this.stop();
		//Adds Click event to main menu play button
		this.play_btn.addEventListener("click", startSceneOne.bind(this));
		//Starts First Scene
		function startSceneOne()
		{
			this.gotoAndPlay(10);
		}
		//Adds Click event to mute button
		this.Mute_Btn.addEventListener("click", fl_MouseClickHandler_6.bind(this));
		//Reduces volume to 0, hides mute button, and unhides unmute button
		function fl_MouseClickHandler_6()
		{
			createjs.Sound.volume = 0;
			this.Mute_Btn.visible = false;
			this.Unmute_BTN.visible= true;
		}
		//Adds Click event to unmute button
		this.Unmute_BTN.addEventListener("click", fl_MouseClickHandler_7.bind(this));
		//Increases volume to 1(max), hides unmute button, and unhides mute button
		function fl_MouseClickHandler_7()
		{
			createjs.Sound.volume = 1;
			this.Mute_Btn.visible = true;
			this.Unmute_BTN.visible= false;
		}
	}
	this.frame_10 = function() {
		playSound("_431860__cheetahzbrain__deadcowwav");
		//Adds Click Event to Rock Button
		this.Rock_btn.addEventListener("click", startSceneFive.bind(this));
		//Starts Scene five at indicated frame
		function startSceneFive() {
			this.gotoAndPlay(496);
		}
		//Adds click event to Road Button
		this.Road_btn1.addEventListener("click", startSceneTwo.bind(this));
		//Starts scene two at indicated frame
		function startSceneTwo() {
			this.gotoAndPlay(125);
		}
	}
	this.frame_119 = function() {
		this.stop();
	}
	this.frame_125 = function() {
		playSound("_175356__maxheadroom__horsegallopingwav");
	}
	this.frame_214 = function() {
		this.stop();
		//Adds click event to button instance 'Next_4'
		this.Next_4.addEventListener("click", startSceneSix.bind(this));
		//Begins scene located at indicated frame
		function startSceneSix()
		{
			this.gotoAndPlay(530);
		}
		//Adds click event to button instance 'Next_2'
		this.Next_2.addEventListener("click", startSceneThree.bind(this));
		//Begins scene located at indicated frame
		function startSceneThree()
		{
			this.gotoAndPlay(245);
		}
	}
	this.frame_239 = function() {
		this.stop();
		this.stop();
	}
	this.frame_245 = function() {
		//Adds click event to button instance 'Next_3'
		this.Next_3.addEventListener("click", startSceneFour.bind(this));
		//Begins scene located at indicated frame
		function startSceneFour()
		{
			this.gotoAndPlay(365);
		}
		//Adds Click Event to Button Instance 'Prev_2'
		this.Prev_2.addEventListener("click", returnToSceneTwo.bind(this));
		//Begins scene located at indicated frame
		function returnToSceneTwo() {
			this.gotoAndPlay(126);
		}
	}
	this.frame_301 = function() {
		playSound("_51751__erkanozan__ufosweep01wav");
	}
	this.frame_359 = function() {
		this.stop();
	}
	this.frame_365 = function() {
		playSound("_235__erratic__ufoatmospherewav");
		//Adds Click Event to Button Instance 'Prev_3'
		this.Prev_3.addEventListener("click", returnToSceneThree.bind(this));
		//Begins scene located at indicated frame
		function returnToSceneThree()
		{
			this.gotoAndPlay(245);
		}
	}
	this.frame_492 = function() {
		this.stop();
		//Adds click event to button instance 'title_btn1'
		this.title_btn1.addEventListener("click", toTitle1.bind(this));
		//Begins scene located at indicated frame
		function toTitle1()
		{
			this.gotoAndPlay(0);
		}
	}
	this.frame_524 = function() {
		this.stop();
		//Adds click event to button instance 'title_btn2'
		this.title_btn2.addEventListener("click", toTitle2.bind(this));
		//Begins scene located at indicated frame
		function toTitle2()
		{
			this.gotoAndPlay(0);
		}
	}
	this.frame_571 = function() {
		this.stop();
		//Adds click event to button instance 'title_btn3'
		this.title_btn3.addEventListener("click", toTitle3.bind(this));
		//Begins scene located at indicated frame
		function toTitle3()
		{
			this.gotoAndPlay(0);
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(10).call(this.frame_10).wait(109).call(this.frame_119).wait(6).call(this.frame_125).wait(89).call(this.frame_214).wait(25).call(this.frame_239).wait(6).call(this.frame_245).wait(56).call(this.frame_301).wait(58).call(this.frame_359).wait(6).call(this.frame_365).wait(127).call(this.frame_492).wait(32).call(this.frame_524).wait(47).call(this.frame_571).wait(174));

	// End
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgxB7IBNj1IAWAAIhND1g");
	this.shape.setTransform(1001.25,166.75);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AA1BTIAAgFQAKgCAFgGQADgFAAgRIAAhEQAAgWgCgGQgCgGgDgCQgEgEgFAAQgIAAgHAGQgIAFgIALIAABWQAAARADAFQAEAHALABIAAAFIhSAAIAAgFQAGgBAEgDQAEgDACgFQABgDAAgPIAAhEQAAgWgCgGQgCgFgEgDQgEgEgFAAQgGAAgGAEQgIAFgKANIAABWQAAARAEAGQADAFALACIAAAFIhUAAIAAgFQALgCAEgFQADgEAAgTIAAhbQAAgRgDgFQgEgEgLgCIAAgHIBCAAIAAAWQAOgPALgGQAMgFANAAQAQAAAJAHQALAHAFAPQAOgQANgGQANgIAOABQAQgBALAJQAJAHAEAMQAEAMAAAaIAABBQAAATAEAFQADAEALACIAAAFg");
	this.shape_1.setTransform(981.675,170.15);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("Ag3A8QgUgZAAgiQAAgiAUgaQAUgaAjAAQAUAAATALQASALAKAUQAJAUAAAXQAAAjgRAXQgWAcglAAQgkAAgTgagAgOhCQgGAHgCATQgDAUAAAiQAAATADAQQACAMAGAGQAHAHAHAAQAHAAAGgEQAGgHADgKQADgQAAgyQAAgdgDgLQgEgLgGgFQgFgEgHABQgIgBgGAHg");
	this.shape_2.setTransform(957.85,170.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgvA+QgUgZAAgjQAAgfARgZQAWgfAlAAQAZAAAOANQAPAMAAAQQAAAKgGAGQgGAGgJgBQgLAAgGgGQgHgHgBgRQgCgLgEgFQgEgEgFAAQgIAAgFAJQgJAOAAAbQAAAXAHAUQAHAUAMALQAKAIAMgBQAJAAAIgDQAHgEALgMIAFAFQgLAVgSAKQgRALgTAAQgfAAgTgYg");
	this.shape_3.setTransform(941.35,170.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgTAVQgJgJAAgMQAAgLAJgJQAIgIALAAQAMAAAIAIQAJAJAAALQAAAMgJAJQgIAIgMAAQgLAAgIgIg");
	this.shape_4.setTransform(928.75,176.05);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AAUBeQgNAPgJAGQgJAFgMAAQgfAAgTgcQgOgXAAggQAAgaAJgUQAJgVAQgLQAQgKARAAQAMAAAIAFQAJAEALAMIAAgyQAAgSgCgEQgCgFgEgCQgFgCgKAAIAAgIIBHAAIAAC5QAAAUABADQABAGAFADQADACAKABIAAAGIhEANgAgVgeQgHAFgEAOQgEAMAAAcQAAAeAFAPQAEAPAIAHQAEADAGAAQAPAAAOgYIAAhUQgNgYgSgBQgHAAgDAEg");
	this.shape_5.setTransform(914.75,167);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AAFBTIAAgFQAJgCAFgGQACgFAAgRIAAhHQABgUgCgFQgBgGgFgCQgDgDgFAAQgOAAgNAWIAABVQAAASAEAFQADAFAJACIAAAFIhSAAIAAgFQALgCAEgFQADgEAAgTIAAhbQAAgRgDgFQgDgEgMgCIAAgHIBCAAIAAAWQANgOAKgGQAMgGANAAQAQgBALAKQAKAIADANQADAKAAAcIAAA+QAAATAEAFQADAEAKACIAAAFg");
	this.shape_6.setTransform(894.8,170.15);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgzBLQgLgIgEgLQgDgMAAgcIAAhAQAAgRgDgFQgEgEgLgCIAAgHIBCAAIAABuQAAARACAFQACAFADADQAEADAEAAQAGAAAFgDQAGgFAKgNIAAhXQAAgRgDgFQgEgEgKgCIAAgHIBCAAIAAB+QAAATAEAFQADAEAKACIAAAFIhCAAIAAgVQgLANgLAHQgMAGgOABQgOAAgKgJg");
	this.shape_7.setTransform(875,170.65);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("Ag3A8QgUgZAAgiQAAgiAUgaQAUgaAjAAQAUAAASALQATALAJAUQAKAUAAAXQAAAjgSAXQgVAcglAAQgjAAgUgagAgOhCQgHAHgCATQgCAUAAAiQABATACAQQACAMAGAGQAGAHAIAAQAHAAAFgEQAHgHACgKQAEgQAAgyQAAgdgEgLQgDgLgGgFQgFgEgHABQgIgBgGAHg");
	this.shape_8.setTransform(856.3,170.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgbBPIgHgDQgGABgEAIIgGAAIgCg6IAFAAQAIAXAOAMQANALAMAAQAIAAAFgFQAFgGAAgHQAAgIgFgGQgFgGgRgNQgagSgIgJQgMgNAAgSQAAgSANgQQANgPAZAAQAMAAAMAGQAFADADAAIAFgBIAGgIIAGAAIACA3IgGAAQgKgYgLgJQgMgJgKAAQgHAAgFAFQgFAFAAAGQAAAEADAFQAGAHAYARQAaASAIAKQAIAMAAAPQAAAOgHANQgHANgMAHQgMAHgPAAQgLAAgTgHg");
	this.shape_9.setTransform(840.575,170.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AAFBTIAAgFQAKgCAEgGQACgFAAgRIAAhHQAAgUgBgFQgCgGgEgCQgDgDgEAAQgPAAgNAWIAABVQAAASAEAFQAEAFAIACIAAAFIhSAAIAAgFQALgCAEgFQADgEAAgTIAAhbQAAgRgDgFQgDgEgMgCIAAgHIBCAAIAAAWQANgOAKgGQAMgGANAAQAQgBALAKQAKAIADANQADAKAAAcIAAA+QAAATADAFQAEAEALACIAAAFg");
	this.shape_10.setTransform(823.8,170.15);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("Ag0A6QgPgWAAgiQAAgnAWgYQAWgYAdAAQAYAAATAVQASATABAoIhYAAQACAfAPATQALAOARAAQAKABAJgGQAIgGAKgPIAGAEQgNAbgQALQgQALgVAAQgjAAgTgcgAgLhBQgKAPAAAbIAAAHIAuAAQAAgdgDgJQgDgLgGgFQgEgDgGAAQgIgBgGAJg");
	this.shape_11.setTransform(806.225,170.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgOB0QgKgEgKgKIgbARIgGAAIAAjNQAAgOgBgCQgCgGgEgCQgDgCgJgBIAAgGIBDAAIAABZQATgVAZAAQAQAAAPAKQAPAKAIARQAIASAAAXQAAAbgKAVQgLAWgRALQgSAMgXAAQgMAAgKgEgAgTgPIAABIQAAAWABAGQACAKAGAGQAHAHAJAAQAJAAAGgGQAHgEAEgPQAEgPAAgnQAAgkgJgOQgHgKgLAAQgOAAgOAQg");
	this.shape_12.setTransform(787.875,167);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgTAVQgJgJAAgMQAAgLAJgJQAIgIALAAQAMAAAIAIQAJAJAAALQAAAMgJAJQgIAIgMAAQgLAAgIgIg");
	this.shape_13.setTransform(773.95,176.05);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AAtBUIgth2IgqB2IgJAAIgrh1QgKgbgHgIQgEgFgJgDIAAgHIBSAAIAAAHQgIAAgDACQgEACAAAEQAAADAGAPIAYA+IAXg+IgCgFQgFgNgEgDQgEgDgIgCIAAgHIBSAAIAAAHQgKABgDABQgCACAAAFQAAADAFAOIAXA+IAVg6QAEgMAAgEQAAgHgDgEQgEgCgJgBIAAgHIAwAAIAAAHQgIABgFAFQgEAEgIAWIgtCAg");
	this.shape_14.setTransform(758.625,170.65);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AAtBUIgth2IgqB2IgJAAIgrh1QgKgbgHgIQgEgFgJgDIAAgHIBSAAIAAAHQgIAAgDACQgEACAAAEQAAADAGAPIAYA+IAXg+IgCgFQgFgNgEgDQgEgDgIgCIAAgHIBSAAIAAAHQgKABgDABQgCACAAAFQAAADAFAOIAXA+IAVg6QAEgMAAgEQAAgHgDgEQgEgCgJgBIAAgHIAwAAIAAAHQgIABgFAFQgEAEgIAWIgtCAg");
	this.shape_15.setTransform(732.975,170.65);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AAtBUIgth2IgqB2IgJAAIgrh1QgKgbgHgIQgEgFgJgDIAAgHIBSAAIAAAHQgIAAgDACQgEACAAAEQAAADAGAPIAYA+IAXg+IgCgFQgFgNgEgDQgEgDgIgCIAAgHIBSAAIAAAHQgKABgDABQgCACAAAFQAAADAFAOIAXA+IAVg6QAEgMAAgEQAAgHgDgEQgEgCgJgBIAAgHIAwAAIAAAHQgIABgFAFQgEAEgIAWIgtCAg");
	this.shape_16.setTransform(707.375,170.65);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgwB7IBMj1IAWAAIhND1g");
	this.shape_17.setTransform(689.75,166.75);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgxB7IBNj1IAVAAIhLD1g");
	this.shape_18.setTransform(679.9,166.75);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgTBNQgJgJAAgMQAAgMAJgIQAIgIALAAQAMAAAIAIQAJAIAAAMQAAAMgJAJQgIAJgMgBQgLABgIgJgAgTgjQgJgJAAgMQAAgMAJgIQAIgJALAAQAMAAAIAJQAJAIAAAMQAAAMgJAJQgIAIgMAAQgLAAgIgIg");
	this.shape_19.setTransform(668.975,170.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgbBPIgHgDQgGABgEAIIgGAAIgCg6IAFAAQAIAXAOAMQANALAMAAQAIAAAFgFQAFgGAAgHQAAgIgFgGQgFgGgRgNQgagSgIgJQgMgNAAgSQAAgSANgQQANgPAZAAQAMAAAMAGQAFADADAAIAFgBIAGgIIAGAAIACA3IgGAAQgKgYgLgJQgMgJgKAAQgHAAgFAFQgFAFAAAGQAAAEADAFQAGAHAYARQAaASAIAKQAIAMAAAPQAAAOgHANQgHANgMAHQgMAHgPAAQgLAAgTgHg");
	this.shape_20.setTransform(656.225,170.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AhWB6IAAgHQAKAAAFgGQADgEABgRIAAiqQAAgRgFgEQgDgGgLAAIAAgHIBDAAIAAAWQAJgNAIgFQAMgJAPABQASAAAPALQAOALAIAUQAHAUAAAWQABAZgJAUQgIAUgOALQgQALgSAAQgNAAgLgHQgJgEgJgLIAAA8QAAANADAEQABAFAEACQAEACAMAAIAAAHgAgThKIAABWQAPAUAQABQAJAAAHgKQAIgPABgnQAAgqgLgQQgGgLgLAAQgQABgMAZg");
	this.shape_21.setTransform(639,173.95);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgRBoQgKgJgDgLQgCgGAAgaIAAhXIgVAAIAAgFQAWgQAPgRQAPgRALgVIAGAAIAAA7IAmAAIAAARIgmAAIAABjQAAAPABAEQACAEACACQAEADACAAQAMAAAKgSIAFAFQgOAhggAAQgOAAgLgIg");
	this.shape_22.setTransform(623.75,167.55);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgQBoQgLgJgDgLQgCgGAAgaIAAhXIgVAAIAAgFQAWgQAPgRQAPgRALgVIAGAAIAAA7IAmAAIAAARIgmAAIAABjQAAAPABAEQABAEADACQAEADADAAQALAAAKgSIAFAFQgOAhgfAAQgPAAgKgIg");
	this.shape_23.setTransform(611.95,167.55);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AAFB1IAAgFQAJgCAFgFQACgFAAgSIAAhIQAAgTgBgFQgCgGgDgCQgEgDgEAAQgHAAgGAFQgHAFgIAMIAABVQABASADAEQACAGALACIAAAFIhTAAIAAgFQALgCAEgFQADgEAAgTIAAikQAAgSgDgEQgDgEgMgCIAAgHIBCAAIAABfQAOgPAKgGQALgFAMAAQAPgBAMAKQAKAIADALQAEANAAAaIAAA/QAAATAEAFQADAEALACIAAAFg");
	this.shape_24.setTransform(596.1,166.75);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AA1BUIAAgHQAKAAAFgIQADgEAAgRIAAhEQAAgWgCgGQgCgGgDgDQgEgCgFAAQgIgBgHAGQgIAFgIALIAABWQAAASADAEQAEAGALABIAAAHIhSAAIAAgHQAGAAAEgDQAEgDACgEQABgFAAgOIAAhEQAAgXgCgFQgCgGgEgDQgEgDgFAAQgGAAgGAEQgIAFgKANIAABWQAAASAEAFQADAFALABIAAAHIhUAAIAAgHQALgBAEgGQADgDAAgTIAAhaQAAgTgDgEQgEgFgLgBIAAgGIBCAAIAAAVQAOgPALgFQAMgHANAAQAQABAJAHQALAHAFAPQAOgQANgGQANgIAOAAQAQAAALAJQAJAHAEAMQAEAMAAAaIAABBQAAATAEAEQADAFALABIAAAHg");
	this.shape_25.setTransform(818.625,129.95);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("Ag4A8QgTgaAAghQAAgiAUgaQAUgaAjAAQAVAAASALQASALAKAUQAJAUAAAXQAAAigSAYQgVAcglAAQgkAAgUgagAgOhCQgGAHgCAUQgCATAAAiQgBATADAQQACAMAGAHQAHAGAHAAQAHAAAFgEQAHgHADgKQADgRAAgwQAAgegDgLQgDgLgHgFQgEgDgIgBQgIABgGAGg");
	this.shape_26.setTransform(794.8,130.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AhEBUIAAgHQAKAAAEgGQADgEABgWIAAhYQgBgOgBgEQgCgFgDgCQgDgCgIgBIAAgGIBBAAIAAAkQAQgZAMgIQAMgJALAAQAKAAAGAHQAGAFgBAMQABALgGAHQgFAGgJAAQgJAAgHgGIgHgHIgFAAQgEAAgFADQgHAGgEALQgFARAAATIAAAlIABAKQAAAKABADQABAFAEACQAEACAKAAIAAAHg");
	this.shape_27.setTransform(779.05,129.95);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AhCB4IAAgGQAMAAAEgCQAEgDADgEQABgEAAgNIAAhwIgYAAIAAgRIAYAAIAAgMIAAgIQAAgZASgQQATgRAdAAQAWAAALAIQAKAIAAAKQgBAHgGAGQgGAGgLAAQgJAAgFgFQgFgFgBgFIABgHIABgFQAAgEgCgCQgDgCgEAAQgFAAgDAEQgDAEAAAJIAAAfIAAAUIAYAAIAAARIgYAAIAABwQAAAQADAEQAGAHAPgBIAAAGg");
	this.shape_28.setTransform(766.8,126.275);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("Ag5BBQgNgQAAgTQAAgaAQgaQAQgcAYgPQAXgOAaAAQASgBAJAIQAJAHAAALQAAAOgMANQgPASgdAJQgSAHgjAEIgCAOQAAAQALALQALALAQgBQALAAAMgEQALgFAXgSIAEAFQgpAoglAAQgZAAgNgPgAgIg0QgUATgJAkQAZgDAQgGQAVgKALgNQAKgNAAgNQAAgGgEgFQgFgFgJABQgRAAgTASg");
	this.shape_29.setTransform(862.925,90.45);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AASBNQgEgEAAgGQAAgGADgKQACgNANgqQgdAwgTASQgUATgRgBQgIABgFgGQgGgGAAgIQAAgNAIgbIAPg0QAFgUAAgFIgBgCIgDgCQgEAAgEACQgDADgNAQIgFgDQANgUAOgKQALgIAKABQAGgBAEAFQAEAEAAAGQAAAJgHAZIgQA2QgHAYAAAFQAAAEACACQADACADAAQAGAAAJgGQAJgFAPgUQAPgUAKgQQAKgRALgmIAEgNIAaAAIgeBmQgHAXAAAGQAAADACACQAAAAAAABQABAAAAAAQAAAAABAAQAAAAABAAQADAAADgDQAEgCAOgTIAFAEQgNAWgPAKQgJAHgJgBQgHAAgEgDg");
	this.shape_30.setTransform(845.875,90.45);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AgoB5QgFgFAAgFQAAgHAGgTIAuiiQAFgUABgDQAAgEgEgCQgDgDgGAAIgMABIAAgGIA6gKIg6DNQgFAPAAAEQAAAAABABQAAABAAAAQAAABAAAAQABABAAAAQAAABABAAQAAAAABAAQAAABABAAQAAAAABAAQADAAAFgDQAHgHAKgPIAFAFQgQAYgNAJQgKAGgJAAQgHAAgEgEg");
	this.shape_31.setTransform(833.1,85.975);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AhsB1IACgGQAOAAAFgDQAHgDADgFQAEgGAHgXIAqiOQAFgSAAgHQAAgHgEgDQgFgEgOAAIgFAAIACgGIBTAAQAVAAARAGQAQAHAJAMQAIALAAANQAAAVgPAQQgPAQghAIQAWAHAKAOQAKANAAAQQAAARgJAQQgJAQgOAIQgOAJgUAFQgPACgdAAgAgQAAIgeBmQATADAKAAQAaAAAVgQQAVgRAAgcQAAgVgNgMQgNgLgcAAIgNAAgAANhnIgZBaIAPABQAkAAASgQQARgPAAgYQAAgSgKgKQgLgKgYAAIgQACg");
	this.shape_32.setTransform(815.575,86.35);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AhmB0IABgGQAQgBAGgFQAFgEAGgXIAsiWQAFgOAAgEQgBgFgCgDQgDgCgFAAIgOABIAAgGIA6gJIgMAoQATgXAPgIQAPgJAPAAQAPAAALAMQAMAMAAATQAAAnggAmQgfAngpAAQgHAAgIgBQgGgCgIgFIgNAsQgEAOAAAEQAAADACADQACADAFACQAEABAOAAIgCAGgAAQhPQgSAWgIAcIgQAzQAMANAQAAQAJAAAKgFQAKgFAIgKQAJgJAHgMQAIgNAFgSQAGgSgBgRQAAgOgFgHQgHgIgIAAQgTABgSAVg");
	this.shape_33.setTransform(785.3,94.05);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("Ag5BBQgNgQAAgTQAAgaAQgaQAQgcAYgPQAXgOAaAAQASgBAJAIQAJAHAAALQAAAOgMANQgPASgdAJQgSAHgjAEIgCAOQAAAQALALQALALAQgBQALAAAMgEQALgFAXgSIAEAFQgpAoglAAQgZAAgNgPgAgIg0QgUATgJAkQAZgDAQgGQAVgKALgNQAKgNAAgNQAAgGgEgFQgFgFgJABQgRAAgTASg");
	this.shape_34.setTransform(771.275,90.45);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#000000").s().p("Ag5BBQgNgQAAgTQAAgaAQgaQAQgcAYgPQAXgOAaAAQASgBAJAIQAJAHAAALQAAAOgMANQgPASgdAJQgSAHgjAEIgCAOQAAAQALALQALALAQgBQALAAAMgEQALgFAXgSIAEAFQgpAoglAAQgZAAgNgPgAgIg0QgUATgJAkQAZgDAQgGQAVgKALgNQAKgNAAgNQAAgGgEgFQgFgFgJABQgRAAgTASg");
	this.shape_35.setTransform(755.525,90.45);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#000000").s().p("AiBB1IACgGQAOAAAEgDQAHgDAEgFQAFgGAGgWIApiOQAFgRAAgJQAAgHgEgEQgFgDgNAAIgEAAIACgGIBKAAQAwgBAXALQAXAMAOAXQANAXAAAbQAAAWgHAWQgIAWgKANQgKANgVAQQgVAPgYAIQgYAHgjAAgAgFhoIgzCwQgGATAAAFQAAACACADQACADADABQAEABAKAAQAcAAAWgGQAXgFAPgLQAVgQANgdQANgcAAgjQAAgogUgVQgTgTglAAIgXAAg");
	this.shape_36.setTransform(733.725,86.35);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#000000").s().p("AgTBNQgJgJAAgMQAAgMAJgIQAIgIALAAQAMAAAIAIQAJAIAAAMQAAAMgJAJQgIAJgMgBQgLABgIgJgAgTgjQgJgJAAgMQAAgMAJgIQAIgJALAAQAMAAAIAJQAJAIAAAMQAAAMgJAJQgIAIgMAAQgLAAgIgIg");
	this.shape_37.setTransform(842.475,49.8);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#000000").s().p("AgwA+QgTgZAAgjQAAgfASgZQAVgfAkAAQAaAAAPANQAOAMAAAQQAAAKgFAGQgHAGgKgBQgJAAgIgGQgGgHgCgRQgBgLgDgFQgFgEgFAAQgHAAgGAJQgJAOAAAbQAAAXAHAUQAIAUALALQAJAIAOgBQAIAAAHgDQAJgEAJgMIAGAFQgMAVgQAKQgRALgUAAQgfAAgUgYg");
	this.shape_38.setTransform(828.9,49.8);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#000000").s().p("AgrB4IAAgGQALAAAFgGQADgEAAgRIAAheQAAgRgEgFQgEgFgLAAIAAgHIBDAAIAACAQAAARAEAFQAEAEAMABIAAAGgAgShKQgIgHAAgLQAAgMAIgHQAIgIAKAAQALAAAIAIQAIAHAAAMQAAALgIAHQgIAIgLAAQgKAAgIgIg");
	this.shape_39.setTransform(816.075,45.875);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#000000").s().p("AgbBPIgHgDQgGABgEAIIgGAAIgCg6IAFAAQAIAXAOAMQANALAMAAQAIAAAFgFQAFgGAAgHQAAgIgFgGQgFgGgRgNQgagSgIgJQgMgNAAgSQAAgSANgPQANgQAZAAQAMAAAMAGQAFADADAAIAFgBIAGgIIAGAAIACA3IgGAAQgKgYgLgJQgMgJgKAAQgHAAgFAFQgFAFAAAGQAAAEADAFQAGAHAYARQAaASAIAKQAIAMAAAPQAAAOgHANQgHANgMAHQgMAHgPAAQgLAAgTgHg");
	this.shape_40.setTransform(804.075,49.8);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#000000").s().p("AgzBLQgLgIgEgLQgDgMAAgcIAAhAQAAgRgDgFQgEgEgLgCIAAgHIBCAAIAABuQAAARACAFQACAFADADQAEADAEAAQAGAAAFgDQAGgFAKgNIAAhXQAAgRgDgFQgEgEgKgCIAAgHIBCAAIAAB+QAAATAEAFQADAEAKACIAAAFIhCAAIAAgVQgLANgLAHQgMAGgOABQgOAAgKgJg");
	this.shape_41.setTransform(787.25,50.05);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#000000").s().p("AAmB1IAAgGIAIAAQAKAAAGgEQAFgCACgGQACgEAAgRIAAiuIhZDVIgEAAIhcjTIAACkQAAARABAEQADAJAHAGQAIAFAQAAIAAAGIhRAAIAAgGIADAAQAIABAGgEQAHgCAEgEQADgFACgJIAAgRIAAiTQAAgTgCgEQgCgEgFgEQgGgDgKAAIgIAAIAAgHIBgAAIBBCaIBAiaIBgAAIAAAHIgIAAQgKAAgGADQgFADgCAFQgCAFAAASIAACaQAAASACAEQACAFAFADQAGADAKAAIAIAAIAAAGg");
	this.shape_42.setTransform(760.675,46.15);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#000000").s().p("AgaCRQgQgNgFgOQgCgJAAgkIAAh5IghAAIAAgJQAigVAYgZQAWgXARgeIAJAAIAABTIA7AAIAAAZIg7AAIAACLQAAATACAGQACAFAFAEQAFAEAEAAQASAAAPgZIAIAGQgWAvgwAAQgWAAgRgMg");
	this.shape_43.setTransform(260.45,160.2);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#000000").s().p("AgZCRQgRgNgEgOQgDgJAAgkIAAh5IggAAIAAgJQAhgVAXgZQAYgXAQgeIAJAAIAABTIA6AAIAAAZIg6AAIAACLQAAATACAGQACAFAFAEQAFAEAEAAQARAAAPgZIAIAGQgVAvgwAAQgXAAgPgMg");
	this.shape_44.setTransform(242.4,160.2);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#000000").s().p("AhQBRQgWgfAAgvQgBg3AigiQAhghAsAAQAmAAAbAcQAcAcADA4IiHAAQACAsAYAaQARAVAZgBQARABANgJQANgIAPgVIAJAGQgVAlgYAQQgYAPghAAQg2AAgcgngAgRhbQgQAWAAAlIAAAIIBHAAQAAgngEgOQgFgOgKgIQgGgFgIAAQgOAAgIANg");
	this.shape_45.setTransform(221.55,164.2);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#000000").s().p("AgEB1IhUivQgPgggIgIQgHgHgLgCIAAgJICCAAIAAAJQgMAAgEAEQgGAFAAAIQgBAJAMAYIAoBVIAghJQAOgfABgNQgBgIgFgFQgGgFgPAAIAAgJIBRAAIAAAJQgMABgHAHQgIAHgPAfIhSCyg");
	this.shape_46.setTransform(195.55,164.55);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#000000").s().p("AhVBUQgegjAAgwQAAgvAfglQAeglA2ABQAfAAAdAPQAcAPAOAcQAOAdAAAgQAAAxgbAgQggAng5AAQg3AAgegkgAgVhcQgLAIgCAdQgDAbAAAwQAAAaADAXQADARAKAIQAJAJAMAAQAMAAAHgFQALgJADgPQAGgWgBhGQAAgpgEgPQgFgQgLgHQgGgFgMAAQgMAAgJAKg");
	this.shape_47.setTransform(168.45,164.2);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#000000").s().p("AilClIAAgJIAMAAQAQAAAJgFQAHgEAEgIQACgGAAgZIAAjXQAAgZgCgHQgDgGgJgFQgJgFgPAAIgMAAIAAgJIC/AAIAAAJIgRAAQgOAAgKAGQgHADgEAIQgCAGAAAZIAADRQAAAZADAHQADAHAKADQAGACAYAAIAeAAQAdAAATgJQATgJAOgUQAOgUAMgnIALAAIgOB0g");
	this.shape_48.setTransform(136.4,159.075);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#000000").s().p("AAJB1IAAgIQANgDAHgJQADgFAAgZIAAhjQAAgcgCgIQgCgHgGgEQgGgEgGAAQgWAAgUAeIAAB4QAAAaAGAHQAFAGAOADIAAAIIh9AAIAAgIQAPgCAGgIQAFgFAAgbIAAh+QAAgbgFgGQgFgGgQgCIAAgJIBkAAIAAAeQAUgTAQgJQASgJATAAQAZAAAQANQAPALAFATQAEAOABAnIAABXQgBAbAGAGQAFAGARADIAAAIg");
	this.shape_49.setTransform(281.45,107.55);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#000000").s().p("AhVBVQgegkAAgwQAAgwAfgkQAfglA1AAQAgABAcAPQAbAPAPAdQAOAbAAAhQAAAwgbAhQggAog5AAQg3gBgegjgAgVhdQgLAKgCAbQgDAbgBAxQAAAaAEAWQADARAKAKQAKAJALAAQAMAAAHgHQAKgIAEgOQAGgYAAhFQgBgpgFgQQgEgPgLgHQgGgFgMAAQgMAAgJAJg");
	this.shape_50.setTransform(252.7,107.9);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#000000").s().p("AAeCEQgTAVgOAIQgPAHgSAAQgwAAgbgnQgWgfAAguQAAglANgcQAPgdAXgPQAZgPAbAAQASAAANAHQANAGAQAQIAAhFQAAgagCgFQgEgHgGgDQgGgEgRAAIAAgKIBsAAIAAEEQAAAbACAFQADAIAFADQAHAEAOABIAAAIIhoATgAghgrQgKAIgGATQgGASgBAnQAAArAHAUQAHAVAMAJQAGAFALAAQAWAAAVgiIAAh2QgUgjgbAAQgKAAgGAFg");
	this.shape_51.setTransform(224.55,103.125);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#000000").s().p("AAJB1IAAgIQANgDAHgJQADgFAAgZIAAhjQAAgcgCgIQgCgHgGgEQgGgEgGAAQgWAAgUAeIAAB4QAAAaAGAHQAFAGAOADIAAAIIh9AAIAAgIQAPgCAGgIQAFgFAAgbIAAh+QAAgbgFgGQgFgGgQgCIAAgJIBkAAIAAAeQAUgTAQgJQASgJATAAQAZAAAQANQAPALAFATQAEAOABAnIAABXQgBAbAGAGQAFAGARADIAAAIg");
	this.shape_52.setTransform(194.15,107.55);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#000000").s().p("AAaBtQgMgIgCgTQgtAkgkAAQgVAAgOgMQgOgNAAgTQAAgaAYgVQAZgUBRgiIAAgXQAAgZgDgHQgEgHgHgFQgIgFgLAAQgRAAgLAHQgHAFAAAFQAAAFAHAIQAKAKAAAKQAAAMgJAJQgKAIgQAAQgQAAgMgJQgLgKAAgMQAAgRAPgQQAPgRAbgIQAbgIAdgBQAjABAUAOQAVANAGAQQADALAAAkIAABXQAAAPACAFQABAEADACQACACAEAAQAHAAAHgJIAIAGQgOASgOAHQgOAIgSAAQgVAAgMgJgAgjAZQgKANAAAOQAAALAIAIQAHAHAMAAQANAAARgOIAAhKQgfAPgQAUg");
	this.shape_53.setTransform(166.175,107.7);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#000000").s().p("AhpB1IAAgIQAQgCAGgHQAFgHAAgdIAAh9QAAgTgCgFQgDgIgFgCQgFgEgMgBIAAgJIBlAAIAAAzQAXgjATgLQASgMARAAQAPAAAJAIQAJAJAAAPQAAAQgIAKQgJAJgMAAQgOAAgLgJIgMgJQgCgCgEABQgIgBgGAGQgLAIgGAQQgHAXAAAcIAAA0IAAANQAAAOACAEQACAGAGADQAGADAPACIAAAIg");
	this.shape_54.setTransform(140.425,107.55);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#000000").s().p("AilClIAAgJQAZAAAJgEQAJgFADgHQAEgGAAgbIAAjVQAAgagEgHQgDgHgJgEQgJgFgZAAIAAgJICrAAQA+AAAaALQAaAKAOAUQAPAUAAAWQAAAYgTATQgSASgwALQAzAKAVAQQAeAWAAAjQgBAlggAYQgoAehNAAgAgfADIAABpIAAAMQAAANAHAHQAHAHAOAAQAVAAARgJQASgIAJgRQAKgQAAgUQAAgXgMgTQgLgSgUgHQgUgHgkAAIgEAAgAgfgPQAlAAARgIQASgIAJgOQAKgOAAgWQAAgWgJgOQgKgOgRgHQgSgHglAAg");
	this.shape_55.setTransform(109.35,102.775);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#000000").s().p("AgeBsQgNgMAAgQQAAgRANgMQAMgMASAAQASAAANAMQANAMAAARQAAAQgNAMQgNAMgSAAQgSAAgMgMgAgegyQgNgMAAgRQAAgQANgMQANgMARAAQASAAANAMQANAMAAAQQAAARgNAMQgNAMgSAAQgRAAgNgMg");
	this.shape_56.setTransform(355.8,51.525);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#000000").s().p("AhpCcQgOgMAAgQQAAgOAJgKQAKgJANAAQAOAAAIAIQAIAIAAAQQAAAJACADQADADADAAQAHAAAHgHQALgLAOgkIAIgTIhQinQgSgmgJgJQgJgJgMgDIAAgJICDAAIAAAJQgMAAgGAEQgFAEAAAGQAAAJANAcIAqBZIAdhFQAPglAAgOQAAgJgGgFQgHgGgQAAIAAgJIBTAAIAAAJQgNABgHAHQgIAIgSArIhHCnQgZA/gNAPQgSAUgcAAQgWAAgNgLg");
	this.shape_57.setTransform(333.175,56.925);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#000000").s().p("AilClIAAgJQAZAAAJgEQAJgFADgHQAEgGAAgbIAAjVQAAgagEgHQgDgHgJgEQgJgFgZAAIAAgJICrAAQA+AAAZALQAaAKAPAUQAPAUAAAWQAAAYgTATQgTASgvALQAzAKAVAQQAdAWAAAjQAAAlggAYQgoAehMAAgAggADIAABpIAAAMQABANAHAHQAHAHAOAAQAUAAATgJQARgIAKgRQAJgQAAgUQAAgXgMgTQgLgSgUgHQgUgHgkAAIgFAAgAgggPQAmAAARgIQASgIAJgOQAKgOAAgWQAAgWgJgOQgKgOgRgHQgRgHgnAAg");
	this.shape_58.setTransform(301.25,46.425);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#000000").s().p("AAJB1IAAgJQAOgBAFgKQAEgFABgYIAAhkQgBgcgCgIQgCgHgGgEQgFgEgIAAQgVAAgUAeIAAB5QABAZAFAHQAEAGAPACIAAAJIh+AAIAAgJQAQgBAHgIQAEgGAAgZIAAh/QAAgbgFgGQgFgGgRgCIAAgJIBlAAIAAAdQAUgSAQgJQARgJAVAAQAXAAARANQAPALAFATQAFAOAAAnIAABYQgBAaAGAGQAFAGARACIAAAJg");
	this.shape_59.setTransform(255.1,51.2);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#000000").s().p("AhVBVQgeglAAgvQAAgwAfgkQAegkA2AAQAgAAAbAPQAcAPAPAcQAOAdAAAgQAAAwgbAhQggAog5AAQg2gBgfgjgAgWhcQgKAJgCAbQgDAbgBAxQAAAaAFAXQACAQAJAKQALAIALABQALAAAJgHQAJgIAEgOQAFgYABhFQAAgpgGgPQgFgQgJgHQgIgFgKAAQgNAAgKAKg");
	this.shape_60.setTransform(226.35,51.55);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#000000").s().p("AhCCpIAAgJQARgBAIgIQAFgGAAgXIAAiEQAAgYgGgHQgGgGgSgBIAAgJIBnAAIAACzQAAAXAGAHQAGAHASABIAAAJgAgchnQgMgMAAgPQAAgQAMgKQAMgMAQAAQARAAAMAMQAMAKAAAQQAAAPgMAMQgMAKgRABQgQgBgMgKg");
	this.shape_61.setTransform(205.525,46.05);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#000000").s().p("AgZCRQgRgMgEgPQgDgIAAglIAAh6IggAAIAAgIQAhgWAXgYQAYgXAQgeIAJAAIAABUIA6AAIAAAXIg6AAIAACMQAAATACAGQACAFAFAFQAFACAEAAQARAAAPgYIAIAGQgVAugwAAQgXAAgPgLg");
	this.shape_62.setTransform(188.95,47.55);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#000000").s().p("AAaBtQgMgIgCgTQgtAkgkAAQgVAAgOgMQgOgNAAgTQAAgaAYgVQAZgUBRgiIAAgXQAAgZgDgHQgEgHgHgFQgIgFgLAAQgRAAgLAHQgHAFAAAGQAAAEAHAIQAKALAAAJQAAAMgJAJQgKAIgQAAQgQAAgMgJQgLgJAAgNQAAgSAPgPQAPgRAbgIQAbgIAdAAQAjgBAUAOQAVAOAGAQQADALAAAkIAABXQAAAPACAFQABADADADQACACAEAAQAHgBAHgIIAIAFQgOATgOAHQgOAJgSgBQgVABgMgKgAgjAZQgKANAAAOQAAALAIAIQAHAHAMAAQANAAARgOIAAhKQgfAQgQATg");
	this.shape_63.setTransform(166.925,51.35);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#000000").s().p("ABRB1IAAgJQAPgBAHgKQAFgGAAgXIAAhhQAAgfgDgIQgDgIgFgEQgGgEgIAAQgLAAgMAIQgLAHgMAPIAAB6QAAAYAEAGQAGAJASABIAAAJIh/AAIAAgJQAKAAAGgEQAGgEACgGQACgHAAgTIAAhhQAAgfgDgIQgCgHgHgFQgGgEgHAAQgLAAgIAFQgNAIgOARIAAB6QAAAYAFAHQAFAIARABIAAAJIiAAAIAAgJQAQgBAHgIQAEgGAAgZIAAh/QAAgbgFgGQgFgGgRgCIAAgJIBlAAIAAAeQAVgVARgIQARgIAVAAQAYAAAPALQAQAJAJAVQAVgWATgJQAUgKAVAAQAaAAAPALQAPALAGARQAGAQAAAlIAABcQAAAaAFAGQAFAGARACIAAAJg");
	this.shape_64.setTransform(130.325,51.2);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#000000").s().p("AhCCpIAAgJQARgBAIgIQAFgGAAgXIAAiEQAAgYgGgHQgGgGgSgBIAAgJIBnAAIAACzQAAAXAGAHQAGAHASABIAAAJgAgchnQgMgMAAgPQAAgQAMgKQAMgMAQAAQARAAAMAMQAMAKAAAQQAAAPgMAMQgMAKgRABQgQgBgMgKg");
	this.shape_65.setTransform(100.225,46.05);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#000000").s().p("AAIB1IAAgJQAPgBAFgKQAFgFAAgYIAAhkQAAgcgCgIQgDgHgFgEQgHgEgHAAQgVAAgTAeIAAB5QAAAZAEAHQAFAGAOACIAAAJIh9AAIAAgJQAQgBAHgIQAEgGAAgZIAAh/QAAgbgFgGQgFgGgRgCIAAgJIBmAAIAAAdQATgSAQgJQARgJAVAAQAYAAAPANQAQALAGATQADAOAAAnIAABYQABAaAFAGQAFAGARACIAAAJg");
	this.shape_66.setTransform(77.55,51.2);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#000000").s().p("AARCoIAAgJIAHAAQAVAAAIgFQAHgEgBgHIgBgJIgIgSIgTgqIh+AAIgPAhQgIAPAAALQAAAOANAGQAHAEAcACIAAAJIh4AAIAAgJQAUgCANgMQAMgMASgnIB/kFIAGAAICBENQASAmANAKQAIAIARABIAAAJgAhVA4IBrAAIg0hwg");
	this.shape_67.setTransform(42.65,46.1);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#000000").s().p("AAsDUQgcAigVAMQgWAMgbAAQhGAAgog+QghgzAAhKQAAg7AUgtQAUgvAkgYQAjgYApAAQAaAAATAKQAVAKAXAaIAAhuQgBgqgDgJQgEgLgKgGQgKgFgYAAIAAgQIChAAIAAGjQAAAqACAIQAEANAJAGQAIAHAWABIAAANIiaAegAgyhFQgPAMgIAeQgKAeABA/QgBBFALAhQAKAhARAPQAJAHAQAAQAhAAAfg2IAAi/Qgdg3gpAAQgPAAgJAIg");
	this.shape_68.setTransform(534.65,142.375);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#000000").s().p("AAMC8IAAgOQAVgDAJgOQAGgKAAgmIAAiiQAAgtgEgMQgDgMgIgGQgIgHgLABQgggBgdAxIAADDQgBApAJAKQAHALAVADIAAAOIi5AAIAAgOQAXgCAKgNQAGgJABgpIAAjOQAAgpgIgKQgIgLgYgDIAAgOICVAAIAAAvQAcgfAZgNQAagOAdAAQAjAAAYAUQAXAUAIAcQAGAXABA/IAACOQAAApAIALQAHAKAYADIAAAOg");
	this.shape_69.setTransform(489.75,149.5);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#000000").s().p("AjwEJIAAgPIASAAQAXAAAOgIQAKgFAGgOQAEgJAAgoIAAlbQAAgkgCgIQgEgOgLgIQgOgKgaAAIgSAAIAAgPIG7AAIAACdIgPAAQgMg6gUgZQgVgZglgLQgWgHg8AAIg2AAIAADXIALAAQAxAAAXgfQAWggAHg+IAPAAIAAEVIgPAAQgFgugPgdQgPgdgUgJQgTgLgmAAIAACWQAAArAEAKQAEAKAKAGQAKAGAWAAIAfAAQBKAAAtgiQAtgiAThHIAPAAIgYCpg");
	this.shape_70.setTransform(439.625,141.825);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#000000").s().p("Ah2CDQgigzAAhKQAAhbAyg1QAxg2BBAAQA4AAApAtQAoAtAEBaIjIAAQAEBHAjAqQAaAhAmAAQAXAAATgNQATgNAWgiIAOAIQgeA9gkAaQgkAZgvAAQhQAAgqg/gAgaiTQgXAiAAA9IAAAOIBpAAQAAg/gGgYQgHgXgPgMQgIgIgOAAQgTAAgNAVg");
	this.shape_71.setTransform(534.575,59.45);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#000000").s().p("AAMEJIAAgOQAUgDAJgNQAHgKAAgoIAAiiQAAgtgDgLQgEgMgIgGQgJgHgKAAQgPAAgPALQgOALgSAbIAADCQAAAoAGAJQAIAOAXADIAAAOIi5AAIAAgOQAXgDAJgMQAHgJAAgqIAAlyQAAgpgHgKQgIgKgYgDIAAgPICUAAIAADTQAfgfAYgNQAZgNAaAAQAjAAAYATQAZAUAIAaQAIAaAAA+IAACOQAAAqAHALQAIAKAYADIAAAOg");
	this.shape_72.setTransform(494.225,51.225);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#000000").s().p("AiKEJIAAgPIASAAQAXAAAOgIQAKgFAGgOQAEgJAAgoIAAmXIgmAAQg2AAgYAWQgiAggJA6IgPAAIAAiPIHbAAIAACPIgOAAQgNgxgPgWQgPgWgbgMQgOgHgmAAIgnAAIAAGXQAAApAFAKQAEAKANAIQANAHAXAAIARAAIAAAPg");
	this.shape_73.setTransform(445.075,51.225);

	this.title_btn1 = new lib.Title_btn();
	this.title_btn1.name = "title_btn1";
	this.title_btn1.setTransform(462.9,226.8,0.5051,0.5051);
	new cjs.ButtonHelper(this.title_btn1, 0, 1, 1);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#00FF00").s().p("AgrBtIBEjZIATAAIhEDZg");
	this.shape_74.setTransform(999.05,171.225);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#00FF00").s().p("AAvBKIAAgGQAJAAAEgHQADgEAAgPIAAg8QAAgTgCgFQgBgGgDgCQgEgDgEAAQgHAAgHAFQgGAFgHAKIAABLQAAAQACAEQAEAGAKAAIAAAGIhKAAIAAgGQAGAAAEgCQADgDACgEQABgEAAgNIAAg8QAAgTgCgFQgBgGgEgCQgEgDgEAAQgGAAgFADQgHAFgIAMIAABLQAAAQADAFQADAEAJABIAAAGIhKAAIAAgGQAJgBAEgEQACgEAAgRIAAhQQAAgQgCgEQgEgEgJgBIAAgGIA7AAIAAATQALgNALgFQAKgFAMAAQANAAAJAGQAJAHAFANQAMgOAMgGQALgGAMAAQAPAAAJAHQAJAGAEALQADAKAAAYIAAA5QAAARADAEQADAEAJABIAAAGg");
	this.shape_75.setTransform(981.625,174.25);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#00FF00").s().p("AgxA1QgRgWAAgeQAAgeASgXQARgXAfAAQASAAARAJQAQAKAIASQAIASAAAUQAAAfgPAUQgTAZghAAQgfAAgSgXgAgMg6QgGAFgBASQgCARAAAeQAAARACAOQACALAFAGQAGAFAGAAQAHAAAEgDQAGgGACgJQADgPAAgrQAAgagDgKQgDgKgFgEQgEgDgHAAQgHAAgFAGg");
	this.shape_76.setTransform(960.475,174.475);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#00FF00").s().p("AgqA2QgRgVAAgfQAAgcAPgWQATgbAhAAQAWAAANALQANALAAAOQAAAJgFAFQgFAFgJAAQgJAAgGgGQgGgGgBgPQgBgKgEgEQgDgEgFAAQgHAAgFAIQgIAMAAAZQAAAUAHARQAGATAKAJQAJAHALAAQAIAAAHgEQAGgDAKgKIAEADQgKATgPAKQgPAJgRAAQgcAAgRgWg");
	this.shape_77.setTransform(945.825,174.475);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#00FF00").s().p("AgRASQgIgHAAgLQAAgKAIgHQAHgIAKAAQAKAAAIAIQAHAHABAKQgBALgHAHQgIAIgKAAQgKAAgHgIg");
	this.shape_78.setTransform(934.7,179.475);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#00FF00").s().p("AASBUQgLANgIAFQgJAEgLAAQgbAAgQgYQgNgVAAgdQAAgXAIgRQAIgSAOgKQAOgJAQAAQAKAAAIAEQAHAEAKAKIAAgrQAAgRgCgEQgCgEgDgCQgEgCgKAAIAAgGIA/AAIAACkQAAARACADQAAAFAEACQADADAJAAIAAAFIg8AMgAgTgbQgFAFgEAMQgEALAAAYQAAAcAEANQAEANAHAGQADADAHAAQAMAAANgVIAAhLQgMgWgPAAQgGAAgEADg");
	this.shape_79.setTransform(922.25,171.45);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#00FF00").s().p("AAEBKIAAgGQAJAAADgHQADgDAAgQIAAg/QAAgRgBgEQgCgFgDgDQgDgCgFAAQgMgBgLAUIAABLQAAARADAEQADAEAIABIAAAGIhJAAIAAgGQAKgBADgEQADgEAAgRIAAhQQAAgQgDgEQgDgEgKgBIAAgGIA7AAIAAATQALgMAJgGQAKgFAMAAQAOAAAJAIQAKAHACAMQADAJAAAYIAAA3QAAARADAFQADADAJABIAAAGg");
	this.shape_80.setTransform(904.6,174.25);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#00FF00").s().p("AgtBDQgKgIgDgKQgDgKAAgZIAAg4QAAgQgDgEQgDgEgKgBIAAgGIA7AAIAABhQAAAQACAEQABAFADACQADACAEAAQAFAAAEgDQAHgEAIgMIAAhMQAAgQgDgEQgDgEgJgBIAAgGIA6AAIAABwQAAAQADAEQADAEAKABIAAAGIg7AAIAAgTQgKAMgJAGQgLAFgNAAQgMAAgJgHg");
	this.shape_81.setTransform(886.975,174.675);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#00FF00").s().p("AgxA1QgRgWAAgeQAAgeASgXQARgXAfAAQASAAARAJQAQAKAIASQAIASAAAUQAAAfgPAUQgTAZghAAQgfAAgSgXgAgMg6QgGAFgBASQgCARAAAeQAAARACAOQACALAFAGQAGAFAGAAQAHAAAEgDQAGgGACgJQADgPAAgrQAAgagDgKQgDgKgFgEQgEgDgHAAQgHAAgFAGg");
	this.shape_82.setTransform(870.375,174.475);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#00FF00").s().p("AgYBGIgHgCQgEAAgEAIIgFAAIgDg0IAGAAQAGAUANAKQALALALAAQAHAAAFgFQAEgFAAgGQAAgHgEgGQgFgFgPgLQgYgQgHgIQgKgNAAgPQAAgQAMgOQAMgNAVAAQAKAAAMAFQAEADACAAIAFgBIAFgHIAFAAIADAxIgFAAQgKgWgKgIQgKgIgJAAQgHAAgEAFQgEAEAAAFQAAAEADAEQAFAGAWAQQAWAPAHAKQAIAKgBAOQABAMgHALQgGAMgKAGQgLAGgNAAQgKAAgRgGg");
	this.shape_83.setTransform(856.45,174.475);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#00FF00").s().p("AAEBKIAAgGQAJAAADgHQADgDAAgQIAAg/QAAgRgBgEQgCgFgDgDQgDgCgFAAQgMgBgLAUIAABLQAAARADAEQADAEAIABIAAAGIhJAAIAAgGQAKgBADgEQADgEAAgRIAAhQQAAgQgDgEQgDgEgKgBIAAgGIA7AAIAAATQALgMAJgGQAKgFAMAAQAOAAAJAIQAKAHACAMQADAJAAAYIAAA3QAAARADAFQADADAJABIAAAGg");
	this.shape_84.setTransform(841.6,174.25);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#00FF00").s().p("AguAzQgNgTAAgeQAAgjATgVQAUgVAZAAQAWAAAQASQAQARABAkIhOAAQACAbANARQAKANAPAAQAJAAAIgFQAHgGAJgNIAFADQgMAYgOAKQgOAKgSAAQgfAAgRgZgAgJg5QgKANAAAYIAAAFIApAAQAAgYgCgKQgDgJgGgFQgDgCgFAAQgHAAgFAIg");
	this.shape_85.setTransform(825.975,174.475);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#00FF00").s().p("AgNBnQgJgEgJgIIgXAPIgFAAIAAi1QAAgNgBgDQgCgFgDgBQgEgDgIAAIAAgFIA9AAIAABOQAQgSAWAAQAPAAANAIQANAKAHAPQAHAPAAAVQAAAXgJAUQgJATgQAKQgPAKgVAAQgKAAgKgDgAgQgNIAAA/IAAAZQABAKAHAFQAGAFAHAAQAJAAAFgEQAFgEAFgNQADgOAAgjQAAgfgIgNQgGgIgLgBQgMAAgLAPg");
	this.shape_86.setTransform(809.65,171.45);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#00FF00").s().p("AgRASQgHgHgBgLQABgKAHgHQAIgIAJAAQALAAAHAIQAIAHAAAKQAAALgIAHQgHAIgLAAQgJAAgIgIg");
	this.shape_87.setTransform(797.35,179.475);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#00FF00").s().p("AAoBKIgnhoIgmBoIgIAAIgmhnQgJgYgGgHQgEgFgIgCIAAgGIBJAAIAAAGQgHAAgDACQgDACAAADIAFAPIAVA4IAUg4IgBgDQgFgMgDgDQgEgDgHgBIAAgGIBJAAIAAAGQgJAAgCACQgDACAAAEQAAADAFALIAUA4IATgzQAEgLAAgEQAAgGgDgDQgDgDgJAAIAAgGIArAAIAAAGQgHABgEAEQgEAEgHATIgoBxg");
	this.shape_88.setTransform(783.725,174.675);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#00FF00").s().p("AAoBKIgnhoIgmBoIgIAAIgmhnQgJgYgGgHQgEgFgIgCIAAgGIBJAAIAAAGQgHAAgDACQgDACAAADIAFAPIAVA4IAUg4IgBgDQgFgMgDgDQgEgDgHgBIAAgGIBJAAIAAAGQgJAAgCACQgDACAAAEQAAADAFALIAUA4IATgzQAEgLAAgEQAAgGgDgDQgDgDgJAAIAAgGIArAAIAAAGQgHABgEAEQgEAEgHATIgoBxg");
	this.shape_89.setTransform(760.975,174.675);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#00FF00").s().p("AAoBKIgnhoIgmBoIgIAAIgmhnQgJgYgGgHQgEgFgIgCIAAgGIBJAAIAAAGQgHAAgDACQgDACAAADIAFAPIAVA4IAUg4IgBgDQgFgMgDgDQgEgDgHgBIAAgGIBJAAIAAAGQgJAAgCACQgDACAAAEQAAADAFALIAUA4IATgzQAEgLAAgEQAAgGgDgDQgDgDgJAAIAAgGIArAAIAAAGQgHABgEAEQgEAEgHATIgoBxg");
	this.shape_90.setTransform(738.275,174.675);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#00FF00").s().p("AgrBtIBEjZIATAAIhEDZg");
	this.shape_91.setTransform(722.65,171.225);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#00FF00").s().p("AgrBtIBDjZIAUAAIhDDZg");
	this.shape_92.setTransform(713.9,171.225);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#00FF00").s().p("AgRBEQgHgHgBgLQABgKAHgIQAIgIAJABQALgBAHAIQAIAIAAAKQAAALgIAHQgHAIgLAAQgJAAgIgIgAgRggQgHgHgBgLQABgKAHgIQAIgHAJAAQALAAAHAHQAIAIAAAKQAAALgIAHQgHAIgLAAQgJAAgIgIg");
	this.shape_93.setTransform(704.2,174.45);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#00FF00").s().p("AgYBGIgGgCQgFAAgEAIIgFAAIgDg0IAGAAQAGAUANAKQALALALAAQAHAAAFgFQAEgFAAgGQAAgHgEgGQgFgFgPgLQgXgQgHgIQgLgNABgPQAAgQALgOQALgNAWAAQALAAAKAFQAFADACAAIAFgBIAFgHIAGAAIACAxIgGAAQgJgWgKgIQgKgIgJAAQgHAAgDAFQgFAEAAAFQAAAEADAEQAFAGAWAQQAWAPAHAKQAIAKAAAOQAAAMgHALQgFAMgMAGQgLAGgMAAQgKAAgRgGg");
	this.shape_94.setTransform(692.9,174.475);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#00FF00").s().p("AhNBsIAAgGQAKAAAEgFQADgEAAgPIAAiXQAAgPgDgEQgEgEgKgBIAAgGIA9AAIAAATQAHgLAIgFQAKgHANAAQAQAAANAKQANAKAHASQAHARAAAVQAAAWgHARQgHASgOAJQgNAKgQAAQgMAAgJgGQgIgEgIgJIAAA1QAAALABAEQACAEAEACQADACAKAAIAAAGgAgQhCIAABMQANATAOAAQAIAAAGgJQAIgNAAgjQAAglgJgOQgGgJgKAAQgOAAgKAWg");
	this.shape_95.setTransform(677.575,177.625);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#00FF00").s().p("AgOBcQgKgIgDgJQgBgGgBgXIAAhMIgSAAIAAgGQATgNAOgPQAOgQAIgSIAGAAIAAA0IAiAAIAAAQIgiAAIAABXQgBANACAEQABADADACQADADACAAQALAAAIgQIAFAEQgNAdgcAAQgNAAgIgHg");
	this.shape_96.setTransform(664.1,171.925);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#00FF00").s().p("AgPBcQgJgIgDgJQgCgGAAgXIAAhMIgSAAIAAgGQATgNAOgPQAOgQAJgSIAEAAIAAA0IAjAAIAAAQIgjAAIAABXQAAANACAEQABADADACQADADACAAQAKAAAJgQIAFAEQgMAdgcAAQgNAAgKgHg");
	this.shape_97.setTransform(653.6,171.925);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#00FF00").s().p("AAEBoIAAgFQAJgBADgFQADgEAAgQIAAhAQAAgRgBgEQgCgFgDgCQgEgDgEAAQgFAAgFAEQgHAEgGALIAABMQAAAPACAEQADAFAJACIAAAFIhJAAIAAgFQAKgBADgFQADgEAAgQIAAiRQAAgQgDgEQgDgEgKgCIAAgFIA7AAIAABTQALgNAKgFQAKgFAKAAQANAAAKAIQAJAIAEAKQADAKAAAYIAAA4QAAAQADAFQADAEAJABIAAAFg");
	this.shape_98.setTransform(639.55,171.225);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#00FF00").s().p("AAvBKIAAgGQAJAAAEgHQADgEAAgPIAAg8QAAgUgCgEQgBgGgDgCQgEgDgEAAQgHAAgHAFQgGAFgHAKIAABLQAAAQACAEQAEAGAKAAIAAAGIhKAAIAAgGQAGAAAEgCQADgDACgEQABgEAAgNIAAg8QAAgUgCgEQgBgGgEgCQgEgDgEAAQgGAAgFADQgHAFgIAMIAABLQAAAQADAFQADAEAJABIAAAGIhKAAIAAgGQAJAAAEgGQACgDAAgRIAAhQQAAgQgCgEQgEgEgJgBIAAgGIA7AAIAAATQALgNALgFQAKgFAMAAQANAAAJAGQAJAHAFANQAMgOAMgGQALgGAMAAQAPAAAJAHQAJAHAEAKQADAKAAAYIAAA5QAAARADAEQADAEAJABIAAAGg");
	this.shape_99.setTransform(836.975,138.6);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#00FF00").s().p("AgxA1QgRgWAAgeQAAgeASgXQARgXAfAAQASAAARAJQAQAKAIASQAIASAAAUQAAAfgPAUQgTAZghAAQgfAAgSgXgAgMg6QgGAFgBASQgCARAAAeQAAARACAOQACALAFAGQAGAFAGAAQAHAAAEgDQAGgGACgJQADgPAAgrQAAgagDgKQgDgKgFgEQgEgDgHAAQgHAAgFAGg");
	this.shape_100.setTransform(815.825,138.825);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#00FF00").s().p("Ag9BKIAAgGQAKAAADgEQADgFAAgTIAAhPQAAgMgBgDQgBgEgDgCQgDgCgIgBIAAgGIA7AAIAAAhQANgWALgIQALgHAKAAQAIAAAGAGQAFAEAAAKQAAAKgFAGQgFAGgHAAQgIAAgGgGIgIgFIgDgBQgFAAgEAEQgGAEgDAKQgEAPAAARIAAAhIAAAJQAAAIABADQABAEAEACQADACAJAAIAAAGg");
	this.shape_101.setTransform(801.875,138.6);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#00FF00").s().p("Ag6BrIAAgGQAKAAAEgCQAEgCABgEQACgDAAgMIAAhjIgVAAIAAgPIAVAAIAAgLIAAgHQAAgWAQgPQARgPAaAAQATAAAJAIQAJAHAAAIQAAAHgFAFQgGAFgKAAQgHAAgFgEQgFgEAAgFIABgGIAAgFQAAAAAAgBQAAgBAAAAQAAgBAAAAQgBgBAAAAQgDgDgDAAQgEAAgDAEQgEAEAAAIIAAAbIAAASIAWAAIAAAPIgWAAIAABjQAAAOAEAEQAEAGAOgBIAAAGg");
	this.shape_102.setTransform(790.95,135.325);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#00FF00").s().p("AgzA5QgLgOAAgRQAAgXAOgXQAOgYAWgNQAUgNAXAAQAQAAAIAGQAIAHAAAJQAAANgKALQgOAQgaAIQgQAGgfAEIgBANQAAANAJAKQAKAJAOAAQAKAAAKgEQAKgFAVgPIADAEQglAkggAAQgWAAgMgOgAgHguQgSARgIAfQAXgCANgGQATgIAJgMQAKgMAAgKQAAgHgEgEQgEgEgIAAQgQAAgQARg");
	this.shape_103.setTransform(876.325,103.575);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("#00FF00").s().p("AARBEQgEgDAAgGIACgOIAOgwQgaAqgRAQQgRAQgPAAQgHAAgGgEQgEgFAAgIQAAgLAGgYIAOgvQAFgRgBgEIgBgDIgDgBQgCAAgEACIgOARIgGgEQAMgRANgJQAKgGAJAAQAFAAAEADQADAEAAAGQAAAIgGAVIgOAxQgGAUgBAGQAAAAABABQAAABAAAAQAAABABAAQAAABABAAQACACACAAQAGAAAIgFQAIgFANgSQANgRAJgOQAJgQAKghIADgMIAYAAIgbBbQgGAUAAAGIABAEQAAAAABAAQAAABAAAAQAAAAABAAQAAAAAAAAQAEAAACgCIARgTIAEADQgMAUgNAJQgJAFgHAAQgGAAgDgDg");
	this.shape_104.setTransform(861.15,103.575);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f("#00FF00").s().p("AgkBrQgEgEAAgFQAAgGAFgRIApiPQAFgSAAgDQAAgDgDgCQgDgDgFAAIgLABIAAgFIA0gJIg0C2IgEAQQAAABAAABQAAAAABABQAAAAAAABQABAAAAAAQAAABABAAQAAAAABABQAAAAABAAQAAAAABAAQACAAAEgDQAHgGAJgNIAEAFQgOAUgMAJQgJAFgIAAQgGAAgEgEg");
	this.shape_105.setTransform(849.825,99.6);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#00FF00").s().p("AhgBoIACgFQAMgBAEgCQAHgCACgFQAEgGAGgUIAlh+QAGgQgBgGQAAgGgEgDQgEgDgMAAIgFgBIACgFIBKAAQASAAAPAGQAPAFAHALQAIAKAAAMQAAASgOAPQgNAOgdAHQATAGAJAMQAJALAAAOQAAAQgIAOQgIAOgNAIQgNAIgRAEQgMACgaAAgAgPAAIgaBbIAaACQAXAAATgPQASgOAAgZQAAgTgLgKQgMgKgYAAIgNAAgAAMhbIgWBQIANAAQAfAAARgOQAPgOAAgUQAAgQgKgJQgJgJgVAAIgOACg");
	this.shape_106.setTransform(834.25,99.925);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#00FF00").s().p("AhbBnIACgGQAOAAAFgFQAFgEAGgTIAmiGIAEgQQAAgEgCgCQgCgDgFAAIgNABIAAgGIAzgHIgKAjQARgUANgIQANgHAOAAQAOAAAJALQAKAKAAASQAAAhgbAjQgcAiglAAQgGAAgHgBQgFgCgHgEIgMAnQgDAMgBAEQAAADACACQACADAEABIAQABIgCAGgAAOhGQgQATgHAaIgNAtQAJALAQAAQAHAAAJgEQAIgFAIgJQAIgIAGgLQAHgLAEgQQAGgPAAgQQgBgMgFgGQgGgHgHAAQgRAAgQATg");
	this.shape_107.setTransform(807.4,106.775);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("#00FF00").s().p("AgzA5QgLgOAAgRQAAgXAOgXQAOgYAWgNQAUgNAXAAQAQAAAIAGQAIAHAAAJQAAANgKALQgOAQgaAIQgQAGgfAEIgBANQAAANAJAKQAKAJAOAAQAKAAAKgEQAKgFAVgPIADAEQglAkggAAQgWAAgMgOgAgHguQgSARgIAfQAXgCANgGQATgIAJgMQAKgMAAgKQAAgHgEgEQgEgEgIAAQgQAAgQARg");
	this.shape_108.setTransform(795.025,103.575);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#00FF00").s().p("AgzA5QgLgOAAgRQAAgXAOgXQAOgYAWgNQAUgNAXAAQAQAAAIAGQAIAHAAAJQAAANgKALQgOAQgaAIQgQAGgfAEIgBANQAAANAJAKQAKAJAOAAQAKAAAKgEQAKgFAVgPIADAEQglAkggAAQgWAAgMgOgAgHguQgSARgIAfQAXgCANgGQATgIAJgMQAKgMAAgKQAAgHgEgEQgEgEgIAAQgQAAgQARg");
	this.shape_109.setTransform(781.025,103.575);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f("#00FF00").s().p("AhzBoIACgFQANgBAEgCQAGgCADgFQAEgGAGgUIAlh9QAEgPAAgIQAAgGgEgDQgEgEgMAAIgDAAIACgFIBBAAQArAAAUAJQAUAKAMAVQANAVAAAYQAAATgHATQgHAUgJALQgJAMgSAOQgTAOgVAHQgVAGgfAAgAgFhcIgtCbQgFASAAAEIACAFQABACADABQAEABAJAAQAYAAAUgFQAVgFANgKQATgOALgZQALgZAAgfQAAgkgRgSQgRgRghAAIgVAAg");
	this.shape_110.setTransform(761.625,99.925);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("#00FF00").s().p("AgRBFQgHgIgBgKQABgLAHgIQAIgIAJABQALgBAHAIQAIAIAAALQAAAKgIAIQgHAHgLAAQgJAAgIgHgAgRggQgHgHgBgLQABgKAHgIQAIgHAJAAQALAAAHAHQAIAIAAAKQAAALgIAHQgHAIgLAAQgJAAgIgIg");
	this.shape_111.setTransform(858.1,67.45);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f("#00FF00").s().p("AgqA2QgRgVAAgfQAAgcAPgWQATgbAhAAQAWAAANALQANALAAAOQAAAJgFAFQgFAFgJAAQgJAAgGgGQgGgGgBgPQgBgKgEgEQgDgEgFAAQgHAAgFAIQgIAMAAAZQAAAUAHARQAGATAKAJQAJAHALAAQAIAAAHgEQAGgDAKgKIAEADQgKATgPAKQgPAJgRAAQgcAAgRgWg");
	this.shape_112.setTransform(846.125,67.475);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f("#00FF00").s().p("AgmBrIAAgGQAKAAAEgFQADgEAAgPIAAhTQAAgPgDgEQgEgFgKAAIAAgGIA7AAIAABxQAAAPAEAEQAEAFAKAAIAAAGgAgQhBQgHgHAAgKQAAgKAHgHQAHgHAJAAQAKAAAHAHQAHAHAAAKQAAAKgHAHQgHAHgKAAQgJAAgHgHg");
	this.shape_113.setTransform(834.675,63.975);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f("#00FF00").s().p("AgYBGIgGgCQgGAAgDAIIgFAAIgCg0IAEAAQAIAUAMAKQALALALAAQAHAAAEgFQAGgFAAgGQAAgHgGgGQgEgFgPgLQgYgQgGgIQgKgNAAgPQAAgQALgOQAMgNAVAAQAKAAALAFQAFADADAAIAEgBIAFgHIAGAAIACAxIgGAAQgJgWgKgIQgKgIgJAAQgGAAgEAFQgFAEAAAFQAAAEADAEQAFAGAWAQQAWAPAIAKQAGAKABAOQgBAMgFALQgGAMgMAGQgLAGgNAAQgJAAgRgGg");
	this.shape_114.setTransform(824.1,67.475);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f("#00FF00").s().p("AgtBDQgKgIgDgKQgDgKAAgZIAAg4QAAgQgDgEQgDgEgKgBIAAgGIA7AAIAABhQAAAQACAEQABAFADACQADACAEAAQAFAAAEgDQAHgEAIgMIAAhMQAAgQgDgEQgDgEgJgBIAAgGIA6AAIAABwQAAAQADAEQADAEAKABIAAAGIg7AAIAAgTQgKAMgJAGQgLAFgNAAQgMAAgJgHg");
	this.shape_115.setTransform(809.125,67.675);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f("#00FF00").s().p("AAiBoIAAgFIAGAAQAKAAAFgEQAEgCADgFQABgEAAgPIAAiaIhPC9IgDAAIhSi8IAACSQAAAQABADQACAIAHAFQAGAFAPAAIAAAFIhIAAIAAgFIACAAQAHAAAHgDQAFgCAEgEQADgEABgHIAAgQIAAiDQAAgQgBgEQgCgEgFgCQgGgEgIAAIgHAAIAAgFIBVAAIA6CIIA4iIIBWAAIAAAFIgHAAQgJAAgGAEQgEACgCAFQgCAEAAAPIAACJQAAAQACAEQABAEAGADQAFADAJAAIAHAAIAAAFg");
	this.shape_116.setTransform(785.55,64.225);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f("#00FF00").s().p("AgaCRQgQgMgEgPQgDgJAAgkIAAh5IggAAIAAgJQAhgVAXgYQAYgYAQgdIAJAAIAABTIA6AAIAAAYIg6AAIAACKQAAAVACAFQACAGAFAEQAFACAEAAQASAAAOgXIAIAFQgVAugwAAQgXABgQgMg");
	this.shape_117.setTransform(298.85,171.5);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f("#00FF00").s().p("AgZCRQgRgMgFgPQgCgJAAgkIAAh5IghAAIAAgJQAigVAYgYQAWgYARgdIAJAAIAABTIA7AAIAAAYIg7AAIAACKQAAAVACAFQACAGAFAEQAFACAEAAQASAAAPgXIAIAFQgWAugwAAQgWABgQgMg");
	this.shape_118.setTransform(280.8,171.5);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f("#00FF00").s().p("AhQBSQgWgggBguQAAg4AighQAhgjAtAAQAlABAbAcQAcAcACA4IiHAAQADAsAYAaQARAVAZAAQAQgBANgHQANgJAPgVIAJAFQgUAmgYAQQgZAQggAAQg2gBgcgmgAgRhbQgQAWAAAlIAAAJIBHAAQAAgogEgOQgFgPgKgHQgFgEgKAAQgNAAgIAMg");
	this.shape_119.setTransform(259.95,175.5);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f("#00FF00").s().p("AgFB1IhTivQgPgggJgJQgGgGgMgCIAAgJICDAAIAAAJQgLAAgFAEQgHAGAAAGQAAAKANAYIAnBUIAhhIQANggAAgNQABgHgGgFQgGgEgQgBIAAgJIBRAAIAAAJQgLABgIAHQgHAGgOAhIhTCxg");
	this.shape_120.setTransform(233.95,175.85);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f("#00FF00").s().p("AhVBVQgegkAAgwQAAgvAfglQAeglA2AAQAfAAAcAQQAdAPAOAdQAOAcAAAgQAAAxgaAgQghAog5AAQg3AAgegkgAgWhdQgJAKgDAbQgDAcAAAwQAAAaADAWQADARAJAJQALAKALgBQALABAJgHQAKgIADgOQAFgYAAhFQAAgpgEgQQgFgPgKgHQgIgFgKAAQgNAAgKAJg");
	this.shape_121.setTransform(206.85,175.5);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f("#00FF00").s().p("AilClIAAgJIAMAAQAPAAAKgFQAHgEAEgIQADgGAAgZIAAjXQAAgZgDgHQgDgGgJgFQgJgFgPAAIgMAAIAAgJIC/AAIAAAJIgQAAQgPAAgKAGQgHADgDAIQgDAGAAAZIAADRQAAAZADAHQADAHAJADQAHACAZAAIAdAAQAdAAATgJQATgJAOgUQAOgUAMgnIALAAIgNB0g");
	this.shape_122.setTransform(174.8,170.375);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f("#00FF00").s().p("AAIB1IAAgJQAOgBAHgJQAEgHAAgXIAAhlQgBgcgBgHQgDgHgFgEQgHgEgHAAQgVAAgTAeIAAB5QAAAZAEAHQAGAGANACIAAAJIh8AAIAAgJQAPgBAGgIQAFgGAAgZIAAiAQAAgZgFgHQgFgGgQgCIAAgJIBlAAIAAAdQASgSARgJQARgJAUAAQAZAAAPAMQAQAMAGATQADANAAAoIAABYQABAZAFAHQAFAGAQACIAAAJg");
	this.shape_123.setTransform(319.85,118.85);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f("#00FF00").s().p("AhVBUQgegjAAgwQAAgvAfglQAeglA2ABQAfAAAdAPQAcAPAOAcQAOAdAAAgQAAAxgaAgQghAng5AAQg3AAgegkgAgWhcQgJAIgDAdQgEAbABAwQAAAaADAXQADARAJAIQAKAJAMAAQAMAAAHgFQALgJADgPQAGgWgBhGQAAgpgEgPQgGgQgKgHQgHgFgLAAQgMAAgKAKg");
	this.shape_124.setTransform(291.1,119.2);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f("#00FF00").s().p("AAdCEQgSAVgNAIQgPAHgTAAQgwAAgbgnQgWgfAAguQAAglAOgcQANgdAYgPQAZgPAbAAQASAAAMAHQAOAGAPAQIAAhFQAAgagCgFQgDgHgGgDQgHgEgQAAIAAgKIBtAAIAAEEQAAAbACAFQABAIAHADQAFAEAPABIAAAIIhpATgAgigrQgJAIgGATQgGASAAAnQgBArAIAUQAGAVAMAJQAGAFALAAQAWAAAUgiIAAh2QgTgjgbAAQgKAAgHAFg");
	this.shape_125.setTransform(262.95,114.425);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.f("#00FF00").s().p("AAIB1IAAgJQAOgBAHgJQADgHABgXIAAhlQgBgcgBgHQgDgHgFgEQgHgEgHAAQgVAAgTAeIAAB5QAAAZAEAHQAGAGANACIAAAJIh8AAIAAgJQAPgBAGgIQAFgGAAgZIAAiAQAAgZgFgHQgFgGgQgCIAAgJIBlAAIAAAdQASgSARgJQASgJATAAQAZAAAPAMQAQAMAGATQADANAAAoIAABYQABAZAFAHQAFAGAQACIAAAJg");
	this.shape_126.setTransform(232.55,118.85);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.f("#00FF00").s().p("AAaBuQgMgJgCgTQgtAlgkAAQgVAAgOgNQgOgNAAgTQAAgaAYgUQAZgVBRgiIAAgXQAAgagDgGQgEgHgHgFQgIgFgLAAQgRAAgLAHQgHAEAAAHQAAAFAHAHQAKAKAAAKQAAAMgJAIQgKAJgQAAQgQAAgMgKQgLgIAAgNQAAgSAPgQQAPgPAbgJQAbgJAdABQAjAAAUANQAVAOAGAQQADAKAAAlIAABXQAAAQACAEQABADADACQACACAEAAQAHAAAHgJIAIAGQgOASgOAJQgOAIgSAAQgVAAgMgJgAgjAZQgKANAAANQAAAMAIAJQAHAGAMAAQANAAARgNIAAhMQgfARgQATg");
	this.shape_127.setTransform(204.575,119);

	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.f("#00FF00").s().p("AhpB1IAAgJQAQgBAGgHQAFgHAAgeIAAh8QAAgTgCgGQgDgGgFgEQgFgDgMgBIAAgJIBlAAIAAA0QAXgkATgLQASgMARAAQAPAAAJAIQAJAJAAAPQAAARgIAIQgJAJgMAAQgOABgLgJIgMgJQgCgCgEAAQgIAAgGAGQgLAIgGAQQgHAXAAAbIAAA0IAAAOQAAAOACAEQACAGAGAEQAGACAPABIAAAJg");
	this.shape_128.setTransform(178.825,118.85);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.f("#00FF00").s().p("AikClIAAgJQAYAAAJgEQAJgFAEgHQADgGAAgbIAAjVQAAgagDgHQgEgHgJgEQgJgFgYAAIAAgJICrAAQA9AAAaALQAZAKAPAUQAPAUAAAWQAAAYgTATQgTASgvALQAzAKAUAQQAeAWABAjQAAAlghAYQgoAehNAAgAgfADIAABpIAAAMQgBANAIAHQAHAHAPAAQATAAASgJQASgIAJgRQAKgQAAgUQAAgXgLgTQgMgSgVgHQgTgHgkAAIgEAAgAgfgPQAkAAATgIQARgIAKgOQAJgOAAgWQAAgWgJgOQgJgOgSgHQgSgHglAAg");
	this.shape_129.setTransform(147.75,114.075);

	this.shape_130 = new cjs.Shape();
	this.shape_130.graphics.f("#00FF00").s().p("AgeBsQgNgMAAgQQAAgRANgMQAMgMASAAQASAAANAMQANAMAAARQAAAQgNAMQgNAMgSAAQgSAAgMgMgAgegyQgNgMAAgRQAAgQANgMQAMgMASAAQASAAANAMQANAMAAAQQAAARgNAMQgNAMgSAAQgSAAgMgMg");
	this.shape_130.setTransform(394.2,62.825);

	this.shape_131 = new cjs.Shape();
	this.shape_131.graphics.f("#00FF00").s().p("AhpCcQgOgMAAgQQAAgOAJgKQAKgJANAAQAOAAAIAIQAIAIAAAQQAAAJACADQADADADAAQAHAAAHgHQALgLAOgkIAIgTIhQinQgSgmgJgJQgJgJgMgDIAAgJICDAAIAAAJQgMAAgGAEQgFAEAAAGQAAAJANAcIAqBZIAdhFQAPglAAgOQAAgJgGgFQgHgGgQAAIAAgJIBTAAIAAAJQgNABgHAHQgIAIgSArIhHCnQgZA/gNAPQgSAUgcAAQgWAAgNgLg");
	this.shape_131.setTransform(371.575,68.225);

	this.shape_132 = new cjs.Shape();
	this.shape_132.graphics.f("#00FF00").s().p("AikClIAAgJQAYAAAJgEQAJgFADgHQAEgGAAgbIAAjVQAAgagEgHQgDgHgJgEQgJgFgYAAIAAgJICqAAQA+AAAaALQAaAKAOAUQAPAUAAAWQAAAYgTATQgSASgwALQAzAKAUAQQAfAWAAAjQgBAlggAYQgoAehNAAgAgfADIAABpIAAAMQAAANAHAHQAHAHAOAAQAVAAARgJQASgIAJgRQAKgQAAgUQAAgXgMgTQgLgSgUgHQgUgHgkAAIgEAAgAgfgPQAlAAASgIQARgIAJgOQAKgOAAgWQAAgWgJgOQgKgOgRgHQgSgHglAAg");
	this.shape_132.setTransform(339.65,57.725);

	this.shape_133 = new cjs.Shape();
	this.shape_133.graphics.f("#00FF00").s().p("AAJB1IAAgJQANgCAHgIQADgHAAgYIAAhkQAAgcgCgHQgCgHgGgEQgFgEgHAAQgWAAgUAeIAAB4QAAAaAGAHQAFAGAOACIAAAJIh9AAIAAgJQAPgBAGgIQAFgGAAgaIAAh/QAAgZgFgHQgFgGgQgCIAAgJIBkAAIAAAeQATgTARgJQASgJATAAQAYAAARAMQAPANAGASQADANAAAoIAABXQAAAaAGAHQAFAGAQACIAAAJg");
	this.shape_133.setTransform(293.5,62.5);

	this.shape_134 = new cjs.Shape();
	this.shape_134.graphics.f("#00FF00").s().p("AhVBUQgegjAAgwQAAgvAfglQAeglA2AAQAfABAdAPQAcAQAOAcQAOAcAAAgQAAAxgbAgQggAng5AAQg3ABgeglgAgVhdQgLAJgCAdQgDAbAAAwQAAAaADAWQADASAKAIQAJAJAMAAQAMAAAHgFQALgJADgPQAGgXgBhFQAAgpgEgQQgFgPgLgHQgGgFgMAAQgMAAgJAJg");
	this.shape_134.setTransform(264.75,62.85);

	this.shape_135 = new cjs.Shape();
	this.shape_135.graphics.f("#00FF00").s().p("AhCCoIAAgIQARAAAIgJQAFgFAAgZIAAiDQAAgYgGgHQgGgHgSgBIAAgJIBnAAIAACzQAAAZAGAGQAGAHASABIAAAIgAgchoQgMgLAAgPQAAgPAMgMQAMgLAQAAQARAAAMALQAMAMAAAPQAAAPgMALQgMALgRAAQgQAAgMgLg");
	this.shape_135.setTransform(243.925,57.35);

	this.shape_136 = new cjs.Shape();
	this.shape_136.graphics.f("#00FF00").s().p("AgZCRQgRgNgFgOQgCgJAAgkIAAh5IghAAIAAgJQAigVAYgYQAWgYARgdIAJAAIAABSIA7AAIAAAZIg7AAIAACKQAAAUACAGQACAFAFAEQAFAEAEAAQASAAAPgZIAIAGQgWAvgwAAQgWAAgQgMg");
	this.shape_136.setTransform(227.35,58.85);

	this.shape_137 = new cjs.Shape();
	this.shape_137.graphics.f("#00FF00").s().p("AAaBuQgMgJgCgTQgtAlgkAAQgVAAgOgNQgOgNAAgTQAAgaAYgUQAZgVBRgiIAAgXQAAgZgDgHQgEgHgHgFQgIgFgLAAQgRAAgLAHQgHAEAAAGQAAAGAHAHQAKAKAAAKQAAAMgJAIQgKAJgQAAQgQAAgMgKQgLgIAAgNQAAgSAPgQQAPgPAbgJQAbgJAdAAQAjAAAUAPQAVANAGAQQADALAAAkIAABXQAAAQACAEQABADADACQACACAEAAQAHAAAHgJIAIAGQgOASgOAJQgOAHgSABQgVgBgMgIgAgjAZQgKANAAANQAAAMAIAJQAHAGAMAAQANAAARgNIAAhMQgfARgQATg");
	this.shape_137.setTransform(205.325,62.65);

	this.shape_138 = new cjs.Shape();
	this.shape_138.graphics.f("#00FF00").s().p("ABRB1IAAgJQAPgBAHgJQAFgHAAgYIAAhgQAAgfgDgIQgDgIgFgEQgGgEgIAAQgLAAgMAHQgLAJgMAPIAAB4QAAAZAEAHQAGAJASAAIAAAJIh/AAIAAgJQAKAAAGgFQAGgDACgHQACgFAAgVIAAhgQAAgfgDgIQgCgHgHgFQgGgFgHAAQgLAAgIAGQgNAIgOASIAAB4QAAAYAFAIQAFAIARABIAAAJIiAAAIAAgJQAQgBAHgIQAEgGAAgaIAAh/QAAgZgFgHQgFgGgRgCIAAgJIBlAAIAAAeQAVgVARgHQARgJAVAAQAYAAAPAKQAQALAJAVQAVgXATgJQAUgKAVAAQAaAAAPALQAPALAGAQQAGARAAAkIAABcQAAAaAFAHQAFAGARACIAAAJg");
	this.shape_138.setTransform(168.725,62.5);

	this.shape_139 = new cjs.Shape();
	this.shape_139.graphics.f("#00FF00").s().p("AhCCoIAAgIQARAAAIgJQAFgFAAgZIAAiDQAAgYgGgHQgGgHgSgBIAAgJIBnAAIAACzQAAAZAGAGQAGAHASABIAAAIgAgchoQgMgLAAgPQAAgPAMgMQAMgLAQAAQARAAAMALQAMAMAAAPQAAAPgMALQgMALgRAAQgQAAgMgLg");
	this.shape_139.setTransform(138.625,57.35);

	this.shape_140 = new cjs.Shape();
	this.shape_140.graphics.f("#00FF00").s().p("AAJB1IAAgJQAOgCAFgIQAEgHAAgYIAAhkQAAgcgCgHQgCgHgGgEQgGgEgGAAQgWAAgUAeIAAB4QAAAaAGAHQAEAGAPACIAAAJIh+AAIAAgJQAQgBAGgIQAFgGAAgaIAAh/QAAgZgFgHQgFgGgRgCIAAgJIBlAAIAAAeQATgTARgJQASgJATAAQAZAAAQAMQAPANAFASQAFANAAAoIAABXQgBAaAGAHQAFAGARACIAAAJg");
	this.shape_140.setTransform(115.95,62.5);

	this.shape_141 = new cjs.Shape();
	this.shape_141.graphics.f("#00FF00").s().p("AARCoIAAgJIAIAAQAUAAAJgGQAFgDABgIIgCgJIgIgSIgTgoIh+AAIgPAfQgHAQAAALQAAANAMAHQAHAEAcACIAAAJIh3AAIAAgJQATgDAMgMQANgMASglIB/kGIAGAAICBENQATAmALAKQAKAIAPABIAAAJgAhWA5IBsAAIg0hxg");
	this.shape_141.setTransform(81.05,57.4);

	this.shape_142 = new cjs.Shape();
	this.shape_142.graphics.f("#00FF00").s().p("AArDUQgbAigUAMQgWAMgcAAQhFAAgpg+QghgzAAhKQAAg7AUgtQAUgvAkgYQAjgYApAAQAaAAATAKQAUAKAXAaIAAhuQAAgqgDgJQgFgLgJgGQgJgFgZAAIAAgQIChAAIAAGjQAAAqADAIQACANAKAGQAIAHAWABIAAANIibAegAgyhFQgOAMgKAeQgJAeABA/QgBBFAKAhQAKAhASAPQAJAHAQAAQAhAAAeg2IAAi/Qgcg3gpAAQgOAAgKAIg");
	this.shape_142.setTransform(571.3,153.675);

	this.shape_143 = new cjs.Shape();
	this.shape_143.graphics.f("#00FF00").s().p("AAMC8IAAgOQAVgDAJgOQAGgKAAgnIAAihQAAgtgEgMQgDgMgIgGQgJgGgKgBQggABgeAvIAADDQAAApAJALQAHALAUADIAAAOIi4AAIAAgOQAXgCAJgNQAIgIAAgrIAAjNQgBgqgHgJQgIgKgYgEIAAgOICUAAIAAAvQAdgfAZgNQAZgOAeAAQAjAAAYAUQAYATAHAeQAHAWAAA/IAACNQgBAqAJALQAHAKAYADIAAAOg");
	this.shape_143.setTransform(526.4,160.8);

	this.shape_144 = new cjs.Shape();
	this.shape_144.graphics.f("#00FF00").s().p("AjwEJIAAgPIASAAQAXAAAOgIQAKgFAGgOQAEgJAAgoIAAlbQAAgkgCgIQgEgOgLgIQgOgKgaAAIgSAAIAAgPIG7AAIAACdIgPAAQgMg6gUgZQgVgZglgLQgWgHg8AAIg2AAIAADXIALAAQAxAAAXgfQAWggAHg+IAPAAIAAEVIgPAAQgFgugPgdQgPgdgUgJQgTgLgmAAIAACWQAAArAEAKQAEAKAKAGQAKAGAWAAIAfAAQBKAAAtgiQAtgiAThHIAPAAIgYCpg");
	this.shape_144.setTransform(476.275,153.125);

	this.shape_145 = new cjs.Shape();
	this.shape_145.graphics.f("#00FF00").s().p("Ah2CDQgigzAAhKQAAhbAyg2QAxg2BBAAQA4AAApAuQAoAtAEBaIjIAAQAEBIAjAqQAaAgAmAAQAXAAATgNQATgNAWgiIAOAJQgeA8gkAZQgkAZgvAAQhQAAgqg+gAgaiUQgXAkAAA8IAAANIBpAAQAAg+gGgXQgHgYgPgMQgIgHgOAAQgTAAgNATg");
	this.shape_145.setTransform(571.225,70.75);

	this.shape_146 = new cjs.Shape();
	this.shape_146.graphics.f("#00FF00").s().p("AAMEJIAAgOQAUgDAJgNQAHgKAAgoIAAiiQAAgtgDgLQgEgMgIgGQgJgHgKAAQgPAAgPALQgOALgSAbIAADCQAAAoAGAJQAIAOAXADIAAAOIi5AAIAAgOQAXgDAJgMQAHgJAAgqIAAlyQAAgpgHgKQgIgKgYgDIAAgPICUAAIAADTQAfgfAYgNQAZgNAaAAQAjAAAYATQAZAUAIAaQAIAaAAA+IAACOQAAAqAHALQAIAKAYADIAAAOg");
	this.shape_146.setTransform(530.875,62.525);

	this.shape_147 = new cjs.Shape();
	this.shape_147.graphics.f("#00FF00").s().p("AiKEJIAAgPIASAAQAXAAAOgIQAKgFAGgOQAEgJAAgoIAAmXIgmAAQg2AAgYAWQgiAggJA6IgPAAIAAiPIHbAAIAACPIgOAAQgNgxgPgWQgPgWgbgMQgOgHgmAAIgnAAIAAGXQAAApAFAKQAEAKANAIQANAHAXAAIARAAIAAAPg");
	this.shape_147.setTransform(481.725,62.525);

	this.title_btn2 = new lib.Title_btn();
	this.title_btn2.name = "title_btn2";
	this.title_btn2.setTransform(511.8,248.6,0.5051,0.5051);
	new cjs.ButtonHelper(this.title_btn2, 0, 1, 1);

	this.shape_148 = new cjs.Shape();
	this.shape_148.graphics.f("#00FF00").s().p("AAvBKIAAgGQAJgBAEgFQADgFAAgOIAAg9QAAgUgCgFQgBgFgDgDQgEgCgEAAQgHAAgHAFQgGAFgHAJIAABNQAAAPACAEQAEAGAKAAIAAAGIhKAAIAAgGQAGAAAEgDQADgCACgEQABgEAAgMIAAg9QAAgUgCgFQgBgEgEgEQgEgCgEAAQgGAAgFADQgHAFgIALIAABNQAAAPADAFQADAFAJAAIAAAGIhKAAIAAgGQAJgBAEgEQACgEAAgQIAAhQQAAgRgCgEQgEgEgJgBIAAgGIA7AAIAAATQALgNALgFQAKgFAMAAQANAAAJAGQAJAHAFANQAMgOAMgGQALgGAMAAQAPAAAJAHQAJAGAEALQADALAAAWIAAA7QAAAQADAEQADAEAJABIAAAGg");
	this.shape_148.setTransform(966.175,178.05);

	this.shape_149 = new cjs.Shape();
	this.shape_149.graphics.f("#00FF00").s().p("AgRASQgIgHABgLQgBgKAIgHQAHgIAKAAQAKAAAIAIQAHAHAAAKQAAALgHAHQgIAIgKAAQgKAAgHgIg");
	this.shape_149.setTransform(919.25,183.275);

	this.shape_150 = new cjs.Shape();
	this.shape_150.graphics.f("#00FF00").s().p("AARBTQgLAOgHAEQgJAFgKABQgcAAgQgZQgNgVAAgdQAAgWAIgSQAIgTAOgIQAOgKAQAAQAKAAAHAEQAJAEAIAKIAAgrQAAgRgBgDQgCgEgEgDQgEgCgIAAIAAgGIA/AAIAACkQAAARABADQABAFADACQAEADAIABIAAAEIg9ANgAgTgbQgFAFgEAMQgEALAAAZQAAAbAEANQAEANAHAGQADADAHAAQANAAALgWIAAhKQgLgWgPAAQgGAAgEADg");
	this.shape_150.setTransform(906.8,175.25);

	this.shape_151 = new cjs.Shape();
	this.shape_151.graphics.f("#00FF00").s().p("AAEBKIAAgGQAJgBAEgFQACgEAAgPIAAhAQAAgRgBgFQgCgEgDgDQgDgCgEAAQgNAAgLASIAABNQAAAQADAEQADAFAIAAIAAAGIhJAAIAAgGQAKgBADgEQADgEAAgQIAAhQQAAgRgDgEQgDgEgKgBIAAgGIA7AAIAAATQALgMAJgGQAKgFAMAAQAOAAAJAIQAJAIAEALQACAIAAAZIAAA4QAAAQADAFQADADAKABIAAAGg");
	this.shape_151.setTransform(889.15,178.05);

	this.shape_152 = new cjs.Shape();
	this.shape_152.graphics.f("#00FF00").s().p("AgYBGIgHgCQgFAAgDAIIgFAAIgCg0IAEAAQAIAUAMAKQAMALAKAAQAHAAAEgFQAGgFAAgGQAAgHgGgGQgEgFgPgLQgXgQgHgIQgLgNAAgPQAAgQAMgOQALgNAWAAQALAAALAFQAEADADAAIAEgBIAGgHIAEAAIADAxIgGAAQgJgWgKgIQgKgIgJAAQgGAAgFAFQgEAEAAAFQAAAEADAEQAFAGAVAQQAXAPAIAKQAGAKAAAOQAAAMgFALQgHAMgKAGQgMAGgNAAQgJAAgRgGg");
	this.shape_152.setTransform(841,178.275);

	this.shape_153 = new cjs.Shape();
	this.shape_153.graphics.f("#00FF00").s().p("AAEBKIAAgGQAJgBAEgFQACgEAAgPIAAhAQAAgRgBgFQgCgEgDgDQgDgCgEAAQgNAAgLASIAABNQAAAQADAEQADAFAIAAIAAAGIhJAAIAAgGQAKgBADgEQADgEAAgQIAAhQQAAgRgDgEQgDgEgKgBIAAgGIA7AAIAAATQALgMAJgGQAKgFAMAAQAOAAAJAIQAJAIAEALQACAIAAAZIAAA4QAAAQADAFQADADAKABIAAAGg");
	this.shape_153.setTransform(826.15,178.05);

	this.shape_154 = new cjs.Shape();
	this.shape_154.graphics.f("#00FF00").s().p("AgNBmQgIgDgKgIIgXAPIgGAAIAAi2QAAgMgBgDQgBgEgDgCQgDgCgJgBIAAgFIA9AAIAABPQAQgTAWAAQAOAAAOAJQANAIAHAQQAIAQAAAUQAAAXgKAUQgJATgQAKQgQALgTAAQgLgBgKgEgAgQgNIAAA/IAAAZQABAJAHAGQAFAGAJgBQAHABAGgFQAGgEADgNQAEgOAAgiQAAghgIgLQgGgJgKAAQgMAAgMAOg");
	this.shape_154.setTransform(794.2,175.25);

	this.shape_155 = new cjs.Shape();
	this.shape_155.graphics.f("#00FF00").s().p("AgRASQgIgHAAgLQAAgKAIgHQAHgIAKAAQALAAAHAIQAHAHAAAKQAAALgHAHQgHAIgLAAQgKAAgHgIg");
	this.shape_155.setTransform(781.9,183.275);

	this.shape_156 = new cjs.Shape();
	this.shape_156.graphics.f("#00FF00").s().p("AgrBtIBDjZIAUAAIhDDZg");
	this.shape_156.setTransform(707.2,175.025);

	this.shape_157 = new cjs.Shape();
	this.shape_157.graphics.f("#00FF00").s().p("AgRBFQgIgIAAgKQAAgLAIgIQAHgHAKAAQALAAAHAHQAHAIAAALQAAAKgHAIQgHAHgLAAQgKAAgHgHgAgRggQgIgHAAgLQAAgKAIgIQAHgHAKAAQALAAAHAHQAHAIAAAKQAAALgHAHQgHAIgLAAQgKAAgHgIg");
	this.shape_157.setTransform(688.75,178.25);

	this.shape_158 = new cjs.Shape();
	this.shape_158.graphics.f("#00FF00").s().p("AgYBGIgHgCQgEAAgEAIIgFAAIgDg0IAGAAQAHAUAMAKQAMALAKAAQAHAAAEgFQAFgFABgGQgBgHgFgGQgEgFgPgLQgYgQgHgIQgJgNgBgPQAAgQAMgOQAMgNAVAAQALAAALAFQAEADACAAIAFgBIAGgHIAEAAIADAxIgFAAQgKgWgKgIQgKgIgJAAQgHAAgEAFQgEAEAAAFQAAAEADAEQAFAGAVAQQAXAPAHAKQAIAKgBAOQAAAMgFALQgHAMgKAGQgLAGgNAAQgKAAgRgGg");
	this.shape_158.setTransform(677.45,178.275);

	this.shape_159 = new cjs.Shape();
	this.shape_159.graphics.f("#00FF00").s().p("AgPBcQgJgIgDgJQgCgGABgXIAAhMIgTAAIAAgGQATgNAOgPQANgQAJgSIAGAAIAAA0IAiAAIAAAQIgiAAIAABXQAAANABAEQABADADACQADADADAAQAJAAAJgQIAFAEQgNAdgbAAQgNAAgKgHg");
	this.shape_159.setTransform(638.15,175.725);

	this.shape_160 = new cjs.Shape();
	this.shape_160.graphics.f("#00FF00").s().p("AAEBoIAAgFQAJgBADgFQADgEAAgQIAAhAQAAgRgBgEQgCgFgDgCQgDgDgEAAQgGAAgGAEQgFAEgHALIAABMQAAAPACAEQADAFAJACIAAAFIhJAAIAAgFQAKgBADgFQADgEAAgQIAAiRQAAgQgDgEQgDgEgKgCIAAgFIA7AAIAABTQALgNAJgFQAKgFALAAQANAAAKAIQAJAIAEAKQADAKAAAYIAAA4QAAAQADAFQADAEAKABIAAAFg");
	this.shape_160.setTransform(624.1,175.025);

	this.shape_161 = new cjs.Shape();
	this.shape_161.graphics.f("#00FF00").s().p("AAvBKIAAgGQAJgBAEgFQADgEAAgQIAAg8QAAgTgCgFQgBgGgDgDQgEgCgEAAQgHAAgHAFQgGAFgHAKIAABLQAAAQACAEQAEAGAKAAIAAAGIhKAAIAAgGQAGAAAEgDQADgCACgEQABgEAAgNIAAg8QAAgTgCgFQgBgFgEgEQgEgCgEAAQgGAAgFADQgHAFgIAMIAABLQAAAQADAFQADAFAJAAIAAAGIhKAAIAAgGQAJgBAEgEQACgEAAgRIAAhQQAAgQgCgEQgEgEgJgBIAAgGIA7AAIAAATQALgNALgFQAKgFAMAAQANAAAJAGQAJAHAFANQAMgOAMgGQALgGAMAAQAPAAAJAHQAJAGAEALQADAKAAAYIAAA5QAAARADAEQADAEAJABIAAAGg");
	this.shape_161.setTransform(821.525,142.4);

	this.shape_162 = new cjs.Shape();
	this.shape_162.graphics.f("#00FF00").s().p("Ag9BKIAAgGQAKgBADgDQADgFAAgTIAAhPQAAgMgBgDQgBgEgDgCQgDgCgIgBIAAgGIA7AAIAAAhQANgXALgHQALgHAKAAQAIAAAGAFQAFAGAAAJQAAALgFAFQgFAGgHAAQgIAAgGgFIgIgGIgDgBQgFAAgEADQgGAGgDAJQgEAPAAARIAAAhIAAAJQAAAIABADQABAEAEACQADACAJAAIAAAGg");
	this.shape_162.setTransform(786.425,142.4);

	this.shape_163 = new cjs.Shape();
	this.shape_163.graphics.f("#00FF00").s().p("Ag6BrIAAgGQAKAAAEgCQAEgCACgEQABgDAAgMIAAhjIgVAAIAAgPIAVAAIAAgLIAAgHQAAgWAQgPQAQgPAbAAQATAAAJAIQAJAHAAAIQAAAHgGAFQgFAFgKAAQgHAAgFgEQgFgEAAgFIABgGIABgFQAAAAgBgBQAAgBAAAAQAAgBAAAAQgBgBAAAAQgCgDgEAAQgFAAgCAEQgDAEAAAIIAAAbIAAASIAVAAIAAAPIgVAAIAABjQAAAOADAEQAEAGAOgBIAAAGg");
	this.shape_163.setTransform(775.5,139.125);

	this.shape_164 = new cjs.Shape();
	this.shape_164.graphics.f("#00FF00").s().p("AAQBEQgDgDAAgGIACgOIAOgwQgaAqgQAQQgTAQgOAAQgIAAgEgEQgGgFAAgIQAAgLAIgYIANgvQAEgRABgEIgBgDIgEgBQgDAAgDACIgPARIgFgEQAMgRANgJQAKgGAIAAQAGAAADADQAEAEAAAGQAAAIgGAVIgPAxQgFAUAAAGQAAAAAAABQAAABAAAAQAAABABAAQAAABABAAQABACADAAQAGAAAIgFQAIgFANgSQAOgRAIgOQAJgQAKghIADgMIAYAAIgbBbQgGAUAAAGIABAEQAAAAABAAQAAABAAAAQAAAAABAAQAAAAABAAQACAAADgCIAQgTIAFADQgMAUgNAJQgIAFgIAAQgGAAgEgDg");
	this.shape_164.setTransform(845.7,107.375);

	this.shape_165 = new cjs.Shape();
	this.shape_165.graphics.f("#00FF00").s().p("AgkBrQgEgEAAgFQAAgFAFgSIApiPQAFgRAAgEQAAgDgDgCQgDgDgFAAIgLACIAAgGIA0gJIg0C2IgEAQQAAABAAABQAAAAABABQAAAAAAABQABAAAAABQABAAAAAAQAAAAABABQAAAAABAAQAAAAABAAQACAAAEgCQAHgHAJgNIAEAFQgOAUgMAIQgJAGgIAAQgGAAgEgEg");
	this.shape_165.setTransform(834.375,103.4);

	this.shape_166 = new cjs.Shape();
	this.shape_166.graphics.f("#00FF00").s().p("AhfBoIACgFQALgBAFgCQAFgCAEgFQADgGAGgUIAmh+QAEgQAAgGQABgGgFgDQgEgDgMAAIgFgBIACgFIBJAAQATAAAPAGQAPAFAHALQAHAKABAMQgBASgMAPQgOAOgdAHQATAGAJAMQAJALAAAOQAAAQgIAOQgIAOgMAIQgOAIgRAEQgNACgZAAgAgOAAIgbBbIAaACQAXAAASgPQATgOAAgZQAAgTgMgKQgLgKgYAAIgMAAgAAMhbIgXBQIAOAAQAfAAARgOQAPgOAAgUQAAgQgJgJQgKgJgVAAIgOACg");
	this.shape_166.setTransform(818.8,103.725);

	this.shape_167 = new cjs.Shape();
	this.shape_167.graphics.f("#00FF00").s().p("AhbBnIACgGQAOAAAFgFQAFgEAFgTIAoiGIADgQQAAgEgCgCQgDgDgEAAIgNABIAAgGIAzgHIgKAjQARgUANgIQANgHAOAAQANAAAKALQAKAKAAASQAAAhgcAjQgbAiglAAQgGAAgHgBQgGgCgHgEIgLAnQgEAMAAAEQAAADADACQABADAEABIAQABIgBAGgAAOhGQgPATgIAaIgNAtQAKALAOAAQAJAAAIgEQAIgFAIgJQAIgIAGgLQAGgLAGgQQAEgPAAgQQAAgMgFgGQgFgHgIAAQgRAAgQATg");
	this.shape_167.setTransform(791.95,110.575);

	this.shape_168 = new cjs.Shape();
	this.shape_168.graphics.f("#00FF00").s().p("AgRBFQgIgIAAgKQAAgLAIgIQAHgHAKAAQALAAAHAHQAHAIAAALQAAAKgHAIQgHAHgLAAQgKAAgHgHgAgRgfQgIgIAAgKQAAgLAIgHQAHgIAKAAQALAAAHAIQAHAHAAALQAAAKgHAIQgHAHgLAAQgKAAgHgHg");
	this.shape_168.setTransform(842.65,71.25);

	this.shape_169 = new cjs.Shape();
	this.shape_169.graphics.f("#00FF00").s().p("AgYBGIgHgCQgEAAgEAIIgFAAIgDg0IAGAAQAGAUANAKQAMALAKAAQAHAAAFgFQAEgFAAgGQAAgHgEgGQgFgFgPgLQgXgQgIgIQgKgNABgPQgBgQAMgOQAMgNAVAAQALAAAKAFQAFADACAAIAFgBIAFgHIAGAAIACAxIgFAAQgKgWgKgIQgKgIgJAAQgHAAgEAFQgEAEAAAFQAAAEADAEQAFAGAWAQQAWAPAHAKQAIAKgBAOQABAMgHALQgFAMgMAGQgKAGgNAAQgKAAgRgGg");
	this.shape_169.setTransform(808.65,71.275);

	this.shape_170 = new cjs.Shape();
	this.shape_170.graphics.f("#00FF00").s().p("AAiBoIAAgFIAHAAQAIAAAGgEQAEgCACgFQACgEAAgPIAAiaIhPC9IgDAAIhSi8IAACSQAAAQABADQACAIAHAFQAGAFAOAAIAAAFIhHAAIAAgFIADAAQAHAAAFgDQAGgCADgEQADgEACgHIABgQIAAiDQAAgQgCgEQgCgEgFgCQgGgEgIAAIgHAAIAAgFIBWAAIA5CIIA5iIIBUAAIAAAFIgGAAQgKAAgFAEQgEACgCAFQgCAEAAAPIAACJQAAAQACAEQACAEAFADQAFADAJAAIAGAAIAAAFg");
	this.shape_170.setTransform(770.1,68.025);

	this.shape_171 = new cjs.Shape();
	this.shape_171.graphics.f("#00FF00").s().p("AgaCRQgQgNgFgOQgCgIAAglIAAh5IghAAIAAgJQAigVAYgYQAWgYARgdIAJAAIAABSIA7AAIAAAZIg7AAIAACKQAAAUACAGQACAFAFAEQAFADAEABQASAAAPgYIAIAFQgWAugwABQgWAAgRgMg");
	this.shape_171.setTransform(283.4,175.3);

	this.shape_172 = new cjs.Shape();
	this.shape_172.graphics.f("#00FF00").s().p("AgZCRQgRgNgEgOQgDgIAAglIAAh5IggAAIAAgJQAhgVAXgYQAYgYAQgdIAJAAIAABSIA7AAIAAAZIg7AAIAACKQAAAUACAGQACAFAFAEQAFADAEABQARAAAPgYIAJAFQgWAugwABQgXAAgPgMg");
	this.shape_172.setTransform(265.35,175.3);

	this.shape_173 = new cjs.Shape();
	this.shape_173.graphics.f("#00FF00").s().p("AhQBSQgWggAAgvQgBg3AigiQAhghAsgBQAmAAAbAdQAcAcADA4IiHAAQACAsAYAaQARAVAZgBQARABANgJQANgIAPgVIAJAFQgVAmgYAQQgZAPggAAQg2ABgcgngAgRhbQgQAWAAAlIAAAIIBHAAQAAgmgEgPQgFgPgKgHQgGgEgIgBQgOAAgIANg");
	this.shape_173.setTransform(244.5,179.3);

	this.shape_174 = new cjs.Shape();
	this.shape_174.graphics.f("#00FF00").s().p("AgEB1IhUivQgPgggIgIQgHgHgLgCIAAgJICCAAIAAAJQgMAAgEAEQgGAGAAAGQgBAJAMAZIAoBVIAghJQAOgfABgNQgBgIgFgFQgGgEgPgBIAAgJIBRAAIAAAJQgMACgHAGQgIAGgPAhIhSCxg");
	this.shape_174.setTransform(218.5,179.65);

	this.shape_175 = new cjs.Shape();
	this.shape_175.graphics.f("#00FF00").s().p("AhVBUQgegkAAgvQAAgvAfglQAeglA2AAQAfAAAdAQQAcAQAOAcQAOAcAAAgQAAAxgbAgQggAng5AAQg3ABgeglgAgVhdQgLAJgCAcQgDAcAAAwQAAAaADAWQADASAKAIQAJAKAMgBQAMAAAHgFQALgJADgPQAGgXgBhFQAAgpgEgQQgFgPgLgHQgGgFgMAAQgMAAgJAJg");
	this.shape_175.setTransform(191.4,179.3);

	this.shape_176 = new cjs.Shape();
	this.shape_176.graphics.f("#00FF00").s().p("AilClIAAgJIAMAAQAQAAAJgFQAHgEAEgIQACgGAAgZIAAjXQAAgZgCgHQgDgGgJgFQgJgFgPAAIgMAAIAAgJIC/AAIAAAJIgRAAQgOAAgKAGQgHADgEAIQgCAGAAAZIAADRQAAAZADAHQADAHAKADQAGACAYAAIAeAAQAdAAATgJQATgJAOgUQAOgUAMgnIALAAIgOB0g");
	this.shape_176.setTransform(159.35,174.175);

	this.shape_177 = new cjs.Shape();
	this.shape_177.graphics.f("#00FF00").s().p("AAJB1IAAgJQANgBAHgKQADgFAAgYIAAhkQAAgdgCgHQgCgHgGgEQgGgEgGAAQgWAAgUAeIAAB5QAAAZAGAHQAFAGAOACIAAAJIh9AAIAAgJQAPgBAGgIQAFgGAAgZIAAh/QAAgagFgHQgFgGgQgCIAAgJIBkAAIAAAdQATgTARgIQASgJATAAQAZAAAQANQAPALAFATQAEAOABAnIAABYQgBAZAGAHQAFAGARACIAAAJg");
	this.shape_177.setTransform(304.4,122.65);

	this.shape_178 = new cjs.Shape();
	this.shape_178.graphics.f("#00FF00").s().p("AhVBVQgeglAAgvQAAgwAfgkQAfgkA1AAQAgAAAcAPQAcAPAOAcQAOAcAAAhQAAAwgbAhQggAog5AAQg3gBgegjgAgVhcQgLAIgCAdQgDAagBAxQAAAaAEAXQADAQAKAKQAJAIAMABQAMgBAHgFQAKgJAEgPQAGgWAAhGQgBgpgEgPQgFgQgLgHQgGgFgMAAQgMAAgJAKg");
	this.shape_178.setTransform(275.65,123);

	this.shape_179 = new cjs.Shape();
	this.shape_179.graphics.f("#00FF00").s().p("AAeCEQgTAVgOAIQgPAHgSAAQgwAAgbgnQgWgfAAguQAAglANgcQAPgdAXgPQAZgPAbAAQASAAANAHQANAGAQAQIAAhFQAAgagCgFQgEgHgGgDQgGgEgRAAIAAgKIBtAAIAAEEQgBAbACAFQADAIAFADQAHAEAOABIAAAIIhoATgAghgrQgKAIgGATQgGASgBAnQAAArAIAUQAGAVAMAJQAGAFALAAQAWAAAVgiIAAh2QgUgjgbAAQgKAAgGAFg");
	this.shape_179.setTransform(247.5,118.225);

	this.shape_180 = new cjs.Shape();
	this.shape_180.graphics.f("#00FF00").s().p("AAJB1IAAgJQANgBAHgKQADgFAAgYIAAhkQAAgdgCgHQgCgHgGgEQgGgEgGAAQgWAAgUAeIAAB5QAAAZAGAHQAFAGAOACIAAAJIh9AAIAAgJQAPgBAGgIQAFgGAAgZIAAh/QAAgagFgHQgFgGgQgCIAAgJIBkAAIAAAdQAUgTAQgIQASgJATAAQAZAAAQANQAPALAFATQAEAOABAnIAABYQgBAZAGAHQAFAGARACIAAAJg");
	this.shape_180.setTransform(217.1,122.65);

	this.shape_181 = new cjs.Shape();
	this.shape_181.graphics.f("#00FF00").s().p("AAaBtQgMgJgCgSQgtAkgkAAQgVAAgOgMQgOgNAAgTQAAgaAYgUQAZgWBRghIAAgXQAAgZgDgHQgEgHgHgFQgIgFgLAAQgRAAgLAHQgHAEAAAHQAAAEAHAIQAKAKAAAKQAAAMgJAJQgKAIgQAAQgQAAgMgJQgLgJAAgNQAAgRAPgRQAPgPAbgJQAbgIAdAAQAjgBAUAOQAVAOAGAQQADALAAAkIAABXQAAAPACAFQABAEADABQACADAEAAQAHgBAHgIIAIAFQgOATgOAHQgOAJgSgBQgVABgMgKgAgjAZQgKAOAAANQAAALAIAJQAHAGAMAAQANAAARgOIAAhKQgfAQgQATg");
	this.shape_181.setTransform(189.125,122.8);

	this.shape_182 = new cjs.Shape();
	this.shape_182.graphics.f("#00FF00").s().p("AhpB1IAAgJQAQgBAGgHQAFgHAAgeIAAh8QAAgUgCgEQgDgIgFgCQgFgEgMgBIAAgJIBlAAIAAA0QAXgkATgLQASgMARAAQAPAAAJAIQAJAJAAAPQAAARgIAJQgJAIgMABQgOAAgLgJIgMgJQgCgBgEAAQgIgBgGAGQgLAIgGAQQgHAXAAAcIAAAzIAAAOQAAAOACAEQACAHAGADQAGACAPABIAAAJg");
	this.shape_182.setTransform(163.375,122.65);

	this.shape_183 = new cjs.Shape();
	this.shape_183.graphics.f("#00FF00").s().p("AilClIAAgJQAZAAAJgEQAJgFADgHQAEgGAAgbIAAjVQAAgagEgHQgDgHgJgEQgJgFgZAAIAAgJICrAAQA+AAAaALQAaAKAOAUQAPAUAAAWQAAAYgTATQgSASgwALQAzAKAVAQQAeAWAAAjQgBAlggAYQgoAehNAAgAgfADIAABpIAAAMQAAANAHAHQAHAHAOAAQAVAAARgJQASgIAJgRQAKgQAAgUQAAgXgMgTQgLgSgUgHQgUgHgkAAIgEAAgAgfgPQAlAAARgIQASgIAJgOQAKgOAAgWQAAgWgJgOQgKgOgRgHQgSgHglAAg");
	this.shape_183.setTransform(132.3,117.875);

	this.shape_184 = new cjs.Shape();
	this.shape_184.graphics.f("#00FF00").s().p("AgeBsQgNgMAAgQQAAgRANgMQAMgMASAAQASAAANAMQANAMAAARQAAAQgNAMQgNAMgSAAQgSAAgMgMgAgegyQgNgMAAgRQAAgQANgMQANgMARAAQASAAANAMQANAMAAAQQAAARgNAMQgNAMgSAAQgRAAgNgMg");
	this.shape_184.setTransform(378.75,66.625);

	this.shape_185 = new cjs.Shape();
	this.shape_185.graphics.f("#00FF00").s().p("AilClIAAgJQAZAAAJgEQAJgFADgHQAEgGAAgbIAAjVQAAgagEgHQgDgHgJgEQgJgFgZAAIAAgJICrAAQA+AAAZALQAbAKAOAUQAPAUAAAWQAAAYgTATQgTASgvALQAzAKAVAQQAdAWAAAjQAAAlggAYQgoAehMAAgAggADIAABpIAAAMQABANAHAHQAHAHAOAAQAUAAATgJQARgIAKgRQAJgQAAgUQAAgXgMgTQgLgSgUgHQgUgHgkAAIgFAAgAgggPQAmAAARgIQASgIAJgOQAKgOAAgWQAAgWgJgOQgKgOgRgHQgRgHgnAAg");
	this.shape_185.setTransform(324.2,61.525);

	this.shape_186 = new cjs.Shape();
	this.shape_186.graphics.f("#00FF00").s().p("AAJB1IAAgJQAOgBAFgKQAEgGABgXIAAhlQgBgbgCgIQgCgHgGgEQgFgEgHAAQgWAAgUAeIAAB5QABAZAFAHQAEAGAOACIAAAJIh9AAIAAgJQAQgBAHgIQAEgGAAgZIAAiAQAAgZgFgHQgFgGgRgCIAAgJIBlAAIAAAdQAUgTAQgIQARgJAVAAQAXAAAQANQAQALAFATQAFANAAAoIAABYQgBAZAGAHQAFAGARACIAAAJg");
	this.shape_186.setTransform(278.05,66.3);

	this.shape_187 = new cjs.Shape();
	this.shape_187.graphics.f("#00FF00").s().p("AhVBUQgegjAAgwQAAgwAfgkQAegkA2AAQAgAAAbAPQAcAPAPAcQAOAcAAAhQAAAwgbAhQggAng5AAQg2AAgfgkgAgVhcQgLAIgCAdQgDAagBAxQAAAaAFAXQACAQAKAKQAKAIALABQALgBAJgFQAJgJAEgPQAFgWABhGQAAgpgGgPQgFgQgJgHQgIgFgKAAQgNAAgJAKg");
	this.shape_187.setTransform(249.3,66.65);

	this.shape_188 = new cjs.Shape();
	this.shape_188.graphics.f("#00FF00").s().p("AhCCpIAAgJQARAAAIgJQAFgFAAgZIAAiDQAAgYgGgHQgGgGgSgCIAAgJIBnAAIAACzQAAAZAGAGQAGAHASABIAAAJgAgchoQgMgKAAgQQAAgPAMgMQAMgKAQgBQARABAMAKQAMAMAAAPQAAAQgMAKQgMAMgRAAQgQAAgMgMg");
	this.shape_188.setTransform(228.475,61.15);

	this.shape_189 = new cjs.Shape();
	this.shape_189.graphics.f("#00FF00").s().p("AgaCRQgQgNgEgOQgDgIAAglIAAh6IggAAIAAgIQAhgWAXgYQAYgXAQgeIAJAAIAABTIA6AAIAAAYIg6AAIAACMQAAAUACAFQACAFAFAFQAFADAEAAQARAAAPgZIAIAGQgVAvgwgBQgXAAgQgLg");
	this.shape_189.setTransform(211.9,62.65);

	this.shape_190 = new cjs.Shape();
	this.shape_190.graphics.f("#00FF00").s().p("AAaBuQgMgKgCgSQgtAkgkAAQgVAAgOgMQgOgNAAgTQAAgaAYgUQAZgWBRghIAAgXQAAgZgDgHQgEgHgHgFQgIgFgLAAQgRAAgLAHQgHAEAAAHQAAAEAHAIQAKAKAAAKQAAAMgJAJQgKAIgQAAQgQAAgMgKQgLgJAAgMQAAgRAPgRQAPgPAbgJQAbgJAdABQAjAAAUANQAVAOAGAQQADALAAAkIAABXQAAAQACAEQABADADACQACACAEABQAHgBAHgIIAIAFQgOATgOAHQgOAJgSgBQgVABgMgJgAgjAZQgKAOAAANQAAALAIAJQAHAGAMAAQANAAARgNIAAhLQgfAQgQATg");
	this.shape_190.setTransform(189.875,66.45);

	this.shape_191 = new cjs.Shape();
	this.shape_191.graphics.f("#00FF00").s().p("ABRB1IAAgJQAPgBAHgKQAFgGAAgXIAAhhQAAgegDgJQgDgIgFgEQgGgEgIAAQgLAAgMAIQgLAHgMAPIAAB6QAAAYAEAGQAGAKASAAIAAAJIh/AAIAAgJQAKAAAGgEQAGgFACgFQACgHAAgTIAAhhQAAgfgDgIQgCgIgHgEQgGgFgHABQgLgBgIAGQgNAHgOASIAAB6QAAAXAFAIQAFAIARABIAAAJIiAAAIAAgJQAQgBAHgIQAEgGAAgZIAAiAQAAgZgFgHQgFgGgRgCIAAgJIBlAAIAAAeQAVgVARgIQARgIAVAAQAYAAAPAKQAQAKAJAWQAVgXATgJQAUgKAVAAQAaAAAPALQAPALAGAQQAGARAAAkIAABdQAAAZAFAHQAFAGARACIAAAJg");
	this.shape_191.setTransform(153.275,66.3);

	this.shape_192 = new cjs.Shape();
	this.shape_192.graphics.f("#00FF00").s().p("AhCCpIAAgJQARAAAIgJQAFgFAAgZIAAiDQAAgYgGgHQgGgGgSgCIAAgJIBnAAIAACzQAAAZAGAGQAGAHASABIAAAJgAgchoQgMgKAAgQQAAgPAMgMQAMgKAQgBQARABAMAKQAMAMAAAPQAAAQgMAKQgMAMgRAAQgQAAgMgMg");
	this.shape_192.setTransform(123.175,61.15);

	this.shape_193 = new cjs.Shape();
	this.shape_193.graphics.f("#00FF00").s().p("AAIB1IAAgJQAPgBAFgKQAFgGAAgXIAAhlQAAgbgCgIQgDgHgFgEQgGgEgIAAQgVAAgTAeIAAB5QAAAZAEAHQAFAGAOACIAAAJIh9AAIAAgJQAQgBAHgIQAEgGAAgZIAAiAQAAgZgFgHQgFgGgRgCIAAgJIBmAAIAAAdQATgTAQgIQARgJAVAAQAYAAAPANQAQALAGATQAEANAAAoIAABYQAAAZAFAHQAFAGAQACIAAAJg");
	this.shape_193.setTransform(100.5,66.3);

	this.shape_194 = new cjs.Shape();
	this.shape_194.graphics.f("#00FF00").s().p("AARCoIAAgJIAHAAQAVAAAIgFQAHgEgBgIIgBgJIgIgSIgTgoIh+AAIgPAfQgIARAAAKQAAAOANAGQAHAEAcACIAAAJIh4AAIAAgJQAUgDANgLQAMgMASgmIB/kGIAGAAICBENQASAmANAKQAIAIARABIAAAJgAhVA5IBrAAIg0hxg");
	this.shape_194.setTransform(65.6,61.2);

	this.shape_195 = new cjs.Shape();
	this.shape_195.graphics.f("#00FF00").s().p("AArDUQgbAigVAMQgWAMgbAAQhGAAgpg+QgggzAAhKQAAg7AUgtQAVgvAjgYQAjgYApAAQAaAAAUAKQAUAKAWAaIAAhuQAAgqgDgJQgFgLgJgGQgKgFgYAAIAAgQIChAAIAAGjQAAAqADAIQADANAIAGQAJAHAWABIAAANIibAegAgyhFQgPAMgJAeQgIAegBA/QABBFAJAhQALAhARAPQAJAHAQAAQAhAAAeg2IAAi/Qgcg3gpAAQgPAAgJAIg");
	this.shape_195.setTransform(555.85,157.475);

	this.shape_196 = new cjs.Shape();
	this.shape_196.graphics.f("#00FF00").s().p("AAMC8IAAgOQAVgDAJgOQAGgKAAgnIAAihQAAgtgDgMQgEgMgIgGQgIgHgLAAQghAAgdAxIAADCQABApAHALQAIALAUADIAAAOIi4AAIAAgOQAXgDAJgMQAIgIgBgrIAAjNQAAgpgHgKQgIgLgYgDIAAgOICUAAIAAAvQAdgeAYgOQAbgOAdAAQAkAAAXAUQAXAUAJAcQAFAXAAA/IAACNQABArAHAKQAIAKAYADIAAAOg");
	this.shape_196.setTransform(510.95,164.6);

	this.shape_197 = new cjs.Shape();
	this.shape_197.graphics.f("#00FF00").s().p("Ah2CDQgigyAAhLQAAhbAyg2QAxg2BBAAQA4ABApAtQAoAtAEBaIjIAAQAEBHAjAqQAaAhAmAAQAXAAATgNQATgNAWgiIAOAIQgeA9gkAaQgkAYgvAAQhQABgqg/gAgaiUQgXAkAAA8IAAANIBpAAQAAg+gGgXQgHgYgPgMQgIgHgOgBQgTABgNATg");
	this.shape_197.setTransform(555.775,74.55);

	this.title_btn3 = new lib.Title_btn();
	this.title_btn3.name = "title_btn3";
	this.title_btn3.setTransform(496.35,252.4,0.5051,0.5051);
	new cjs.ButtonHelper(this.title_btn3, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.title_btn1},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},492).to({state:[]},1).to({state:[{t:this.title_btn2},{t:this.shape_147,p:{x:481.725,y:62.525}},{t:this.shape_146,p:{x:530.875,y:62.525}},{t:this.shape_145},{t:this.shape_144,p:{x:476.275,y:153.125}},{t:this.shape_143},{t:this.shape_142},{t:this.shape_141},{t:this.shape_140},{t:this.shape_139},{t:this.shape_138},{t:this.shape_137},{t:this.shape_136},{t:this.shape_135},{t:this.shape_134},{t:this.shape_133},{t:this.shape_132},{t:this.shape_131,p:{x:371.575,y:68.225}},{t:this.shape_130},{t:this.shape_129},{t:this.shape_128},{t:this.shape_127},{t:this.shape_126},{t:this.shape_125},{t:this.shape_124},{t:this.shape_123},{t:this.shape_122},{t:this.shape_121},{t:this.shape_120},{t:this.shape_119},{t:this.shape_118},{t:this.shape_117},{t:this.shape_116},{t:this.shape_115,p:{x:809.125,y:67.675}},{t:this.shape_114},{t:this.shape_113,p:{x:834.675,y:63.975}},{t:this.shape_112,p:{x:846.125,y:67.475}},{t:this.shape_111},{t:this.shape_110,p:{x:761.625,y:99.925}},{t:this.shape_109,p:{x:781.025,y:103.575}},{t:this.shape_108,p:{x:795.025,y:103.575}},{t:this.shape_107},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103,p:{x:876.325,y:103.575}},{t:this.shape_102},{t:this.shape_101},{t:this.shape_100,p:{x:815.825,y:138.825}},{t:this.shape_99},{t:this.shape_98},{t:this.shape_97,p:{x:653.6,y:171.925}},{t:this.shape_96},{t:this.shape_95,p:{x:677.575,y:177.625}},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92,p:{x:713.9,y:171.225}},{t:this.shape_91},{t:this.shape_90,p:{x:738.275,y:174.675}},{t:this.shape_89,p:{x:760.975,y:174.675}},{t:this.shape_88,p:{x:783.725,y:174.675}},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85,p:{x:825.975,y:174.475}},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82,p:{x:870.375,y:174.475}},{t:this.shape_81,p:{x:886.975,y:174.675}},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77,p:{x:945.825,y:174.475}},{t:this.shape_76,p:{x:960.475,y:174.475}},{t:this.shape_75},{t:this.shape_74,p:{x:999.05,y:171.225}}]},31).to({state:[]},5).to({state:[{t:this.title_btn3},{t:this.shape_147,p:{x:466.275,y:66.325}},{t:this.shape_146,p:{x:515.425,y:66.325}},{t:this.shape_197},{t:this.shape_144,p:{x:460.825,y:156.925}},{t:this.shape_196},{t:this.shape_195},{t:this.shape_194},{t:this.shape_193},{t:this.shape_192},{t:this.shape_191},{t:this.shape_190},{t:this.shape_189},{t:this.shape_188},{t:this.shape_187},{t:this.shape_186},{t:this.shape_185},{t:this.shape_131,p:{x:356.125,y:72.025}},{t:this.shape_184},{t:this.shape_183},{t:this.shape_182},{t:this.shape_181},{t:this.shape_180},{t:this.shape_179},{t:this.shape_178},{t:this.shape_177},{t:this.shape_176},{t:this.shape_175},{t:this.shape_174},{t:this.shape_173},{t:this.shape_172},{t:this.shape_171},{t:this.shape_170},{t:this.shape_115,p:{x:793.675,y:71.475}},{t:this.shape_169},{t:this.shape_113,p:{x:819.225,y:67.775}},{t:this.shape_112,p:{x:830.675,y:71.275}},{t:this.shape_168},{t:this.shape_110,p:{x:746.175,y:103.725}},{t:this.shape_109,p:{x:765.575,y:107.375}},{t:this.shape_108,p:{x:779.575,y:107.375}},{t:this.shape_167},{t:this.shape_166},{t:this.shape_165},{t:this.shape_164},{t:this.shape_103,p:{x:860.875,y:107.375}},{t:this.shape_163},{t:this.shape_162},{t:this.shape_100,p:{x:800.375,y:142.625}},{t:this.shape_161},{t:this.shape_160},{t:this.shape_159},{t:this.shape_97,p:{x:648.65,y:175.725}},{t:this.shape_95,p:{x:662.125,y:181.425}},{t:this.shape_158},{t:this.shape_157},{t:this.shape_74,p:{x:698.45,y:175.025}},{t:this.shape_156},{t:this.shape_90,p:{x:722.825,y:178.475}},{t:this.shape_89,p:{x:745.525,y:178.475}},{t:this.shape_88,p:{x:768.275,y:178.475}},{t:this.shape_155},{t:this.shape_154},{t:this.shape_85,p:{x:810.525,y:178.275}},{t:this.shape_153},{t:this.shape_152},{t:this.shape_82,p:{x:854.925,y:178.275}},{t:this.shape_81,p:{x:871.525,y:178.475}},{t:this.shape_151},{t:this.shape_150},{t:this.shape_149},{t:this.shape_77,p:{x:930.375,y:178.275}},{t:this.shape_76,p:{x:945.025,y:178.275}},{t:this.shape_148},{t:this.shape_92,p:{x:983.6,y:175.025}}]},42).to({state:[]},1).to({state:[]},172).wait(1));

	// Scene4
	this.Next_4 = new lib.Next();
	this.Next_4.name = "Next_4";
	this.Next_4.setTransform(848.95,389.7,0.5812,0.5812,-90);
	this.Next_4._off = true;
	new cjs.ButtonHelper(this.Next_4, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.Next_4).wait(214).to({_off:false},0).to({_off:true},12).wait(519));

	// Scene3
	this.Next_3 = new lib.Next();
	this.Next_3.name = "Next_3";
	this.Next_3.setTransform(982.1,710.05,0.581,0.581,0,0,0,0.1,0.1);
	this.Next_3._off = true;
	new cjs.ButtonHelper(this.Next_3, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.Next_3).wait(245).to({_off:false},0).to({_off:true},116).wait(384));

	// Scene2
	this.Next_2 = new lib.Next();
	this.Next_2.name = "Next_2";
	this.Next_2.setTransform(980.5,663.05,0.5812,0.5812);
	this.Next_2._off = true;
	new cjs.ButtonHelper(this.Next_2, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.Next_2).wait(214).to({_off:false},0).to({_off:true},2).wait(529));

	// Scene2_Road
	this.shape_198 = new cjs.Shape();
	this.shape_198.graphics.f("#663300").s().p("AqQOyIAA9jIUhAAIAAdjg");
	this.shape_198.setTransform(1102,469.625);

	this.shape_199 = new cjs.Shape();
	this.shape_199.graphics.f("#663300").s().p("AqPOyIAA9jIUfAAIAAdjg");
	this.shape_199.setTransform(1022.95,469.625);

	this.shape_200 = new cjs.Shape();
	this.shape_200.graphics.f("#663300").s().p("AqQOyIAA9jIUgAAIAAdjg");
	this.shape_200.setTransform(999.7,469.625);

	this.shape_201 = new cjs.Shape();
	this.shape_201.graphics.f("#663300").s().p("AqPOyIAA9jIUgAAIAAdjg");
	this.shape_201.setTransform(953.2,469.625);

	this.shape_202 = new cjs.Shape();
	this.shape_202.graphics.f("#663300").s().p("AqQYVMAAAgwpIUgAAMAAAAwpg");
	this.shape_202.setTransform(844.7,469.75);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_198,p:{x:1102}}]},203).to({state:[{t:this.shape_198,p:{x:1046.2}}]},1).to({state:[{t:this.shape_199,p:{x:1022.95}}]},1).to({state:[{t:this.shape_200,p:{x:999.7}}]},1).to({state:[{t:this.shape_198,p:{x:976.45}}]},1).to({state:[{t:this.shape_201,p:{x:953.2}}]},1).to({state:[{t:this.shape_200,p:{x:929.95}}]},1).to({state:[{t:this.shape_198,p:{x:906.7}}]},1).to({state:[{t:this.shape_201,p:{x:891.2}}]},1).to({state:[{t:this.shape_199,p:{x:875.7}}]},1).to({state:[{t:this.shape_200,p:{x:860.2}}]},1).to({state:[{t:this.shape_202}]},1).to({state:[]},2).wait(529));

	// Previous_Button3
	this.Prev_3 = new lib.Prev();
	this.Prev_3.name = "Prev_3";
	this.Prev_3.setTransform(63.45,710,0.581,0.581,0,0,0,0.1,0);
	this.Prev_3._off = true;
	new cjs.ButtonHelper(this.Prev_3, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.Prev_3).wait(365).to({_off:false},0).to({_off:true},127).wait(253));

	// Previous_Button2
	this.Prev_2 = new lib.Prev();
	this.Prev_2.name = "Prev_2";
	this.Prev_2.setTransform(63.45,710.05,0.581,0.581,0,0,0,0.1,0.1);
	this.Prev_2._off = true;
	new cjs.ButtonHelper(this.Prev_2, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.Prev_2).wait(245).to({_off:false},0).to({_off:true},116).wait(384));

	// UFO
	this.UFO = new lib.UFO();
	this.UFO.name = "UFO";
	this.UFO.setTransform(-113.75,68.75,0.1,0.1,0,0,0,-1.5,-0.5);
	this.UFO._off = true;

	this.timeline.addTween(cjs.Tween.get(this.UFO).wait(10).to({_off:false},0).to({x:101,y:77.6},14).to({regX:-1.4,regY:-0.4,scaleX:0.25,scaleY:0.25,x:939.35,y:132.45},23).to({scaleX:0.5,scaleY:0.5,x:172.6,y:180.45},24).to({scaleX:0.75,scaleY:0.75,x:795.2,y:175.5},12).to({scaleX:1,scaleY:1,x:243.15,y:113.7},12).wait(24).to({regY:-0.3,scaleX:0.5,scaleY:0.5,x:-129.65,y:117.15},5).to({x:-121.65,y:123.15},8).to({x:0,y:92.15},11).to({x:168.05,y:90.15},13).to({x:244.05,y:226.2},11).to({x:272.05,y:120.2},12).to({x:178.05,y:107.2},12).to({x:223.05,y:168.25},12).to({x:257.05,y:103.2},12).to({x:372.1,y:186.25},12).to({x:511.55,y:190.2},12).to({x:-218.85,y:151.85},1).to({x:27.25,y:137.6},59).to({x:467.65,y:85.75},13).to({_off:true},48).wait(135).to({_off:false,regX:-1.5,regY:-0.5,scaleX:1,scaleY:1,x:-235.3,y:0},0).to({x:574.55},10).wait(15).to({x:548,y:-162.7},4).to({x:-355.45,y:162.7},5).wait(10).to({scaleX:1.2409,scaleY:1.2409},12).to({x:513.55,y:365.6},9).to({x:1359.55,y:166.55},10).to({_off:true},1).wait(174));

	// Inside_Beam
	this.instance = new lib.Tween29("synched",0);
	this.instance.setTransform(686.45,-34.3);
	this.instance._off = true;

	this.instance_1 = new lib.Tween30("synched",0);
	this.instance_1.setTransform(703.95,86.8);
	this.instance_1._off = true;

	this.instance_2 = new lib.Tween31("synched",0);
	this.instance_2.setTransform(695.55,223.45);
	this.instance_2._off = true;

	this.instance_3 = new lib.Tween32("synched",0);
	this.instance_3.setTransform(672.85,86.15);
	this.instance_3._off = true;

	this.instance_4 = new lib.Beam();
	this.instance_4.setTransform(665.1,-159.9);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(373).to({_off:false},0).to({_off:true,x:703.95,y:86.8},10).wait(362));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(373).to({_off:false},10).to({scaleY:1.1006,x:694.7,y:217.6},11).to({_off:true,scaleY:1,x:695.55,y:223.45},1).wait(350));
	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(394).to({_off:false},1).to({_off:true,x:672.85,y:86.15},12).wait(338));
	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(395).to({_off:false},12).to({_off:true,x:665.1,y:-159.9,mode:"independent"},14).wait(324));
	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(407).to({_off:false},14).wait(70).to({_off:true},1).wait(253));

	// Alien
	this.instance_5 = new lib.Alien();
	this.instance_5.setTransform(226.7,360.1,1,1,0,0,0,-241.2,-1);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(365).to({_off:false},0).wait(30).to({skewY:180},49).to({skewY:0},11).to({x:677.4,y:414.5},12).to({x:1084.05,y:422.3},12).to({x:1340.45,y:406.75},12).to({_off:true},1).wait(253));

	// Bag
	this.instance_6 = new lib.Bag();
	this.instance_6.setTransform(817.35,296.55,0.2413,0.2413,0,0,0,36.6,14.1);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(336).to({_off:false},0).to({x:673.55,y:214.95},11).to({x:500,y:102.25},12).to({regX:36.9,regY:14.2,scaleX:2.1349,scaleY:2.1349,x:712.8,y:566.15},6).wait(30).to({x:694.65,y:290.3},12).to({x:699.85,y:-23.1},14).to({x:690.8,y:-235.5},10).wait(60).to({_off:true},1).wait(253));

	// Human
	this.instance_7 = new lib.Human();
	this.instance_7.setTransform(684.85,572.35,1,1,0,0,0,149,97.5);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(365).to({_off:false},0).wait(126).to({_off:true},1).wait(253));

	// UFO_Beam
	this.instance_8 = new lib.Tween18("synched",0);
	this.instance_8.setTransform(245.15,214.8);
	this.instance_8._off = true;

	this.instance_9 = new lib.Tween19("synched",0);
	this.instance_9.setTransform(220.15,403.6);

	this.instance_10 = new lib.Tween20("synched",0);
	this.instance_10.setTransform(469.6,134.95);
	this.instance_10._off = true;

	this.instance_11 = new lib.Tween21("synched",0);
	this.instance_11.setTransform(535.05,132.45);
	this.instance_11._off = true;

	this.instance_12 = new lib.Tween22("synched",0);
	this.instance_12.setTransform(615.45,206.65);
	this.instance_12._off = true;

	this.instance_13 = new lib.Tween23("synched",0);
	this.instance_13.setTransform(645.2,222.2);
	this.instance_13._off = true;

	this.instance_14 = new lib.Tween24("synched",0);
	this.instance_14.setTransform(645.2,222.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_8}]},95).to({state:[{t:this.instance_8}]},3).to({state:[{t:this.instance_8}]},4).to({state:[{t:this.instance_8}]},4).to({state:[{t:this.instance_9}]},1).to({state:[]},14).to({state:[{t:this.instance_10}]},191).to({state:[{t:this.instance_11}]},5).to({state:[{t:this.instance_11}]},4).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_12}]},1).to({state:[{t:this.instance_13}]},11).to({state:[{t:this.instance_14}]},24).to({state:[]},1).wait(384));
	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(95).to({_off:false},0).to({regY:0.4,scaleY:7.6879,y:217.9},3).to({regY:0,scaleY:12.6529,y:245.8},4).to({regX:0.1,regY:0.7,scaleX:1.3048,scaleY:18.3336,x:242.3,y:362.8},4).to({_off:true,regX:0,regY:0,scaleX:1,scaleY:1,x:220.15,y:403.6},1).wait(638));
	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(312).to({_off:false},0).to({_off:true,x:535.05,y:132.45},5).wait(428));
	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(312).to({_off:false},5).to({x:575.25,y:169.55},4).to({x:585.3,y:178.85},1).to({x:595.35,y:188.1},1).to({x:605.4,y:197.4},1).to({_off:true,x:615.45,y:206.65},1).wait(420));
	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(324).to({_off:false},1).to({_off:true,x:645.2,y:222.2},11).wait(409));
	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(325).to({_off:false},11).to({_off:true},24).wait(385));

	// Door
	this.shape_203 = new cjs.Shape();
	this.shape_203.graphics.f().s("#000000").ss(1,1,1).p("AjBlwIGDAAIAALhImDAAg");
	this.shape_203.setTransform(901.475,444.9);

	this.shape_204 = new cjs.Shape();
	this.shape_204.graphics.f("#996600").s().p("AjBFxIAArhIGDAAIAALhg");
	this.shape_204.setTransform(901.475,444.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_204},{t:this.shape_203}]},244).to({state:[]},115).to({state:[]},363).wait(23));

	// Rock
	this.Rock_btn = new lib.Rock();
	this.Rock_btn.name = "Rock_btn";
	this.Rock_btn.setTransform(877.4,400.55,0.6758,0.6758);
	this.Rock_btn._off = true;
	new cjs.ButtonHelper(this.Rock_btn, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.Rock_btn).wait(10).to({_off:false},0).to({_off:true},113).wait(372).to({_off:false,regX:0.1,regY:0.1,scaleX:1.6026,scaleY:1.6026,x:537.4,y:531.7,mode:"synched",startPosition:0},0).to({_off:true},34).wait(216));

	// UFO_Beam_2
	this.instance_15 = new lib.Beam();
	this.instance_15.setTransform(582.95,0,1,0.5968);
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(506).to({_off:false},0).wait(1).to({scaleY:1.0288},0).wait(1).to({scaleY:1.4032},0).wait(1).to({regX:0.1,scaleY:1.8348,x:583.05},0).wait(1).to({scaleY:2.0936},0).wait(1).to({scaleY:2.4676},0).wait(1).to({scaleY:2.8414},0).wait(1).to({regX:0.2,scaleY:3.1296,x:583.15},0).wait(1).to({scaleY:3.4757},0).wait(1).to({regX:0,scaleY:3.7064,x:582.95},0).wait(1).to({scaleY:3.1015},0).wait(1).to({scaleY:2.554},0).wait(1).to({scaleY:2.1798},0).wait(1).to({scaleY:1.4311},0).wait(1).to({scaleY:0.5961},0).to({_off:true},1).wait(224));

	// Cow
	this.S1_cow = new lib.Cow();
	this.S1_cow.name = "S1_cow";
	this.S1_cow.setTransform(604.55,608.1,0.7284,0.7284,0,0,0,166,60.2);
	this.S1_cow._off = true;

	this.timeline.addTween(cjs.Tween.get(this.S1_cow).wait(10).to({_off:false},0).wait(109).to({x:-329.85,y:596.3},5).to({x:204.6,y:556.35},8).to({x:522.3,y:543.4},11).wait(72).to({x:942.4,y:566.4},12).to({x:1237.5,y:580.3},12).to({x:1250.45,y:1164.45},1).to({x:-504.55,y:1112.6},1).to({x:-261.05,y:518.1},1).to({x:-232.55,y:534.95},3).to({x:3.2,y:549.2},5).to({regX:166.1,regY:60.4,scaleX:0.4649,scaleY:0.4649,x:435.8,y:502.7},13).to({regX:166.8,regY:60.9,scaleX:0.1405,scaleY:0.1405,x:882.7,y:449.6},12).to({regX:167.8,scaleX:0.0477,x:898.2,y:453.5},7).to({_off:true},5).wait(208).to({_off:false,regX:166,regY:42.2,scaleX:0.7532,scaleY:0.7532,x:-213.6,y:739.6},0).to({x:621.75,y:502.35},10).to({y:423},11).to({regX:166.1,regY:42.3,scaleX:0.5767,scaleY:0.5767,x:604.55,y:340.25},1).to({x:594.2,y:264.35},1).to({x:587.3,y:167.8},1).to({regX:166.2,scaleX:0.4526,scaleY:0.2874,x:583.85,y:40.15},1).to({_off:true},1).wait(8).to({_off:false,regX:166,scaleX:0.4079,scaleY:0.4079,x:544.2,y:879.85},0).to({x:540.4,y:386.05},22).to({_off:true},9).wait(185));

	// House
	this.instance_16 = new lib.House();
	this.instance_16.setTransform(900.9,376.05);
	this.instance_16._off = true;
	new cjs.ButtonHelper(this.instance_16, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(244).to({_off:false},0).to({_off:true},116).wait(385));

	// Rock_and_Bush
	this.instance_17 = new lib.Tween8("synched",0);
	this.instance_17.setTransform(767.65,427.35);
	this.instance_17._off = true;

	this.instance_18 = new lib.Tween9("synched",0);
	this.instance_18.setTransform(484.6,428.35);
	this.instance_18._off = true;

	this.instance_19 = new lib.Tween10("synched",0);
	this.instance_19.setTransform(93.5,440.35);
	this.instance_19._off = true;

	this.instance_20 = new lib.Tween11("synched",0);
	this.instance_20.setTransform(-247.6,448.35);
	this.instance_20._off = true;

	this.instance_21 = new lib.Tween12("synched",0);
	this.instance_21.setTransform(-344.65,973.5);
	this.instance_21._off = true;

	this.instance_22 = new lib.Tween13("synched",0);
	this.instance_22.setTransform(1237.75,993.5);
	this.instance_22._off = true;

	this.instance_23 = new lib.Tween14("synched",0);
	this.instance_23.setTransform(1094.55,759.55);
	this.instance_23._off = true;

	this.instance_24 = new lib.Tween15("synched",0);
	this.instance_24.setTransform(500.4,759.55);
	this.instance_24._off = true;

	this.instance_25 = new lib.Tween16("synched",0);
	this.instance_25.setTransform(-21.7,759.55);
	this.instance_25._off = true;

	this.instance_26 = new lib.Tween17("synched",0);
	this.instance_26.setTransform(-317.8,758.55);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_17}]},124).to({state:[{t:this.instance_18}]},32).to({state:[{t:this.instance_19}]},11).to({state:[{t:this.instance_20}]},12).to({state:[{t:this.instance_21}]},5).to({state:[{t:this.instance_22}]},1).to({state:[{t:this.instance_23}]},6).to({state:[{t:this.instance_24}]},12).to({state:[{t:this.instance_25}]},12).to({state:[{t:this.instance_26}]},12).to({state:[]},12).to({state:[]},1).wait(505));
	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(124).to({_off:false},0).to({_off:true,x:484.6,y:428.35},32).wait(589));
	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(124).to({_off:false},32).to({_off:true,x:93.5,y:440.35},11).wait(578));
	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(156).to({_off:false},11).to({_off:true,x:-247.6,y:448.35},12).wait(566));
	this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(167).to({_off:false},12).to({_off:true,x:-344.65,y:973.5},5).wait(561));
	this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(179).to({_off:false},5).to({_off:true,x:1237.75,y:993.5},1).wait(560));
	this.timeline.addTween(cjs.Tween.get(this.instance_22).wait(184).to({_off:false},1).to({_off:true,x:1094.55,y:759.55},6).wait(554));
	this.timeline.addTween(cjs.Tween.get(this.instance_23).wait(185).to({_off:false},6).to({_off:true,x:500.4},12).wait(542));
	this.timeline.addTween(cjs.Tween.get(this.instance_24).wait(191).to({_off:false},12).to({_off:true,x:-21.7},12).wait(530));
	this.timeline.addTween(cjs.Tween.get(this.instance_25).wait(203).to({_off:false},12).to({_off:true,x:-317.8,y:758.55},12).wait(518));

	// Title
	this.instance_27 = new lib.Title();
	this.instance_27.setTransform(513.5,109.85);

	this.timeline.addTween(cjs.Tween.get(this.instance_27).to({_off:true},1).wait(744));

	// Play_Button
	this.play_btn = new lib.Play_Button();
	this.play_btn.name = "play_btn";
	this.play_btn.setTransform(526.05,264.7);
	new cjs.ButtonHelper(this.play_btn, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.play_btn).to({_off:true},1).wait(744));

	// Mute_Buttons
	this.Unmute_BTN = new lib.Mute_Button();
	this.Unmute_BTN.name = "Unmute_BTN";
	this.Unmute_BTN.setTransform(858.3,688.75,0.5479,0.5479);
	this.Unmute_BTN.visible = false;
	new cjs.ButtonHelper(this.Unmute_BTN, 0, 1, 1);

	this.Mute_Btn = new lib.UnMute_Button();
	this.Mute_Btn.name = "Mute_Btn";
	this.Mute_Btn.setTransform(855.4,688.2,0.5323,0.5323);
	new cjs.ButtonHelper(this.Mute_Btn, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.Mute_Btn,p:{scaleX:0.5323,scaleY:0.5323,x:855.4,y:688.2}},{t:this.Unmute_BTN,p:{scaleX:0.5479,scaleY:0.5479,x:858.3,y:688.75}}]}).to({state:[{t:this.Mute_Btn,p:{scaleX:0.3528,scaleY:0.3528,x:910.1,y:62.75}},{t:this.Unmute_BTN,p:{scaleX:0.3412,scaleY:0.3412,x:912.55,y:62.9}}]},1).wait(744));

	// Spaceship_Floor
	this.shape_205 = new cjs.Shape();
	this.shape_205.graphics.f().s("#000000").ss(1,1,1).p("EhwagdoMDg1AAAMAAAA7RMjg1AAAg");
	this.shape_205.setTransform(509.675,702.675);

	this.shape_206 = new cjs.Shape();
	this.shape_206.graphics.f("#CCCCCC").s().p("EhwaAdpMAAAg7RMDg1AAAMAAAA7Rg");
	this.shape_206.setTransform(509.675,702.675);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_206},{t:this.shape_205}]},361).to({state:[{t:this.shape_206},{t:this.shape_205}]},131).to({state:[]},1).wait(252));

	// Spaceship_Wall
	this.shape_207 = new cjs.Shape();
	this.shape_207.graphics.f().s("#000000").ss(1,1,1).p("EhcEhAjMC4JAAAMAAACBHMi4JAAAg");
	this.shape_207.setTransform(461.1,391.15);

	this.shape_208 = new cjs.Shape();
	this.shape_208.graphics.f("#999999").s().p("EhcEBAkMAAAiBGMC4JAAAMAAACBGg");
	this.shape_208.setTransform(461.1,391.15);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_208},{t:this.shape_207}]},361).to({state:[{t:this.shape_208},{t:this.shape_207}]},131).to({state:[]},1).wait(252));

	// Road
	this.Road_btn1 = new lib.Road();
	this.Road_btn1.name = "Road_btn1";
	this.Road_btn1.setTransform(830.55,599.35,1,1.1892,0,32.5922,-0.1766);
	new cjs.ButtonHelper(this.Road_btn1, 0, 1, 1);

	this.shape_209 = new cjs.Shape();
	this.shape_209.graphics.f("#663300").s().p("EhlfAOyIAA9jMDK/AAAIAAdjg");
	this.shape_209.setTransform(520.525,655.025);

	this.shape_210 = new cjs.Shape();
	this.shape_210.graphics.f("#663300").s().p("EgR1Ar6MAAAhXzMAjrAAAMAAABXzg");
	this.shape_210.setTransform(534.55,594.85);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.Road_btn1}]},10).to({state:[{t:this.shape_209,p:{x:520.525,y:655.025}}]},114).to({state:[{t:this.shape_209,p:{x:493.675,y:627.975}}]},115).to({state:[{t:this.shape_209,p:{x:381.625,y:631.975}}]},5).to({state:[]},117).to({state:[{t:this.shape_210}]},168).to({state:[]},192).wait(24));

	// Hills
	this.shape_211 = new cjs.Shape();
	this.shape_211.graphics.f().s("#000000").ss(1,1,1).p("EBTYAAAUgNRAYVghzARNUghzAROgiiAAAUgiiAAAgPDgROQvCxNNR4VUANRgYUAhzgROUAh0gRNAihAAAUAiiAAAAPCARNQPDROtRYUg");
	this.shape_211.setTransform(1212.2195,727.975);

	this.shape_212 = new cjs.Shape();
	this.shape_212.graphics.f("#007733").s().p("EhRmApiQvCxNNR4VUANRgYUAhzgROUAh0gRNAihAAAUAiiAAAAPCARNQPDROtRYUUgNRAYVghzARNUghzAROgiiAAAUgiiAAAgPDgROg");
	this.shape_212.setTransform(1212.2195,727.975);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_212},{t:this.shape_211}]}).to({state:[]},10).to({state:[]},114).wait(621));

	// Ground
	this.shape_213 = new cjs.Shape();
	this.shape_213.graphics.f("#006633").s().p("EhbnAl6MAAAhL0MC3PAAAMAAABL0g");
	this.shape_213.setTransform(520.725,640.1);

	this.shape_214 = new cjs.Shape();
	this.shape_214.graphics.f("#006633").s().p("EhbnAl7MAAAhL0MC3PAAAMAAABL0g");
	this.shape_214.setTransform(520.725,555.1);
	this.shape_214._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_213}]}).to({state:[{t:this.shape_213}]},119).to({state:[{t:this.shape_214}]},4).to({state:[{t:this.shape_214}]},116).to({state:[{t:this.shape_214}]},5).to({state:[{t:this.shape_214}]},117).to({state:[{t:this.shape_214}]},132).to({state:[]},228).wait(24));
	this.timeline.addTween(cjs.Tween.get(this.shape_214).wait(123).to({_off:false},0).wait(370).to({_off:true},228).wait(24));

	// Sky
	this.shape_215 = new cjs.Shape();
	this.shape_215.graphics.f().s("#000000").ss(1,1,1).p("EhRag9MMCi1AAAMAAAB6ZMii1AAAg");
	this.shape_215.setTransform(515.275,386.8);

	this.shape_216 = new cjs.Shape();
	this.shape_216.graphics.f("#000066").s().p("EhRaA9NMAAAh6ZMCi1AAAMAAAB6Zg");
	this.shape_216.setTransform(515.275,386.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_216},{t:this.shape_215}]}).to({state:[{t:this.shape_216},{t:this.shape_215}]},119).to({state:[{t:this.shape_216},{t:this.shape_215}]},4).to({state:[{t:this.shape_216},{t:this.shape_215}]},116).to({state:[{t:this.shape_216},{t:this.shape_215}]},5).to({state:[{t:this.shape_216},{t:this.shape_215}]},117).to({state:[{t:this.shape_216},{t:this.shape_215}]},131).to({state:[{t:this.shape_216},{t:this.shape_215}]},228).to({state:[]},1).wait(24));

	this._renderFirstFrame();

}).prototype = p = new lib.AnMovieClip();
p.nominalBounds = new cjs.Rectangle(-195.4,-61.9,2325.1,1370.1000000000001);
// library properties:
lib.properties = {
	id: 'A7E57CD1D0DD0E4188C49659C0555532',
	width: 1024,
	height: 768,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"sounds/_175356__maxheadroom__horsegallopingwav.mp3", id:"_175356__maxheadroom__horsegallopingwav"},
		{src:"sounds/_235__erratic__ufoatmospherewav.mp3", id:"_235__erratic__ufoatmospherewav"},
		{src:"sounds/_431860__cheetahzbrain__deadcowwav.mp3", id:"_431860__cheetahzbrain__deadcowwav"},
		{src:"sounds/_51751__erkanozan__ufosweep01wav.mp3", id:"_51751__erkanozan__ufosweep01wav"},
		{src:"sounds/bensounddeepblue.mp3", id:"bensounddeepblue"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['A7E57CD1D0DD0E4188C49659C0555532'] = {
	getStage: function() { return exportRoot.stage; },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}
an.handleSoundStreamOnTick = function(event) {
	if(!event.paused){
		var stageChild = stage.getChildAt(0);
		if(!stageChild.paused || stageChild.ignorePause){
			stageChild.syncStreamSounds();
		}
	}
}
an.handleFilterCache = function(event) {
	if(!event.paused){
		var target = event.target;
		if(target){
			if(target.filterCacheList){
				for(var index = 0; index < target.filterCacheList.length ; index++){
					var cacheInst = target.filterCacheList[index];
					if((cacheInst.startFrame <= target.currentFrame) && (target.currentFrame <= cacheInst.endFrame)){
						cacheInst.instance.cache(cacheInst.x, cacheInst.y, cacheInst.w, cacheInst.h);
					}
				}
			}
		}
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;